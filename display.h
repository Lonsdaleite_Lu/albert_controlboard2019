#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include "stm8s.h"

#define CLK_HIGH TRUE
#define CLK_LOW FALSE

/************************************************************************
以下是数码管显示“0~F”的数组，方便以后调用
1、共阳：
         unsigned char table[]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,0x80,0x90,0x88,0x83,0xc6,0xa1,0x86,0x8e};
2、共阴：
         unsigned char table[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f,0x77,0x7c,0x39,0x5e,0x79,0x71};
--------------------- 
版权声明：本文为CSDN博主「chengbozhe」的原创文章，遵循CC 4.0 by-sa版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/chengbozhe/article/details/44860215
*******************************************************************************/
#define NUM_OF_LED 4
#define DISPLAY_0 0xBF
#define DISPLAY_1 0x86
#define DISPLAY_2 0xDB
#define DISPLAY_3 0xCF
#define DISPLAY_4 0xE6
#define DISPLAY_5 0xED
#define DISPLAY_6 0xFD
#define DISPLAY_7 0x87
#define DISPLAY_8 0xFF
#define DISPLAY_9 0xEF
// #define DISPLAY_H 0xF6
// #define DISPLAY_P 0xF3
#define DISPLAY_Dark 0x00
#define DISPLAY_A 0x77     //"A"                 
#define DISPLAY_B 0x7C     //"B"                 
#define DISPLAY_C 0x39     //"C"                 
#define DISPLAY_D 0x5E     //"D"                 
#define DISPLAY_E 0x79     //"E"                 
#define DISPLAY_F 0x71     //"F"                 
#define DISPLAY_H 0x76     //"H"                 
#define DISPLAY_L 0x38     //"L"                 
#define DISPLAY_N 0x37     //"n"                 
#define DISPLAY_U 0x3E     //"u"                 
#define DISPLAY_P 0x73     //"P"                 
#define DISPLAY_O 0x5C     //"o"                 
#define DISPLAY_Minus 0x40 //"-"   
#define DISPLAY_W 0x6A     //"w"
#define DISPLAY_R 0x50     //"r"
#define DISPLAY_K 0x72     //"k"
#define DISPLAY_Y 0x6e     //"y"
#define DISPLAY_I 0x19     //"I""" ""
#define DISPLAY_T 0x78 //"t""" "" 

#define SYSOFF 0x4800       // 系统禁止，时钟震荡，不显示
#define SYSON 0x4801        // 系统使能，开启显示，按键8级亮度
#define SLEEPOFF 0x4800     // 睡眠禁止
#define SLEEPON 0x4804      // 睡眠使能，时钟停振
#define SEG7_ON 0x4809      // 系统使能，7段显示
#define SEG8_ON 0x4801      // 系统使能，8段显示，DP作SG显示驱动，8级亮度
#define SYSON_1_SEG8 0x4811 // 系统使能，8段显示，1级亮度
#define SYSON_2_SEG8 0x4821 // 系统使能，8段显示，2级亮度
#define SYSON_3_SEG8 0x4831 // 系统使能，8段显示，3级亮度
#define SYSON_4_SEG8 0x4841 // 系统使能，8段显示，4级亮度
#define SYSON_5_SEG8 0x4851 // 系统使能，8段显示，5级亮度
#define SYSON_6_SEG8 0x4861 // 系统使能，8段显示，6级亮度
#define SYSON_7_SEG8 0x4871 // 系统使能，8段显示，7级亮度
#define SYSON_8_SEG8 0x4801 // 系统使能，8段显示，8级亮度
#define SYSON_1_SEG7 0x4819 // 系统使能，7段显示，1级亮度
#define SYSON_2_SEG7 0x4829 // 系统使能，7段显示，2级亮度
#define SYSON_3_SEG7 0x4839 // 系统使能，7段显示，3级亮度
#define SYSON_4_SEG7 0x4849 // 系统使能，7段显示，4级亮度
#define SYSON_5_SEG7 0x4859 // 系统使能，7段显示，5级亮度
#define SYSON_6_SEG7 0x4869 // 系统使能，7段显示，6级亮度
#define SYSON_7_SEG7 0x4879 // 系统使能，7段显示，7级亮度
#define SYSON_8_SEG7 0x4809 // 系统使能，7段显示，8级亮度

#define mSetDataHigh() GPIO_WriteHigh(GPIOC, GPIO_PIN_3)
#define mSetDataLow() GPIO_WriteLow(GPIOC, GPIO_PIN_3)
#define mSetDataRev() GPIO_WriteReverse(GPIOC, GPIO_PIN_3)
#define mSetDataInput() GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_IN_FL_NO_IT)
#define mSetDataOutputHigh() GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_OUT_PP_HIGH_FAST)
#define mSetDataOutputLow() GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_FAST)

#define mSetClkHigh() GPIO_WriteHigh(GPIOC, GPIO_PIN_5)
#define mSetClkLow() GPIO_WriteLow(GPIOC, GPIO_PIN_5)
#define mSetClkRev() GPIO_WriteReverse(GPIOC, GPIO_PIN_5)
#define mSetClkInput() GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_IN_FL_NO_IT)
#define mSetClkOutputHigh() GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_FAST)
#define mSetClkOutputLow() GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_FAST)

#define DISPLAY_LIGHT SYSON_1_SEG8, SYSON_2_SEG8, SYSON_3_SEG8, \
                      SYSON_4_SEG8, SYSON_5_SEG8, SYSON_6_SEG8, \
                      SYSON_7_SEG8, SYSON_8_SEG8

#define DISPLAY_NUMBER DISPLAY_0, DISPLAY_1, DISPLAY_2,     \
                       DISPLAY_3, DISPLAY_4, DISPLAY_5,     \
                       DISPLAY_6, DISPLAY_7, DISPLAY_8,     \
                       DISPLAY_9,                           \
                       DISPLAY_A, DISPLAY_B, DISPLAY_C,     \
                       DISPLAY_D, DISPLAY_E, DISPLAY_F,     \
                       DISPLAY_H, DISPLAY_L, DISPLAY_N,     \
                       DISPLAY_U, DISPLAY_P, DISPLAY_O,     \
                       DISPLAY_W, DISPLAY_R, DISPLAY_Minus, \
                       DISPLAY_K, DISPLAY_Y, DISPLAY_I,     \
                       DISPLAY_T,\
                       DISPLAY_Dark

typedef enum _tDisplayType
{
  Display_0,
  Display_1,
  Display_2,
  Display_3,
  Display_4,
  Display_5,
  Display_6,
  Display_7,
  Display_8,
  Display_9,
  Display_A,
  Display_B,
  Display_C,
  Display_D,
  Display_E,
  Display_F,
  Display_H,
  Display_L,
  Display_N,
  Display_U,
  Display_P,
  Display_O,
  Display_W,
  Display_R,
  Display_Minus,
  Display_K,
  Display_Y,
  Display_I,
  Display_T,
  Display_Dark,
  Total_Display_Num
} tDisplayType;

/*  
typedef enum _tDisplayLight
{
  DisplayLight_1,
  DisplayLight_2,
  DisplayLight_3,
  DisplayLight_4,
  DisplayLight_5,
  DisplayLight_6,
  DisplayLight_7,
  DisplayLight_8,
  Total_DisplayLight_Num
}tDisplayLight;
 */

typedef enum _tSendCommandState
{
  SendCommand_IdleState,
  SendCommand_Start,
  SendCommand_SendHighByte,
  SendCommand_WaitingForACK1,
  SendCommand_SendLowByte,
  SendCommand_WaitingForACK2,
  SendCommand_Finish
} tSendCommandState;

typedef enum _tSegState
{
  Seg_IdleState,
  Seg_SystemOn,
  Seg_Enable,
  Seg_Disable,
  Seg_Refresh,
  Seg_OneValueUpdating,
  Seg_Working
} tSegState;

typedef enum _tSendCommandConfig
{
  Command_Idle,
  Command_Send,
  //Command_Displayinfo,
  Command_Updating,
  Command_Updated
} tSendCommandConfig;

static u8 uiDisplayLED_DP;

void Display_Init(void);
//void Display_ChangeISR(void);
void Display_SendCommand_UPD_IRQ(void);
//void Display_SendOneCommand(void);
//void Display_SendCommand_CAP_IRQ(void);
void Display_SetDispalyInfo(u16 uiValue);
//void Display_SetConfig(u16 uiConfig);
void Display_Blink(u8 uiDisplayValue);     // high 4 bit should be 0.eg.0x0f...low bit 1 means blink
void Display_Not_Blink(u8 uiDisplayValue); // high 4 bit should be 1.eg.0xf0...low bit means no blink
void Display_7Seg_Blink(void);
void Display_7Seg_Not_Blink(void);
void Display_Clr_AllBlink(void);
void Display_DP_Blink(void);
void Display_DP_Not_Blink(void);

void Display_Set_DP(void);
void Display_Clr_DP(void);
void Display_SetLED1(void);
void Display_ClrLED1(void);
void Display_SetLED1_Blink(void);
void Display_ClrLED1_Blink(void);

void Display_SetLED2(void);
void Display_ClrLED2(void);
void Display_SetLED2_Blink(void);
void Display_ClrLED2_Blink(void);

void Display_Code_OFF(void);
void Display_OFF(void);

void Display_Handler(void);
void Display_code(u8 *pDisplayBuf);
//void Display_Lightness_UPD(void);

#endif //#ifndef __DISPLAY_H__