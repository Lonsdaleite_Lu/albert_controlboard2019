#include "key.h"
//#include "stm8s_uart1.h"
#include <string.h>
#include "display.h"
#include "beep.h"
#include "timebase.h"
#include "stm8s_i2c.h"
// #include <stdbool.h>
#include "stm8s.h"
#include "global_macro.h"
#include "I2C_Function.h"
#include "flexible_button.h"
#include "eeprom.h"
#include "error.h"
#include "extendkeys.h"

#define IIC_FREQUENCY 5000 // unit: Hz
//#define KEY_COMMAND_READ_KEY (u8)0x11

#define KEY_SHORT_PRESS_ACTIVE_COUNTER 10
#define KEY_LONG_PRESS_ACTIVE_COUNTER 30

#define SENSITIVITY_RETRY_TIME_MAX 5
#define READKEYCOMMUNICATION_NUM 3 // Xx 10ms
//#define KeyPressTime 100          // single key sensitivity
//#define KeyLongPressRepeatTime 50 // 10ms x 30

typedef struct _tKey
{
  bool bTK0;
  bool bTK1;
  bool bTK2;
  bool bTK3;
  bool bTK4;
  bool bTK5;
  bool bTK6;
  bool bTK7;
} tKey;

typedef struct _tKey_RxTx_Buffer
{
  u8 ucKey_Tx_Config[KEY_SENSITIVY_CONFIG_BUFFE_RSIZE];
  u8 ucKey_Rx_Config;
  u8 ucKey_Rx_ScanFeedback[KEY_RESPONSE_BUFFER_SIZE];
  u8 ucKey_Rx_ScanFeedBackBuffer;
} tKey_RxTx_Buffer;

typedef struct _tKey_WorkingState
{
  bool bReadingKeyWorkingState;
  bool bReadingCorrect;
  u8 ucNumberOfKeys;
  u8 ucKeyDelayCounter;
  u8 ucKeyRepeatCounter;
} tKey_WorkingState;

typedef struct _tKey_Control
{
  bool bKeyWork;
  bool bSingleKey;
  bool bDualKeys;
  bool bkeyLongPress;
} tKey_Control;

typedef enum
{
  USER_BUTTON_Lock = 0,
  USER_BUTTON_Setting,
  USER_BUTTON_AutoP1,
  USER_BUTTON_AutoP2,
  USER_BUTTON_Up,
  USER_BUTTON_Down,
  USER_BUTTON_Power,
#ifdef ExtendKeys
  USER_BUTTON_ExtendPower,  
  USER_BUTTON_ExtendUp,
  USER_BUTTON_ExtendDown,
#endif // ExtendKeys
  USER_BUTTON_MAX
} user_button_t;

static flex_button_t user_button[USER_BUTTON_MAX];

static uint8_t common_btn_read(void *arg);
static uint8_t common_extendkey_read(void *arg);
static void common_btn_evt_cb(void *arg);
// Key state
static tKey sKey;

// communication
static tKey_RxTx_Buffer sKey_RxTx_Buffer;
static u8 ucKey_RxTx_BufferIndex;
static u8 ucReadErrCount;
static u8 ucSensitivity_retry_count = 0;
// check key working state
static tKey_WorkingState sKey_WorkingState;
static tKey_Control sKey_Control;
//static u8 ucKeyPressingCounter;

// Configurate sensitivity flag
static bool bSetSensitivityCommand;

//static u8 ucKey;
static u8 ucKeyResponse[KEY_RESPONSE_BUFFER_SIZE] = {0, 0};
static u8 ucKey_Rx_Scan_Buf[KEY_RESPONSE_BUFFER_SIZE]; // = {0, 0}; KEY_RESPONSE_BUFFER_SIZE
static u8 ucKey_Rx_Feedback_Buf[KEY_RESPONSE_BUFFER_SIZE];
static u8 ucKeyResponseIndex;
I2C_Event_TypeDef Event = I2C_EVENT_MASTER_MODE_SELECT;
static u8 key_short_press_counter;
static u8 ucKeyState;
static u8 ucKeyState_test;

/***************************************************************************/
/*                          static Function prototype                      */
/***************************************************************************/
static void InitKeyState(void);
static u8 ucGetPressedKeyNumbers(void);
static u8 ucGetPressCounter(const u8 ucCnt);
static void user_button_init(void);

/***************************************************************************/
/*                               static Function                           */
/***************************************************************************/
static void InitKeyState(void)
{
  memset(&sKey, 0, sizeof(tKey));
  memset(&sKey_WorkingState, 0, sizeof(tKey_WorkingState));
  memset(&sKey_Control, 0, sizeof(tKey_Control));
}

static void InitKeyReadInfo(void)
{
  memset(&sKey_RxTx_Buffer, 0, sizeof(tKey_RxTx_Buffer));
  sKey_RxTx_Buffer.ucKey_Rx_Config = KEY_COMMAND_SENSITIVITY_FEEDBACK;
}

static u8 ucGetPressedKeyNumbers(void)
{
  u8 ucKeyIndex;
  u8 ucKeyNumberPressing = 0;
  u8 ucKeyState = sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[0];
  for (ucKeyIndex = 0; ucKeyIndex < KEY_TOTAL_NUMBER; ucKeyIndex++)
  {
    if (((ucKeyState >> ucKeyIndex) & 0x01) == 0)
    {
      // do nothing
    }
    else
    {
      ucKeyNumberPressing++;
    }
  }
  return ucKeyNumberPressing;
}

static void ReadKeyWorkingState(void)
{
  if (sKey_WorkingState.bReadingKeyWorkingState == TRUE)
  {
    // bool bCommunicationFinished = FALSE;
    if (bSetSensitivityCommand == FALSE)
    {
      // read key state
      // if (ucKey_RxTx_BufferIndex < KEY_RESPONSE_BUFFER_SIZE)
      // sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[ucKey_RxTx_BufferIndex] = I2C_ReI2CReadByteceiveData();
      // ErrorStatus i;
      //GPIO_WriteHigh(GPIOD, GPIO_PIN_4);
      //GPIO_WriteLow(GPIOD, GPIO_PIN_4);

      //https://blog.csdn.net/u012388993/article/details/77100875
      if (I2CGeneralReadData(IIC_SLAVE_ADDRESS7, ucKey_Rx_Scan_Buf, KEY_RESPONSE_BUFFER_SIZE))
      {
        sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[0] = ucKey_Rx_Scan_Buf[0];
        sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[1] = ucKey_Rx_Scan_Buf[1];
        //TEST
        if (ucKey_Rx_Scan_Buf[0] != 0)
        {
          nop();
          //Beep_OneShortBeep(); //testing
        }

        ucKey_Rx_Scan_Buf[0] = 0;
        ucKey_Rx_Scan_Buf[1] = 0;
        // check key value
        u8 ucKeyScanFeedbackReverse = ~(sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[1]);
        if (sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[0] == ucKeyScanFeedbackReverse)
        {
          // Communication success
          // ucKey_RxTx_BufferIndex = 0;
          // read key finish
          // bCommunicationFinished = TRUE;
          sKey_WorkingState.bReadingCorrect = TRUE;
        }
        else
        {
          // Communication fail
          // TODO: add error timeout
          nop();
        }
        // reset index
      }
    }
    else
    {
      // read config command feedback
      if (TimeBase_Get250msSystemTimeDelta())
      {
        if (I2CWriteandReadData(IIC_SLAVE_ADDRESS7, eeprom_config.key_sensitivity_config_Buf, KEY_SENSITIVY_CONFIG_BUFFE_RSIZE, ucKey_Rx_Feedback_Buf, KEY_RESPONSE_BUFFER_SIZE))
        {
          sKey_RxTx_Buffer.ucKey_Rx_Config = ucKey_Rx_Feedback_Buf[0];
          eeprom_config.key_sensitivity_feedback = sKey_RxTx_Buffer.ucKey_Rx_Config;

          if (sKey_RxTx_Buffer.ucKey_Rx_Config == KEY_COMMAND_SENSITIVITY_FEEDBACK)
          {
            // Set config success
            Beep_TwoShortBeep();
            bSetSensitivityCommand = FALSE;
            eeprom_config.bkey_sensitivity_need_update = FALSE;
            writeconfig();
          }
          else
          {
            //I2CInit(); // not need
            if (SENSITIVITY_RETRY_TIME_MAX < ucSensitivity_retry_count++)
            {
              eeprom_config.bkey_sensitivity_need_update = TRUE; // True if need update always, Flase: update one time after set
              set_warning(KEY_SENSITIVY_CONFIG_FAIL);
              writeconfig(); // store error in eeprom
              bSetSensitivityCommand = FALSE;
            }
          }
        }
      }
    }
  }
}

/***************************************************************************/
/*                               Output Function                           */
/***************************************************************************/
// Key init function
// should be invoked in system init
void Key_Init(void)
{
#ifdef ExtendKeys
  ExtendKey_Init();
#endif //ExtendKeys
  /* init I2C   -----------------------------------------*/
  I2CInit();
  // ----------  Key state init  ----------
  //
  //memset(&sKey,0,sizeof(tKey));
  InitKeyState();
  //
  // InitKeyReadInfo();

  bSetSensitivityCommand = FALSE;
  if (eeprom_config.bkey_sensitivity_need_update)
    bSetSensitivityCommand = TRUE;

  // eKey_PressMode = eKey_Idle;
  //
  // default: enable read key state after init
  sKey_WorkingState.bReadingKeyWorkingState = TRUE;

  // key event try run
  user_button_init();
}

// iic comminucation with SCT80S16B
// sent command to config Key sensitity / read Key state feedback
// here uses iic event interrupt, function will be invoked in I2C_IRQHandler

void Key_IIC_IRQHandler(void) // not used
{
  /* Read SR2 register to get I2C error */ //状态寄存器2(I2C_SR2)用来标记通信是否发生错误
  while (!I2CCheckERREN())
  {
    I2CInit();
  }
  if (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
  {
    I2C_Cmd(DISABLE);
    I2CSoftStop();
    I2C_Cmd(ENABLE);
  }
  if (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
  {
    return;
  }

  IWDG_ReloadCounter();
  // I2C_Event_TypeDef
  Event = I2C_GetLastEvent();

  switch (Event)
  {
  case I2C_EVENT_MASTER_MODE_SELECT: // EV5
  {
    // Check master mode
    // if send Config Command, it should be in Tx Mode
    // otherwise, it should be set as Rx Mode
    I2C_Direction_TypeDef eIIC_Direction;
    if (bSetSensitivityCommand == TRUE)
    {
      eIIC_Direction = I2C_DIRECTION_TX;
    }
    else
    {
      eIIC_Direction = I2C_DIRECTION_RX;
    }
    //
    // send slave address & communication direction
    I2C_Send7bitAddress(IIC_SLAVE_ADDRESS7, eIIC_Direction);
    // I2C_ClearFlag(I2C_FLAG_ADDRESSSENTMATCHED);
  }
  break;
  case I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED: // EV6
  {
    /* Send the first Data */
    u8 ucLocalData;
    if (bSetSensitivityCommand == TRUE)
    {
      ucLocalData = sKey_RxTx_Buffer.ucKey_Tx_Config[ucKey_RxTx_BufferIndex];
      ucKey_RxTx_BufferIndex++;

      //send data
      I2C_SendData(ucLocalData);
    }
  }
  break;
  case I2C_EVENT_MASTER_BYTE_TRANSMITTING: // EV8
  {
    if (bSetSensitivityCommand == TRUE)
    {
      u8 ucLocalData;
      if (ucKey_RxTx_BufferIndex < KEY_SENSITIVY_CONFIG_BUFFE_RSIZE)
      {
        ucLocalData = sKey_RxTx_Buffer.ucKey_Tx_Config[ucKey_RxTx_BufferIndex];
        ucKey_RxTx_BufferIndex++;
        //send data
        I2C_SendData(ucLocalData);
      }
      else
      {
        // reset index
        ucKey_RxTx_BufferIndex = 0;
        // send slave address & communication direction
        //I2C_Send7bitAddress(IIC_SLAVE_ADDRESS7, I2C_DIRECTION_RX);
      }
    }
  }
  break;
  case I2C_EVENT_MASTER_BYTE_TRANSMITTED: // EV8_2
  {
    if (bSetSensitivityCommand == TRUE)
    {
      I2C_Send7bitAddress(IIC_SLAVE_ADDRESS7, I2C_DIRECTION_RX);
    }
  }
  break;
  case I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED: // EV6
  {
    // do nothing

    // waiting for EV7
  }
  break;
  case I2C_EVENT_MASTER_BYTE_RECEIVED: // EV7
  {
    bool bCommunicationFinished = FALSE;
    if (bSetSensitivityCommand == FALSE)
    {
      // read key state
      if (ucKey_RxTx_BufferIndex < KEY_RESPONSE_BUFFER_SIZE)
      {
        sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[ucKey_RxTx_BufferIndex] = I2C_ReceiveData();
      }
      else
      {
        // check key value
        u8 ucKeyScanFeedbackReverse = ~(sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[1]);
        if (sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[0] == ucKeyScanFeedbackReverse)
        {
          // Communication success
        }
        else
        {
          // Communication fail
          // TODO: add error timeout
        }
        // reset index
        ucKey_RxTx_BufferIndex = 0;
        // read key finish
        bCommunicationFinished = TRUE;
        sKey_WorkingState.bReadingCorrect = TRUE;
      }
    }
    else
    {
      // read config command feedback
      sKey_RxTx_Buffer.ucKey_Rx_Config = I2C_ReceiveData();
      if (sKey_RxTx_Buffer.ucKey_Rx_Config == KEY_COMMAND_SENSITIVITY_FEEDBACK)
      {
        // Set config success
      }
      else
      {
        // Set config fail
      }
      //
      // communication finish
      bCommunicationFinished = TRUE;
    }

    if (bCommunicationFinished == TRUE)
    {
      // read key finished, release iic bus
      // generator iic stop situation
      I2C_GenerateSTOP(ENABLE);
      // disable isr
      I2C->SR1;
      I2C->SR3;
      // I2C_ITConfig((I2C_IT_EVT | I2C_IT_BUF), DISABLE);
    }
  }
  break;
  case I2C_EVENT_SLAVE_ACK_FAILURE:
  {
    // error
  }
  break;

  default:
  {
    I2C->SR1;
    I2C->SR3;
    I2C->DR;
  }
  break;
  }
}

bool Key_bIsKeyWork(void)
{
  return sKey_Control.bKeyWork;
}
/*************************************************************************************************************************************************************************************************

函数名称 ： Key_ConfirmKeyWithLongkeyRepetition
功    能 ： 
参    数 ： 
            
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void Key_ConfirmKeyWithLongkeyRepetition(bool blongkeyrepetition)
{
  Key_ConfirmKey();
  // if (blongkeyrepetition)
  //   bkeyrepetition = TRUE;
  // else
  // {
  //   bkeyrepetition = FALSE;
  // }

  // Beep_OneShortBeep();
}
bool Key_bIsSingleKeyWithNoError(void)
{
  return sKey_Control.bSingleKey;
}

bool Key_bIsDoubleKeyWithNoError(void)
{
  return sKey_Control.bDualKeys;
}

bool Key_bIsSingleKeyLongPress(void)
{
  //
}

void Key_ConfirmKey(void)
{
  memset(&sKey_Control, 0, sizeof(tKey_Control));
  memset(&sKey, 0, sizeof(tKey));
}

bool Key_bLock(void)
{
  return sKey.bTK0;
}

bool Key_bSetting(void)
{
  return sKey.bTK1;
}

bool Key_bAutoP1(void)
{
  return sKey.bTK2;
}

bool Key_bAutoP2(void)
{
  return sKey.bTK3;
}

bool Key_bUp(void)
{
  return sKey.bTK4;
}

bool Key_bDown(void)
{
  return sKey.bTK5;
}

bool Key_bPower(void)
{
  return sKey.bTK6;
}

bool key_bKeyPressLongF(void)
{
  return sKey_Control.bkeyLongPress;
}

void Key_Handler(void)
{
  // memset(&sKey,0,sizeof(tKey));
  // the key state will be checked each 10ms
  if (TimeBase_Get10msSystemTimeDelta())
  {
    // Key_SendReadKeyCmd();
    // Display_SetDispalyInfo(ucKeyState);
    // Key_SetKey();
    // ReadKeyWorkingState();
    // try event of button
    flex_button_scan();
    // Key_IIC_IRQHandler();
  }

  return;
  // !!!!!!!!!!!!!  IN coding
  if (sKey_WorkingState.bReadingCorrect == TRUE)
  {
    sKey_RxTx_Buffer.ucKey_Rx_ScanFeedBackBuffer = sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[0];
    memset(&sKey, 0, sizeof(tKey));
    ucKeyState = sKey_RxTx_Buffer.ucKey_Rx_ScanFeedBackBuffer;

    // check key pressed counter
    if (sKey_RxTx_Buffer.ucKey_Rx_ScanFeedBackBuffer != sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[0])
    {
      // key changed, clear counter
      // key_short_press_counter = KEY_SHORT_PRESS_ACTIVE_COUNTER;
    }
    else
    {
      // if (key_short_press_counter >KEY_SHORT_PRESS_ACTIVE_COUNTER )
      {
        /* code */

        if (ucKeyState & 0x01)
        {
          sKey.bTK0 = TRUE;
        }
        else if (ucKeyState & 0x02)
        {
          sKey.bTK1 = TRUE;
        }
        else if (ucKeyState & 0x04)
        {
          sKey.bTK2 = TRUE;
        }
        else if (ucKeyState & 0x08)
        {
          sKey.bTK3 = TRUE;
        }
        else if (ucKeyState & 0x10)
        {
          sKey.bTK4 = TRUE;
        }
        else if (ucKeyState & 0x20)
        {
          sKey.bTK5 = TRUE;
        }
        else if (ucKeyState & 0x40)
        {
          sKey.bTK6 = TRUE;
        }
        // u8 ucPressedKeyNumber = ucGetPressedKeyNumbers();
        // if (ucPressedKeyNumber >= 1)
        // {

        // }
        // switch (ucPressedKeyNumber)
        // {
        //   case 1:
        //   {

        //   }
        // }
      }
    }

    tKey_Control sLocalKeysControl;
    memset(&sLocalKeysControl, 0, sizeof(tKey_Control));
    //
    static u8 ucPressedKeyNumber;
    ucPressedKeyNumber = ucGetPressedKeyNumbers();
    if ((ucPressedKeyNumber >= 1) && (sKey_WorkingState.ucNumberOfKeys <= ucPressedKeyNumber)) //original >= a bug?
    {
      if (sKey_WorkingState.ucKeyDelayCounter > 0)
      {
        if (TimeBase_Get10msSystemTimeDelta())
        {
          sKey_WorkingState.ucKeyDelayCounter--;
        }

        //
        if (sKey_WorkingState.ucKeyDelayCounter == 0)
        {
          sLocalKeysControl.bKeyWork = TRUE;
          switch (ucPressedKeyNumber)
          {
          case 1:
          {
            sLocalKeysControl.bSingleKey = TRUE;
          }
          break;
          case 2:
          {
            sLocalKeysControl.bDualKeys = TRUE;
          }
          break;
          default:
            nop();
          }
          //

          sKey_Control = sLocalKeysControl;
          sKey_WorkingState.ucNumberOfKeys = ucPressedKeyNumber;
        }
      }
      else // short key ok but still pressing
      {
        //
        sLocalKeysControl = sKey_Control;

        if (sKey_WorkingState.ucKeyRepeatCounter < KEY_REPEAT_COUNTER_LIMIT)
        {
          sKey_WorkingState.ucKeyRepeatCounter++;
        }
        if (sKey_WorkingState.ucKeyRepeatCounter == KEY_REPEAT_COUNTER_LIMIT)
        {
          sLocalKeysControl.bkeyLongPress = TRUE;
          sKey_Control = sLocalKeysControl;
        }
      }
    }
    else // key number changed
    {
      sKey_WorkingState.ucKeyDelayCounter = KEY_DELAY_COUNTER_LIMIT;
      sKey_WorkingState.ucKeyRepeatCounter = 0;
      sLocalKeysControl.bkeyLongPress = FALSE;
      sKey_Control = sLocalKeysControl;
    }
    //sKey_Control = sLocalKeysControl;

    //
    sKey_WorkingState.bReadingCorrect = FALSE;
  }
}

bool check_sensitive_inrange(uint8_t *data, uint8_t data_cout)
{
  bool bInrange = TRUE;
  uint8_t *p_data_cal;
  p_data_cal = data;
  for (uint8_t i = 0; i < data_cout; i++)
  {
    if ((*p_data_cal > KEY_SENSITIVITY_MAX) || (*p_data_cal < KEY_SENSITIVITY_MIN))
    {
      bInrange = FALSE;
      return bInrange;
    }
    p_data_cal++;
  }
  return bInrange;
}

bool check_sensitive_changed(uint8_t *data1, uint8_t *data2, uint8_t data_cout)
{
  bool bChanged = FALSE;
  uint8_t *p_data_cal1, *p_data_cal2;
  p_data_cal1 = data1;
  p_data_cal2 = data2;
  for (uint8_t i = 0; i < data_cout; i++)
  {
    if (*p_data_cal1 != *p_data_cal2)
    {
      bChanged = TRUE;
      return bChanged;
    }
    p_data_cal1++;
    p_data_cal2++;
  }
  return bChanged;
}

// event key try run*************************************

static void user_button_init(void)
{
  int i;

  memset(&user_button[0], 0x0, sizeof(user_button));

  for (i = 0; i < USER_BUTTON_MAX; i++)
  {
    user_button[i].id = i;
    user_button[i].usr_button_read = common_btn_read;
    user_button[i].cb = common_btn_evt_cb;
    user_button[i].pressed_logic_level = 1;
    user_button[i].short_press_start_tick = FLEX_MS_TO_SCAN_CNT(KEY_SHORT_PRESS_SCAN_CNT);   //100ms
    user_button[i].long_press_start_tick = FLEX_MS_TO_SCAN_CNT(KEY_LONG_PRESS_SCAN_CNT);     //1.2s
    user_button[i].long_hold_start_tick = FLEX_MS_TO_SCAN_CNT(KEY_LONG_HOLD_PRESS_SCAN_CNT); //3s

#ifdef ExtendKeys
    user_button[i].usr_button_read = common_extendkey_read;

    if ((i == USER_BUTTON_ExtendPower) ||
        (i == USER_BUTTON_ExtendDown) ||
        (i == USER_BUTTON_ExtendUp))
    {
      user_button[i].id = i;
      user_button[i].pressed_logic_level = 0;
    }

#endif // ExtendKeys

    flex_button_register(&user_button[i]);
  }
}
#ifdef ExtendKeys
static uint8_t common_extendkey_read(void *arg)
{
  uint8_t value = 0;

  flex_button_t *btn = (flex_button_t *)arg;

  switch (btn->id)
  {
  case 0:
    ReadKeyWorkingState();
    value = sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[0];
    if (sKey_WorkingState.bReadingCorrect != TRUE)
      value = 0;
    break;
  case USER_BUTTON_ExtendPower:
    value = Read_Extend_Key(EXTEND_KEY_POWER);
    break;
  case USER_BUTTON_ExtendDown:
    value = Read_Extend_Key(EXTEND_KEY_MINUS);
    break;
  case USER_BUTTON_ExtendUp:
    value = Read_Extend_Key(EXTEND_KEY_PLUS);
    break;

  default:
  }
  return value;
}
#endif // ExtendKeys

static uint8_t common_btn_read(void *arg)
{
  uint8_t value = 0;

  flex_button_t *btn = (flex_button_t *)arg;
  ReadKeyWorkingState();

  if (sKey_WorkingState.bReadingCorrect == TRUE)
  {
    value = sKey_RxTx_Buffer.ucKey_Rx_ScanFeedback[0];
  }
  else
  {
    return 0;
  }
  return value;
}

static void common_btn_evt_cb(void *arg)
{
  // FLEX_BTN_PRESS_DOUBLE_CLICK,
  // FLEX_BTN_PRESS_SHORT_START,
  // FLEX_BTN_PRESS_SHORT_UP,
  // FLEX_BTN_PRESS_LONG_START,
  // FLEX_BTN_PRESS_LONG_UP,
  // FLEX_BTN_PRESS_LONG_HOLD,
  // FLEX_BTN_PRESS_LONG_HOLD_UP,
  // flex_button_t *btn = (flex_button_t *)arg;
  // printf("id: [%d - %s]  event: [%d - %30s]  repeat: \n");
  switch (GetPressedKeyNumbers())
  {
  case 1:
  {
    sKey_Control.bSingleKey = TRUE;

    if (flex_button_event_read(&user_button[USER_BUTTON_Lock]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK0 = TRUE;
      //Beep_OneShortBeep();
      Autopower_Key_Lock_setting();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_Lock]) == FLEX_BTN_PRESS_LONG_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK0 = TRUE;
      sKey_Control.bkeyLongPress = TRUE;
      Autopower_LongKey_Lock_setting();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_Setting]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK1 = TRUE;
      Autopower_Key_Set_setting();
      //Beep_OneShortBeep();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_Setting]) == FLEX_BTN_PRESS_LONG_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK1 = TRUE;
      sKey_Control.bkeyLongPress = TRUE;
      Autopower_LongKey_Set_setting();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_AutoP1]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK2 = TRUE;
      Autopower_Key_P1_setting();
      //Beep_OneShortBeep();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_AutoP1]) == FLEX_BTN_PRESS_LONG_HOLD)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK2 = TRUE;
      sKey_Control.bkeyLongPress = TRUE;
      Autopower_LongKey_P1_setting();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_AutoP2]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK3 = TRUE;
      Autopower_Key_P2_setting();
      //Beep_OneShortBeep();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_AutoP2]) == FLEX_BTN_PRESS_LONG_HOLD)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK3 = TRUE;
      sKey_Control.bkeyLongPress = TRUE;
      Autopower_LongKey_P2_setting();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_Up]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK4 = TRUE;
      Autopower_Key_up_setting();
      //Beep_OneShortBeep();
    }

    if ((flex_button_event_read(&user_button[USER_BUTTON_Up]) == FLEX_BTN_PRESS_LONG_START)) // ||
                                                                                             // (flex_button_event_read(&user_button[USER_BUTTON_Up]) == FLEX_BTN_PRESS_LONG_HOLD))
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK4 = TRUE;
      sKey_Control.bkeyLongPress = TRUE;
      Autopower_LongKey_up_setting();
    }

    if ((flex_button_event_read(&user_button[USER_BUTTON_Up]) == FLEX_BTN_PRESS_LONG_UP) ||
        (flex_button_event_read(&user_button[USER_BUTTON_Up]) == FLEX_BTN_PRESS_LONG_HOLD_UP))
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK4 = FALSE;
      sKey_Control.bkeyLongPress = FALSE;
      // Autopower_LongKey_up_setting();
    }
    if (flex_button_event_read(&user_button[USER_BUTTON_Down]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK5 = TRUE;
      Autopower_Key_Down_setting();
      //Beep_OneShortBeep();
    }

    if ((flex_button_event_read(&user_button[USER_BUTTON_Down]) == FLEX_BTN_PRESS_LONG_START)) // ||
                                                                                               // (flex_button_event_read(&user_button[USER_BUTTON_Down]) == FLEX_BTN_PRESS_LONG_HOLD))
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK5 = TRUE;
      sKey_Control.bkeyLongPress = TRUE;
      Autopower_LongKey_Down_setting();
    }

    if ((flex_button_event_read(&user_button[USER_BUTTON_Down]) == FLEX_BTN_PRESS_LONG_UP) ||
        (flex_button_event_read(&user_button[USER_BUTTON_Down]) == FLEX_BTN_PRESS_LONG_HOLD_UP))
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK5 = FALSE;
      sKey_Control.bkeyLongPress = FALSE;
      // Autopower_LongKey_Down_setting();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_Power]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK6 = TRUE;
      Autopower_Key_Power_setting();
      //Beep_OneShortBeep();
    }

    if (flex_button_event_read(&user_button[USER_BUTTON_Power]) == FLEX_BTN_PRESS_LONG_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK6 = TRUE;
      sKey_Control.bkeyLongPress = TRUE;
    }

#ifdef ExtendKeys
    if (flex_button_event_read(&user_button[USER_BUTTON_ExtendPower]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK6 = TRUE;
      Autopower_Key_Power_setting();
    }

  if (flex_button_event_read(&user_button[USER_BUTTON_ExtendUp]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK4 = TRUE;
      Autopower_Key_up_setting();
    }

    if ((flex_button_event_read(&user_button[USER_BUTTON_ExtendUp]) == FLEX_BTN_PRESS_LONG_START))
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK4 = TRUE;
      sKey_Control.bkeyLongPress = TRUE;
      Autopower_LongKey_up_setting();
    }

    if ((flex_button_event_read(&user_button[USER_BUTTON_ExtendUp]) == FLEX_BTN_PRESS_LONG_UP) ||
        (flex_button_event_read(&user_button[USER_BUTTON_ExtendUp]) == FLEX_BTN_PRESS_LONG_HOLD_UP))
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK4 = FALSE;
      sKey_Control.bkeyLongPress = FALSE;
    }
    if (flex_button_event_read(&user_button[USER_BUTTON_ExtendDown]) == FLEX_BTN_PRESS_SHORT_START)
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK5 = TRUE;
      Autopower_Key_Down_setting();
    }

    if ((flex_button_event_read(&user_button[USER_BUTTON_ExtendDown]) == FLEX_BTN_PRESS_LONG_START))
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK5 = TRUE;
      sKey_Control.bkeyLongPress = TRUE;
      Autopower_LongKey_Down_setting();
    }

    if ((flex_button_event_read(&user_button[USER_BUTTON_ExtendDown]) == FLEX_BTN_PRESS_LONG_UP) ||
        (flex_button_event_read(&user_button[USER_BUTTON_ExtendDown]) == FLEX_BTN_PRESS_LONG_HOLD_UP))
    {
      sKey_Control.bKeyWork = TRUE;
      sKey.bTK5 = FALSE;
      sKey_Control.bkeyLongPress = FALSE;
    }

#endif //ExtendKeys
  }
  break;
  case 2:
  {
    sKey_Control.bDualKeys = TRUE;
    if ((flex_button_event_read(&user_button[USER_BUTTON_AutoP1]) == FLEX_BTN_PRESS_LONG_HOLD) &&
        (flex_button_event_read(&user_button[USER_BUTTON_Down]) == FLEX_BTN_PRESS_LONG_HOLD))
    {
      Autopower_LongKey_P1_Clear_setting();
    }
    if ((flex_button_event_read(&user_button[USER_BUTTON_AutoP2]) == FLEX_BTN_PRESS_LONG_HOLD) &&
        (flex_button_event_read(&user_button[USER_BUTTON_Down]) == FLEX_BTN_PRESS_LONG_HOLD))
    {
      Autopower_LongKey_P2_Clear_setting();
    }
    if ((flex_button_event_read(&user_button[USER_BUTTON_Up]) == FLEX_BTN_PRESS_LONG_HOLD) &&
        (flex_button_event_read(&user_button[USER_BUTTON_Down]) == FLEX_BTN_PRESS_LONG_HOLD))
    {
      Autopower_LongKey_Time_PlusMinus_Clear();
    }

#ifdef ExtendKeys

#endif //ExtendKeys
    Key_ConfirmKey();
  }
  break;
  default:
    Key_ConfirmKey();
  }
}
