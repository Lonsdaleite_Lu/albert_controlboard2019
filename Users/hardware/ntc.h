#ifndef __NTC_H__
#define __NTC_H__

#include "stm8s.h"
#include "config.h"

#define NTC_GPIO_JX7 GPIO_PIN_2
#define NTC_GPIO_JX8 GPIO_PIN_3
#define NTC_ADCH_JX7 ADC1_CHANNEL_2
#define NTC_ADCH_JX8 ADC1_CHANNEL_3

#if defined(NTC)
void NTC_Init(void);
void NTC_Handler(void);
#endif // KNOB

#endif // !__NTC_H__