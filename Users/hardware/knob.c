#include "knob.h"
#include "stm8s_adc1.h"
#include "display.h"
#include "stdlib.h"

#define N 12 // filter
#define REF_VTG 5
#define FULL_SCALE 1024 


static u16 ucKnob_ADvalue, ucKnob_voltage;
static u16 value_buf[N];
static u8 count;

static u16 get_ad(void);
static u16 KnobADC_filter(void);

void Knob_Init(void)
{
  // /* De-Init ADC peripheral*/
  // ADC1_DeInit();
  // /*  Init GPIO for ADC1 */
  GPIO_Init(GPIOB, GPIO_PIN_0, GPIO_MODE_IN_FL_NO_IT);

  // /* Init ADC2 peripheral */
  // ADC1_Init(ADC1_CONVERSIONMODE_SINGLE,
  //           ADC1_CHANNEL_0,
  //           ADC1_PRESSEL_FCPU_D2,
  //           ADC1_EXTTRIG_TIM,
  //           DISABLE,
  //           ADC1_ALIGN_RIGHT,
  //           ADC1_SCHMITTTRIG_CHANNEL0,
  //           DISABLE);
  // /* Enable EOC interrupt */
  // // ADC1_ITConfig(ADC1_IT_EOCIE, ENABLE);
  // ADC1_Cmd(ENABLE); //开启ADC

  ADC_Init();
}

static u16 KnobADC_filter(void)
{
  u8 i, j;

  u16 temp, sum = 0;

  value_buf[count++] = get_ad();
  if (count == N)
  {
    count = 0;
  }

  for (j = 0; j < N - 1; j++)
  {
    for (i = 0; i < N - j; i++)
    {
      if (value_buf[i] > value_buf[i + 1])
      {
        temp = value_buf[i];
        value_buf[i] = value_buf[i + 1];
        value_buf[i + 1] = temp;
      }
    }
  }
  for (i = 1; i < N - 1; i++)
  {
    sum += value_buf[i];
  }
  return (u16)(sum / (N - 2));
}



static u16 get_ad(void)
{
  // uint8_t i;
  // uint16_t adc_value = 0;
  // for (i = 0; i < 10; i++)
  // {
  //   ADC1_StartConversion(); //启动AD转换
  //   while (RESET == ADC1_GetFlagStatus(ADC1_FLAG_EOC))
  //   {
  //     /* code */
  //     nop();
  //   }
  //   ADC1_ClearFlag(ADC1_FLAG_EOC); //等待转换完成，并清除标志

  //   adc_value += ADC1_GetConversionValue(); //读取转换结果
  // }

  // adc_value = adc_value / 10; //求平均
  // return adc_value;
  return Get_ADCCH_Avr_value(ADC1_CHANNEL_0);
}

void Knob_Handler(void)
{

  if (TimeBase_Get250msSystemTimeDelta())
  {
    ucKnob_ADvalue = KnobADC_filter();
    ucKnob_voltage = ucKnob_ADvalue *5 ;  // ucKnob_voltage = (((float)ucKnob_ADvalue * (float)REF_VTG))/((float)FULL_SCALE);
    Display_SetDispalyInfo(ucKnob_voltage); // testing
  }
}

u16 GetKnobvoltage (void)
{
  return ucKnob_voltage;
}
