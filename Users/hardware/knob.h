#ifndef __KNOB_H__
#define __KNOB_H__

#include "stm8s.h"
#include "config.h"
#include "adc.h"

#if defined(KNOB)
void Knob_Init(void);

void Knob_Handler(void);
u16 GetKnobvoltage (void);
#endif // KNOB
#endif // !__KNOB_H__