#include "beep.h"
//#include "stm8s_beep.h"
#include "timebase.h"
#include "config.h"

#define SHORT_BEEP_COUNTER 5
#define LONG_BEEP_COUNTER 50
#define BEEP_INTERVAL_COUNTER 25

static u8 ucBeepingCounter[TotalBeepTime];
static u8 ucNonBeepingCounter[TotalBeepTime];

static bool bIsBeeping;

static u8 ucBeepingRepeaTime;

static void Beep_StartBeeping(tBeepType eBeepType, u8 ucRepeatTime);

static void Beep_StartBeeping(tBeepType eBeepType, u8 ucRepeatTime)
{
  // GPIO_WriteHigh(GPIOD, GPIO_PIN_4);
  BEEP_Cmd(ENABLE);
  bIsBeeping = TRUE;
  ucBeepingRepeaTime = ucRepeatTime;
  u8 ucLocalBeepingCounter = 0;
  if (eBeepType == ShortBeep)
  {
    ucLocalBeepingCounter = SHORT_BEEP_COUNTER;
  }
  else
  {
    ucLocalBeepingCounter = LONG_BEEP_COUNTER;
  }

  int i;
  for (i = 0; i < TotalBeepTime; i++)
  {
    if (i < ucRepeatTime)
    {
      ucBeepingCounter[i] = ucLocalBeepingCounter;
    }
    else
    {
      ucBeepingCounter[i] = 0;
    }
  }

  for (i = 0; i < TotalBeepTime - 1; i++)
  {
    if (i < ucRepeatTime - 1)
    {
      ucNonBeepingCounter[i] = BEEP_INTERVAL_COUNTER;
    }
    else
    {
      ucNonBeepingCounter[i] = 0;
    }
  }
}

void Beep_Init(void)
{
  //BEEP_DeInit();
  //BEEP_LSICalibrationConfig(LSIMeasurment());

#ifdef Buzzer_Active
  // GPIO_Init(GPIOD, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOD, GPIO_PIN_4, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_WriteLow(GPIOD, GPIO_PIN_4);
#endif // Buzzer_Active

#ifdef Buzzer_Passive

  BEEP_DeInit();
  BEEP_LSICalibrationConfig(150000);
  BEEP_Init(BEEP_FREQUENCY_4KHZ); //BEEP_FREQUENCY_1KHZ BEEP_FREQUENCY_2KHZ BEEP_FREQUENCY_4KHZ

  BEEP_Cmd(DISABLE);
#endif // Buzzer_Passive
}

void Beep_OneShortBeep(void)
{
  Beep_StartBeeping(ShortBeep, 1);
}

void Beep_OneLongBeep(void)
{
  Beep_StartBeeping(LongBeep, 1);
}

void Beep_ThreeLongBeep(void)
{
  Beep_StartBeeping(LongBeep, 3);
}

void Beep_TwoShortBeep(void)
{
  Beep_StartBeeping(ShortBeep, 2);
}

void Beep_Handler(void)
{
  static u8 ucRepeatTime = 0;
  if (bIsBeeping == TRUE)
  {
    if (TimeBase_Get10msSystemTimeDelta())
    {
      if (ucBeepingCounter[ucRepeatTime] != 0)
      {
        ucBeepingCounter[ucRepeatTime]--;
        if (ucBeepingCounter[ucRepeatTime] == 0)
        {
#ifdef Buzzer_Active
          GPIO_WriteLow(GPIOD, GPIO_PIN_4);
#endif // Buzzer_Active
#ifdef Buzzer_Passive
          BEEP_Cmd(DISABLE);
#endif // Buzzer_Passive
        }
      }
      else
      {
        if (ucNonBeepingCounter[ucRepeatTime] != 0)
        {
          ucNonBeepingCounter[ucRepeatTime]--;
        }
        else
        {
          ucRepeatTime++;
          if (ucRepeatTime >= ucBeepingRepeaTime)
          {
            bIsBeeping = FALSE;
            ucRepeatTime = 0;
// GPIO_WriteLow(GPIOD, GPIO_PIN_4);
#ifdef Buzzer_Active
            GPIO_WriteLow(GPIOD, GPIO_PIN_4);
#endif // Buzzer_Active
#ifdef Buzzer_Passive
            BEEP_Cmd(DISABLE);
#endif // Buzzer_Passive
          }
          else
          {
// GPIO_WriteHigh(GPIOD, GPIO_PIN_4);
// BEEP_Cmd(ENABLE);
#ifdef Buzzer_Active
            GPIO_WriteHigh(GPIOD, GPIO_PIN_4);
#endif // Buzzer_Active
#ifdef Buzzer_Passive
            BEEP_Cmd(ENABLE);
#endif // Buzzer_Passive
          }
        }
      }
    }
  }
}
