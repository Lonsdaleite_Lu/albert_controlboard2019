#ifndef __BEEP_H__
#define __BEEP_H__

#include "stm8s.h"
#include "stm8s_beep.h"


typedef enum _tBeepCounter
{
  BeepingCounter1,
  BeepingCounter2,
  BeepingCounter3,
  TotalBeepTime
}_tBeepCounter;

typedef enum _tBeepType
{
  ShortBeep,
  LongBeep
}tBeepType;

/*
#define Beep_OptionAdd  0x4803
#define FLASH_OPTIONBYTE_ERROR  ((uint16_t)0x5555)
#define OPERATION_TIMEOUT  ((uint32_t)0xFFFFF)
#define LSI_128kHz 128000
#define BEEP_FREQUENCY_2KHZ 0x40
#define NEAR __near
*/

void Beep_Init(void);

void Beep_OneShortBeep(void);
void Beep_OneLongBeep(void);
void Beep_TwoShortBeep(void);
void Beep_ThreeLongBeep(void);

void Beep_Handler(void);


#endif //#ifndef __BEEP_H__