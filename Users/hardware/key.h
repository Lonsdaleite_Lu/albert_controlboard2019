#ifndef __KEY_H__
#define __KEY_H__

#include "stm8s.h"

#define KEY_SENSITIVY_CONFIG_BUFFE_RSIZE 9
#define KEY_RESPONSE_BUFFER_SIZE 2
#define KEY_TOTAL_NUMBER 8 // max key num from chip
#define KEY_ACTUAL_NUMBER 7 // actual key num
//no use
#define KEY_DELAY_COUNTER_LIMIT 6u    // 6*10ms = 60 ms change to 2*250ms = 500 ms : 10ms for IIC interrupt too much
#define KEY_REPEAT_COUNTER_LIMIT 120u // 120*10ms = 1.2 s change to 5*250ms = 1.25 ms : 10ms for IIC interrupt too much
//for event handling
#define KEY_SHORT_PRESS_SCAN_CNT 100u      //ms
#define KEY_LONG_PRESS_SCAN_CNT 1200u      //ms
#define KEY_LONG_HOLD_PRESS_SCAN_CNT 2400u //ms

// Key sensitivity is defined 15 levels totally
//                0x01   0x02 .....0x0e   0x0f
// sensitivity:     High   ---_---->>   Low
//                               TK0    TK1  TK2   TK3   TK4   TK5   TK6  TK7   checksum
#define KEY_DEFAUT_SENSITIVITY                               \
    {                                                        \
        0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0X40 \
    }

#define KEY_COMMAND_SENSITIVITY_FEEDBACK 0X53
#define KEY_SENSITIVITY_MAX 0x0F
#define KEY_SENSITIVITY_MIN 0x01

void Key_Init(void);
void Key_IIC_IRQHandler(void);

bool check_sensitive_inrange(uint8_t *data, uint8_t data_cout);
bool check_sensitive_changed(uint8_t *data1,uint8_t *data2, uint8_t data_cout);
bool Key_bLock(void);
bool Key_bSetting(void);
bool Key_bAutoP1(void);
bool Key_bAutoP2(void);
bool Key_bUp(void);
bool Key_bDown(void);
bool Key_bPower(void);
bool key_bKeyPressLongF(void);
void Key_ConfirmKey(void);
void Key_ConfirmKeyWithLongkeyRepetition(bool blongkeyrepetition);
bool Key_bIsSingleKeyWithNoError(void);
bool Key_bIsDoubleKeyWithNoError(void);
void Key_Handler(void);

#endif //#ifndef __KEY_H__