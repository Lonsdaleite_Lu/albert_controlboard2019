/**
  ******************************************************************************
  * @file     display.c
  * @author   AL
  * @version 
  * @date     
  * @brief    
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "display.h"
#include "stm8s_tim2.h"
//#include "utils.h"
#include "beep.h"
#include <string.h>
#include "timebase.h"
#include "eeprom.h"
#include "global_macro.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

#define NUM_OF_LED_Blink 6
#define Blink_count_max 100
#define Blink_count_num 50
#define TIM2_PERIOD 399
#define SEG_REFRESH_COUNTER 10
#define Seg_updateErrCount 100
#define BlinkValueDP1Bit 0x10  // LED1, LED2, DP2, DP1, DLED1, DLED2, DLED3, DLED4
#define BlinkValueDP2Bit 0x20  // LED1, LED2, DP2, DP1, DLED1, DLED2, DLED3, DLED4
#define BlinkValueLED1Bit 0x80 // LED1, LED2, DP2, DP1, DLED1, DLED2, DLED3, DLED4
#define BlinkValueLED2Bit 0x40 // LED1, LED2, DP2, DP1, DLED1, DLED2, DLED3, DLED4

/* Private macro -------------------------------------------------------------*/
#define ClrLED1() ClrBit(uiUpdateInfo[0], 7); //LED1
#define ClrLED2() ClrBit(uiUpdateInfo[1], 7); //LED2
#define ClrDP1() ClrBit(uiUpdateInfo[2], 7);  //DP1
#define ClrDP2() ClrBit(uiUpdateInfo[3], 7);  //DP2
#define SetLED1() SetBit(uiUpdateInfo[0], 7); //LED1
#define SetLED2() SetBit(uiUpdateInfo[1], 7); //LED2
#define SetDP1() SetBit(uiUpdateInfo[2], 7);  //DP1
#define SetDP2() SetBit(uiUpdateInfo[3], 7);  //DP2

#define AllDark {Display_Dark, Display_Dark, Display_Dark, Display_Dark};
/* Private variables ---------------------------------------------------------*/

const u8 ucDisplayNumber[Total_Display_Num] = {DISPLAY_NUMBER};
const u16 ucDisplayLIGHT[Total_DisplayLight_Num] = {DISPLAY_LIGHT};

static bool bSegReadyToDisplay = FALSE;
//static bool bSendDisplayConfig = FALSE;
//static bool bDisplayConfigUpdated = FALSE;
//static bool bDisplayInfoUpdating = FALSE;
//static bool bSendOneCommandFinish = FALSE;
//static u16 uiDisplayInfo[NUM_OF_LED] = {0x6800, 0x6A00, 0x6C00, 0x6E00};
static u16 uiDisplayInfoBuffer[NUM_OF_LED] = {0x6800, 0x6A00, 0x6C00, 0x6E00};
const u8 ucDislaySegID[NUM_OF_LED] = {0x68, 0x6A, 0x6C, 0x6E};
static u8 uiDisplayValueinputOld[NUM_OF_LED];
static u16 uiDisplayValueinput;
static u16 uiDisPlayConfigCommand;
static u8 Seg_updateErrCount1;
static u8 Blink_count;
static u8 SEGBrightnessLevel;
static u16 SYSONstatus = SYSOFF;
u8 uiDIG0;
u8 uiDIG1;
u8 uiDIG2;
u8 uiDIG3;
static u16 SEGLightstatus; // = SYSON_1_SEG8;
static u8 SEGLightSaveR[EEPROM_SEGLightEEpsize];
u16 ucDisplayLIGHTTemp;
//const u8 ucDisplayCode[NUM_OF_LED] = {Display_Dark, Display_Dark, Display_Dark, Display_Dark};
static u8 ucDisplayCode[NUM_OF_LED];
//u8 a;
//static bool bDisplayInfoBufferNeedUpdate;
static bool bDisplayInfoNeedUpdate;
static bool bRefreshDisplay;
static bool bDisplayingOneValue;
static bool bDisplayUpdatingOneSeg;
static bool Blink_F;
static bool bDisplayOriginalF;
static bool SEGLightSaveNoErrF;
static bool bUpdateDisplayInfoBuffer = FALSE;
// the seg will be refreshed regularly
// 10ms * 100 = 1s
// each 1 sec, the display will be refreshed
// if display information is updated, this counter will be reload
static u8 ucDisplayRefreshCounter; // = SEG_REFRESH_COUNTER;
//static bool bDisplayConfigNeedUpdat;
//static u16 uiConfigCommandInfo;
static u8 ucDIGIndex = 0;
//static u16 uiSendBuffer = 0xAAAA;
//static u8 ucSendBufferIndex = 0;
//static tSegState eSegState = SegIdle;

//static tUpdateInfoProcess eConfigUpdateProcess;
//static tUpdateInfoProcess eDisplayInfoUpdateProcess;
//static bool bConfig_CAP_IRQ = FALSE;
//static bool bConfig_UPD_IRQ = FALSE;

static u16 uiSendCommandBuffer = {0xAAAA};
static u8 ucSendCommandBufferIndex;

static tSendCommandState eSendCommandState = SendCommand_IdleState;
static bool bClkState;
static bool bStartSendingCommand;
//static bool bStartSendingCommand2;
static bool bStartSendingBits;

static tSegState eSegState = Seg_IdleState;

static tSendCommandConfig sSendCommandConfig;
static u8 BlinkValue; // LED1, LED2, DP2, DP1, DLED1, DLED2, DLED3, DLED4
static bool DP1 = FALSE;
static bool DP2 = FALSE;
static bool LED1 = FALSE;
static bool LED2 = FALSE;

extern eeprom_config_s eeprom_config;
// testing
static u8 ucTestIndex;
const u16 uiSegCheck[10] = {0000, 1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888, 9999};

/* Private function prototypes -----------------------------------------------*/
//static tSendCommandState Display_eSendCommandHigh(void);
//static tSendCommandState Display_eSendCommandLow(void);
static void SetClkHigh(void);
static void SetClkLow(void);
static void SetClkRev(void);
static void Display_Brightness_Init(void);
static void Display_ReStart(void);
/* Private functions ----------------------------------------------------------*/

/*
static tSendCommandState Display_eSendCommandHigh(void){
  tSendCommandState eReturnValue = SendingBits;
  u8 ucHighByte = (u8)(uiSendBuffer >> 8);
  u8 ucSendState = 0;;
  if( ucSendBufferIndex < 8 )
  {
    if(ucSendBufferIndex == 0)
    {
      ucSendState = ucHighByte & 0x80;
    }
    else
    {
      ucSendState = ((ucHighByte << ucSendBufferIndex) & 0x80);
    }
    if(ucSendState == 0)
    {
      // Set data low
      GPIO_WriteLow(GPIOD,GPIO_PIN_0);
    }
    else
    { 
      // Set data high
      GPIO_WriteHigh(GPIOD,GPIO_PIN_0);
    }
      // bit index counter 
      ucSendBufferIndex++;
  }
  else
  {
    ucSendBufferIndex = 0;
    eReturnValue = SendByteComplete;
  }
  
  // return state send state
  return eReturnValue;
}

static tSendCommandState Display_eSendCommandLow(void){
  mSetDataOutputLow();
  tSendCommandState eReturnValue = SendingBits;
  u8 ucLowByte = (u8)(uiSendBuffer & 0x00FF);
  u8 ucSendState = 0;
  if( ucSendBufferIndex < 8 )
  {
    if(ucSendBufferIndex == 0)
    {
      ucSendState = ucLowByte & 0x80;
    }
    else
    {
      ucSendState = (ucLowByte << ucSendBufferIndex) & 0x80;
    }
    if(ucSendState == 0)
    {
      // Set data low
      GPIO_WriteLow(GPIOC,GPIO_PIN_3);
    }
    else
    { 
      // Set data high
      GPIO_WriteHigh(GPIOC,GPIO_PIN_3);
    }
      // bit index counter 
      ucSendBufferIndex++;
  }
  else
  {
    ucSendBufferIndex = 0;
    eReturnValue = SendByteComplete;
  }
  
  // return state send state
  return eReturnValue;
}
*/
static void Display_ReStart(void)
{
  bStartSendingCommand = FALSE;
  eSegState = Seg_IdleState;
  eSendCommandState = SendCommand_IdleState;
  bDisplayInfoNeedUpdate = FALSE;
  bRefreshDisplay = FALSE;
  bDisplayingOneValue = FALSE;
  bDisplayUpdatingOneSeg = FALSE;
}

static void Display_Start(void)
{
  bStartSendingCommand = TRUE;

  if (eSegState == Seg_Working)
  {
    if (bRefreshDisplay == TRUE)
    {
      eSegState = Seg_Refresh;
    }
  }
  //
  switch (eSegState)
  {
  case Seg_IdleState:
  {
    if (sSendCommandConfig == Command_Idle)
    {
      uiDisPlayConfigCommand = SYSONstatus; //SYSON;
      //bDisplayInfoBufferNeedUpdated = TRUE;
      sSendCommandConfig = Command_Send;
      eSendCommandState = SendCommand_Start;
    }

    if (sSendCommandConfig == Command_Updated)
    {
      eSegState = Seg_SystemOn;
      sSendCommandConfig = Command_Idle;
    }
    //Beep_OneShortBeep();
  }
  break;
  case Seg_SystemOn:
  {
    if (sSendCommandConfig == Command_Idle)
    {
      uiDisPlayConfigCommand = SEGLightstatus; //SYSON_1_SEG8; //SYSON_5_SEG8;//SEG8_ON;     uiDisPlayConfigCommand = SEGLightstatus; //SEGLightstatus; // ; //SYSON_1_SEG8; //SYSON_5_SEG8;//SEG8_ON;
      //bDisplayInfoBufferNeedUpdated = TRUE;
      sSendCommandConfig = Command_Send;
      eSendCommandState = SendCommand_Start;
    }

    if (sSendCommandConfig == Command_Updated)
    {
      bSegReadyToDisplay = TRUE;
      eSegState = Seg_Working;
      sSendCommandConfig = Command_Idle;
    }
  }
  break;
  case Seg_Enable:
  {
    //
  }
  break;
  case Seg_Working:
  {
    //
  }
  break;
  case Seg_Disable:
  {
    //
  }
  break;
  case Seg_Refresh:
  {
    if (bDisplayingOneValue == FALSE)
    {
      if (bRefreshDisplay == TRUE)
      {
        if (eSendCommandState == SendCommand_IdleState)
        {
          //eSendCommandState = SendCommand_Start;
          bRefreshDisplay = FALSE;
          bDisplayingOneValue = TRUE;
          eSegState = Seg_OneValueUpdating;
        }
      }
      //
      if (bDisplayInfoNeedUpdate == TRUE)
      {
        if (eSendCommandState == SendCommand_IdleState)
        {
          //memcpy(uiDisplayInfo, &uiDisplayInfoBuffer, sizeof(uiDisplayInfo)); //* 4
          //eSendCommandState = SendCommand_Start;
          bDisplayInfoNeedUpdate = FALSE;
          //
          //
          bDisplayingOneValue = TRUE;
          eSegState = Seg_OneValueUpdating;
        }
      }
    }
  }
  break;
  case Seg_OneValueUpdating:
  {
    //bool bUpdate = FALSE;
    if (bDisplayingOneValue == TRUE)
    {
      if (bDisplayUpdatingOneSeg == FALSE)
      {
        eSendCommandState = SendCommand_Start;
        bDisplayUpdatingOneSeg = TRUE;
        Seg_updateErrCount1 = 0;
      }
      else
      {
        Seg_updateErrCount1++;
        if (Seg_updateErrCount1 > Seg_updateErrCount)
        {
          Seg_updateErrCount1 = 0;
          //eSegState = Seg_IdleState;
          eSegState = Seg_SystemOn;
        }
      }
    }
    else
    {
      eSegState = Seg_Working;
    }
  }
  break;
  default:
    nop();
  }
}

static void SetClkHigh(void)
{
  mSetClkHigh();
  bClkState = CLK_HIGH;
}

static void SetClkLow(void)
{
  mSetClkLow();
  bClkState = CLK_LOW;
}

static void SetClkRev(void)
{
  mSetClkRev();
  if (bClkState == CLK_HIGH)
  {
    bClkState = CLK_LOW;
  }
  else
  {
    bClkState = CLK_HIGH;
  }
}

static void Display_UpdateDisplayInfoBuffer(void)
{

  // bool bUpdateDisplayInfoBuffer = FALSE;
  // if (uiDisplayValueinputOld != uiDisplayValueinput)
  // {
  //   bUpdateDisplayInfoBuffer = TRUE;
  //   uiDisplayValueinputOld = uiDisplayValueinput;
  // }

  // if (bUpdateDisplayInfoBuffer == TRUE)
  // {
  // }

  uiDIG0 = uiDisplayValueinput / 1000;         // LED 1
  uiDIG1 = (uiDisplayValueinput % 1000) / 100; // LED 2
  uiDIG2 = (uiDisplayValueinput % 100) / 10;   // LED 3
  uiDIG3 = uiDisplayValueinput % 10;           // LED 4

  if (uiDIG0 == Display_0)
  {
    uiDIG0 = Display_Dark;
    if (uiDIG1 == Display_0)
    {
      uiDIG1 = Display_Dark;
      if (uiDIG2 == Display_0)
        uiDIG2 = Display_Dark;
    }
  }

  if (bDisplayOriginalF)
  {
    // u16 uiUpdateInfo[NUM_OF_LED] = ucDisplayCode;
    //  memcpy(uiUpdateInfo, &ucDisplayCode, sizeof(uiUpdateInfo));
    uiDIG0 = ucDisplayCode[0];
    uiDIG1 = ucDisplayCode[1];
    uiDIG2 = ucDisplayCode[2];
    uiDIG3 = ucDisplayCode[3];
  }

  u8 uiUpdateInfo[NUM_OF_LED] = {ucDisplayNumber[uiDIG0], ucDisplayNumber[uiDIG1], ucDisplayNumber[uiDIG2], ucDisplayNumber[uiDIG3]};

  u8 j;
  for (j = 0; j < NUM_OF_LED; j++)
  {
    if ((BlinkValue >> j) & 0x1)
    {
      if (Blink_F)
      {
        uiUpdateInfo[NUM_OF_LED - 1 - j] = ucDisplayNumber[Display_Dark];
      }
    }
  }

  ClrLED1();
  ClrLED2();
  ClrDP1();
  ClrDP2();

  if (DP1)
  {
    SetDP1();
    if (BlinkValue & BlinkValueDP1Bit)
    {
      if (Blink_F)
      {
        ClrDP1();
      }
    }
  }

  if (DP2)
  {
    SetDP2();
    if (BlinkValue & BlinkValueDP2Bit)
    {
      if (Blink_F)
      {
        ClrDP2();
      }
    }
  }

  if (LED1)
  {
    SetLED1();
    if (BlinkValue & BlinkValueLED1Bit)
    {
      if (Blink_F)
      {
        ClrLED1();
      }
    }
  }

  if (LED2)
  {
    SetLED2();
    if (BlinkValue & BlinkValueLED2Bit)
    {
      if (Blink_F)
      {
        ClrLED2();
      }
    }
  }

  u8 count;

  for (count = 0; count < NUM_OF_LED; count++)
  {
    if (uiUpdateInfo[count] != uiDisplayValueinputOld[count])
    {
      bUpdateDisplayInfoBuffer = TRUE;
      uiDisplayValueinputOld[count] = uiUpdateInfo[count];
    }
  }

  if (bUpdateDisplayInfoBuffer == TRUE)
  {

    for (count = 0; count < NUM_OF_LED; count++)
    {
      uiDisplayInfoBuffer[count] = ((u16)(ucDislaySegID[count] << 8)) | (uiUpdateInfo[count] & 0x00FF);
    }

    bDisplayInfoNeedUpdate = TRUE;
  }
}

/* Public functions ----------------------------------------------------------*/

void Display_Init(void)
{
  // clk init
  // use TIM2 pwm to simulate clk
  // Deinitializes the TIM2 peripheral registers to their default reset values.
  TIM2_DeInit();

  // TIM2 configuration:
  // Communication frequuency 10kHz: T = 1/10000 = 100us
  // TIM2 Prescaler is equal to 2 16/2 = 8MHZ
  // TIM2 Period = 50*8-1 = 399
  TIM2_TimeBaseInit(TIM2_PRESCALER_2, TIM2_PERIOD);

  // Clear TIM2 update flag
  TIM2_ClearFlag(TIM2_FLAG_UPDATE);

  /* PWM1 Mode configuration: Channel1 */
  // DO NOT USE PMW METHOD
  //TIM2_OC1Init(TIM2_OCMODE_PWM1, TIM2_OUTPUTSTATE_ENABLE,400, TIM2_OCPOLARITY_HIGH);
  //TIM2_OC1PreloadConfig(ENABLE);

  //TIM2_OC2Init(TIM2_OCMODE_PWM1, TIM2_OUTPUTSTATE_ENABLE,200, TIM2_OCPOLARITY_HIGH);
  //TIM2_OC2PreloadConfig(ENABLE);

  //TIM2_OC3Init(TIM2_OCMODE_PWM1, TIM2_OUTPUTSTATE_ENABLE,200, TIM2_OCPOLARITY_HIGH);
  //TIM2_OC3PreloadConfig(ENABLE);

  //TIM2_ARRPreloadConfig(ENABLE);

  //TIM2_CCxCmd(TIM2_CHANNEL_1 , ENABLE);   //濠电姷鏁搁崑鐘诲箵椤忓棛绀婇柍褜鍓熼弻娑欐償閳藉棛鍚嬮悗瑙勬礃椤ㄥ懓鐏冪紓鍌氱▉閺侊拷2_CH3PWM闂傚倷绀侀幖顐λ囬鐐村亱濠电姴娲ょ粻浼存煙闂傚顦﹂柛姘秺閺屻劑鎮ら崒娑橆伓
  //TIM2_CCxCmd(TIM2_CHANNEL_2 , ENABLE);   //濠电姷鏁搁崑鐘诲箵椤忓棛绀婇柍褜鍓熼弻娑欐償閳藉棛鍚嬮悗瑙勬礃椤ㄥ懓鐏冪紓鍌氱▉閺侊拷2_CH3PWM闂傚倷绀侀幖顐λ囬鐐村亱濠电姴娲ょ粻浼存煙闂傚顦﹂柛姘秺閺屻劑鎮ら崒娑橆伓
  //TIM2_CCxCmd(TIM2_CHANNEL_3 , ENABLE);   //濠电姷鏁搁崑鐘诲箵椤忓棛绀婇柍褜鍓熼弻娑欐償閳藉棛鍚嬮悗瑙勬礃椤ㄥ懓鐏冪紓鍌氱▉閺侊拷2_CH3PWM闂傚倷绀侀幖顐λ囬鐐村亱濠电姴娲ょ粻浼存煙闂傚顦﹂柛姘秺閺屻劑鎮ら崒娑橆伓

  /* TIM2 enable counter */
  TIM2_Cmd(ENABLE);

  // TIM2 interrupt config
  TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);

  //TIM2_ITConfig(TIM2_IT_CC1,ENABLE);

  //
  //
  // data pin init
  //GPIO_Init(GPIOC, GPIO_PIN_3,GPIO_MODE_OUT_PP_HIGH_SLOW/*GPIO_MODE_OUT_PP_LOW_SLOW*/);
  GPIO_Init(GPIOD, GPIO_PIN_0, GPIO_MODE_OUT_PP_HIGH_FAST);
  //mSetDataHigh();

  // CLK pin init
  GPIO_Init(GPIOD, GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_FAST);
  bClkState = CLK_HIGH;
  // test use
  //GPIO_Init(GPIOD , GPIO_PIN_3 , GPIO_MODE_OUT_PP_LOW_FAST);
  SYSONstatus = SYSON;
  Display_Brightness_Init();
}

void Display_Brightness_Init(void)
{
  // EEPROM_ReadNByteBuf(SEGLightSaveR, EEPROM_SEGLightEEpaddress, EEPROM_SEGLightEEpsize);
  // ucDisplayLIGHTTemp = ((u16)(SEGLightSaveR[0] << 8)) | (SEGLightSaveR[1] & 0x00FF);
  ucDisplayLIGHTTemp = eeprom_config.SEG_birghtness_config;
  for (u8 i = 0; i < Total_DisplayLight_Num; i++)
  {
    if (ucDisplayLIGHT[i] == ucDisplayLIGHTTemp)
    {
      SEGLightSaveNoErrF = TRUE;
      SEGBrightnessLevel = i + 1;
      SEGLightstatus = ucDisplayLIGHTTemp;
    }
  }
  if (SEGLightSaveNoErrF == FALSE)
  {
    SEGLightSaveR[1] = SYSON_1_SEG8 & 0x00FF;
    SEGLightSaveR[0] = 0x48;
    SEGLightSaveNoErrF = FALSE;
    SEGBrightnessLevel = 1;
    SEGLightstatus = ((u16)(SEGLightSaveR[0] << 8)) | (SEGLightSaveR[1] & 0x00FF);
  }

  // EEPROM_WriteNByteBuf(SEGLightSaveR, EEPROM_SEGLightEEpaddress, EEPROM_SEGLightEEpsize);

  eeprom_config.SEG_birghtness_config = SEGLightstatus;
}

bool Display_Brightness_Level_Check(u16 ucDisplayLIGHTTempBuf)
{
  bool bSEGLightNoErrF;
  u8 BrightnessLevel;
  for (u8 i = 0; i < Total_DisplayLight_Num; i++)
  {
    if (ucDisplayLIGHT[i] == ucDisplayLIGHTTempBuf)
    {
      bSEGLightNoErrF = TRUE;
      SEGBrightnessLevel = i + 1;
      return bSEGLightNoErrF;
    }
  }
  SEGBrightnessLevel = 1;
  return bSEGLightNoErrF;
}

void Display_Brightness_UPD(u8 Increase_or_Decrease)
{
  // EEPROM_ReadNByteBuf(SEGLightSaveR, EEPROM_SEGLightEEpaddress, EEPROM_SEGLightEEpsize);
  // eeprom_config_s tSEGLightness_config;
  ucDisplayLIGHTTemp = eeprom_config.SEG_birghtness_config;
  u8 cont;
  // ucDisplayLIGHTTemp = ((u16)(SEGLightSaveR[0] << 8)) | (SEGLightSaveR[1] & 0x00FF);
  for (u8 i = 0; i < Total_DisplayLight_Num; i++)
  {
    cont = 0;
    if (ucDisplayLIGHT[i] == ucDisplayLIGHTTemp) // i = current Brightness
    {
      if (Increase_or_Decrease >= 1)
      {
        if (i >= (Total_DisplayLight_Num - 1))
        {
          cont = 0;
        }
        else
        {
          cont = i + 1;
          i = Total_DisplayLight_Num;
        }
      }
      else
      {
        if (i <= 0)
        {
          cont = Total_DisplayLight_Num - 1;
        }
        else
        {
          cont = i - 1;
        }
        i = Total_DisplayLight_Num;
      }
    }
  }
  SEGLightSaveR[1] = ucDisplayLIGHT[cont] & 0x00FF;
  SEGLightSaveR[0] = 0x48; //ucDisplayLIGHT[cont+1]>> 8;
  SEGBrightnessLevel = cont + 1;

  // EEPROM_WriteNByteBuf(SEGLightSaveR, EEPROM_SEGLightEEpaddress, EEPROM_SEGLightEEpsize);

  SEGLightstatus = ((u16)(SEGLightSaveR[0] << 8)) | (SEGLightSaveR[1] & 0x00FF);
  eeprom_config.SEG_birghtness_config = SEGLightstatus;
  Display_ReStart();
  //eSegState =Seg_IdleState;// Seg_SystemOn;
}

u8 Display_Brightness_level(void)
{
  return SEGBrightnessLevel;
}

void Display_SendCommand_UPD_IRQ(void)
{
  // record current clk state
  //bClkState = !bClkState;
  if (bStartSendingBits == TRUE)
  {
    SetClkRev();
  }

  if (bStartSendingCommand == TRUE)
  {
    switch (eSendCommandState)
    {
    case SendCommand_IdleState:
    {
      if (eSegState == Seg_OneValueUpdating)
      {
        nop();
      }
      if (bClkState == CLK_LOW)
      {
        SetClkHigh();
      }

      //
      //if( bStartSendingCommand2 == TRUE )
      //{
      //mSetDataLow();
      //eSendCommandState = SendCommand_Start;
      //}
    }
    break;
    case SendCommand_Start:
    {
      //if( bClkState == CLK_HIGH)
      //{
      mSetDataLow();

      //
      //
      //update display info
      if (sSendCommandConfig == Command_Send)
      {
        uiSendCommandBuffer = uiDisPlayConfigCommand;
        sSendCommandConfig = Command_Updating;
      }
      else
      {
        if (bDisplayingOneValue == TRUE)
        {
          uiSendCommandBuffer = uiDisplayInfoBuffer[ucDIGIndex]; //uiDisplayInfo

          if (ucDIGIndex < NUM_OF_LED - 1)
          {
            ucDIGIndex++;
          }
          else
          {
            ucDIGIndex = 0;
          }
        }
      }

      SetClkLow();

      //bClkState = CLK_LOW;
      bStartSendingBits = TRUE;

      eSendCommandState = SendCommand_SendHighByte;

      //}
    }
    //break;
    case SendCommand_SendHighByte:
    {
      if (bClkState == CLK_LOW)
      {
        u8 ucSendBit = 0;
        u8 ucSendByte = (u8)(uiSendCommandBuffer >> 8);
        if (ucSendCommandBufferIndex < 8)
        {
          if (ucSendCommandBufferIndex == 0)
          {
            ucSendBit = ucSendByte & 0x80;
          }
          else
          {
            ucSendBit = ((ucSendByte << ucSendCommandBufferIndex) & 0x80);
          }

          ucSendCommandBufferIndex++;
          // send bit
          if (ucSendBit)
          {
            mSetDataHigh();
          }
          else
          {
            mSetDataLow();
          }
        }
        else
        {
          ucSendCommandBufferIndex = 0;
          //mSetDataInput();
          mSetDataLow();
          eSendCommandState = SendCommand_WaitingForACK1;
        }
      }
    }
    break;
    case SendCommand_WaitingForACK1:
    {
      //Delay(1000);
      if (bClkState == CLK_LOW)
      {
        mSetDataLow(); //mSetDataOutputLow();
        eSendCommandState = SendCommand_SendLowByte;
      }
    }
    //break;
    case SendCommand_SendLowByte:
    {
      if (bClkState == CLK_LOW)
      {
        u8 ucSendBit = 0;
        u8 ucSendByte = (u8)(uiSendCommandBuffer);
        if (ucSendCommandBufferIndex < 8)
        {
          if (ucSendCommandBufferIndex == 0)
          {
            ucSendBit = ucSendByte & 0x80;
          }
          else
          {
            ucSendBit = ((ucSendByte << ucSendCommandBufferIndex) & 0x80);
          }

          // send bit
          if (ucSendBit)
          {
            mSetDataHigh();
          }
          else
          {
            mSetDataLow();
          }

          ucSendCommandBufferIndex++;
        }
        else
        {
          ucSendCommandBufferIndex = 0;
          //mSetDataInput();
          mSetDataLow();
          eSendCommandState = SendCommand_WaitingForACK2;
        }
      }
    }
    break;
    case SendCommand_WaitingForACK2:
    {
      //Delay(1000);
      if (bClkState == CLK_LOW)
      {
        mSetDataOutputLow();
        eSendCommandState = SendCommand_Finish;
      }
    }
    break;
    case SendCommand_Finish:
    {
      if (bClkState == CLK_HIGH)
      {
        //Delay(1300);

        bStartSendingBits = FALSE;

        if (sSendCommandConfig == Command_Updating)
        {
          sSendCommandConfig = Command_Updated;
        }

        if (bDisplayingOneValue == TRUE)
        {
          if (ucDIGIndex == 0)
          {
            bDisplayingOneValue = FALSE;
          }
          bDisplayUpdatingOneSeg = FALSE;
        }

        mSetDataHigh();

        // after sending finsh, eSendCommandState should always set as SendCommand_IdleState;
        eSendCommandState = SendCommand_IdleState;
      }
    }
    break;
    }
  }
}

void Display_SetDispalyInfo(u16 uiValue)
{
  if (bSegReadyToDisplay == TRUE)
  {
    bDisplayOriginalF = FALSE;
    uiDisplayValueinput = uiValue;
  }
}

void Display_Handler(void)
{
  // display refresh counter
  if ((eSendCommandState == SendCommand_IdleState) && (eSegState == Seg_Working))
  {
    if (TimeBase_Get10msSystemTimeDelta())
    {
      //bRefreshDisplay = TRUE;
      if (ucDisplayRefreshCounter != 0)
      {
        ucDisplayRefreshCounter--;
      }
      else
      {
        ucDisplayRefreshCounter = SEG_REFRESH_COUNTER;
        bRefreshDisplay = TRUE;
      }
    }
  }

  if (TimeBase_Get10msSystemTimeDelta())
  {
    Blink_count++;
    if (Blink_count > Blink_count_max)
    {
      Blink_count = 0;
    }

    if (Blink_count < Blink_count_num)
    {
      Blink_F = FALSE;
    }
    else
    {
      Blink_F = TRUE;
    }

    //Display_UpdateDisplayInfoBuffer();
  }
  // display information change check
  Display_UpdateDisplayInfoBuffer();
  // update send command info

  // seg state change
  Display_Start();

  // TESTING
  /*
  Display_SetDispalyInfo(uiSegCheck[ucTestIndex]);
  Display_SetLED2_Blink();
  Display_ClrLED1();
  Display_DP_Blink();
  if (TimeBase_Get1sSystemTimeDelta())
  {
    //Display_Brightness_UPD();
    if (ucTestIndex < 9)
    {
      ucTestIndex++;
    }
    else
    {
      ucTestIndex = 0;
    }
  }
  */
}

void Display_Clr_AllBlink(void)
{
  Display_DP_Not_Blink();
  Display_ClrLED1_Blink();
  Display_ClrLED2_Blink();
  Display_Not_Blink(b11111111);
}

void Display_Set_DP(void)
{
  DP1 = TRUE;
  DP2 = TRUE;
}

void Display_Clr_DP(void)
{
  // BlinkValue = 0x0F0;// LED1, LED2, DP2, DP1, DLED1, DLED2, DLED3, DLED4
  DP1 = FALSE;
  DP2 = FALSE;
}

void Display_DP_Blink(void)
{
  // BlinkValue = 0x0F0;// LED1, LED2, DP2, DP1, DLED1, DLED2, DLED3, DLED4
  DP1 = TRUE;
  DP2 = TRUE;
  SetBit(BlinkValue, 4); //4 DP1, 5 DP2, 6 led2, 7 LED1
  SetBit(BlinkValue, 5); //4 DP1, 5 DP2, 6 led2, 7 LED1
}

void Display_DP_Not_Blink(void)
{
  // BlinkValue = 0x0F0;// LED1, LED2, DP2, DP1, DLED1, DLED2, DLED3, DLED4
  DP1 = FALSE;
  DP2 = FALSE;
  ClrBit(BlinkValue, 4); //4 DP1, 5 DP2, 6 led2, 7 LED1
  ClrBit(BlinkValue, 5); //4 DP1, 5 DP2, 6 led2, 7 LED1
}

void Display_Blink(u8 uiDisplayValue)
{
  BlinkValue = BlinkValue | uiDisplayValue;
}
void Display_Not_Blink(u8 uiDisplayValue)
{
  BlinkValue = BlinkValue & (~(uiDisplayValue));
}
void Display_7Seg_Blink(void)
{
  BlinkValue = BlinkValue | 0x0f;
}

void Display_7Seg_Not_Blink(void)
{
  BlinkValue = BlinkValue & 0xf0;
}

void Display_SetLED1(void)
{
  LED1 = TRUE;
}

void Display_ClrLED1(void)
{
  LED1 = FALSE;
  Display_ClrLED1_Blink();
}
void Display_SetLED2(void)
{
  LED2 = TRUE;
}

void Display_ClrLED2(void)
{
  LED2 = FALSE;
  Display_ClrLED2_Blink();
}

void Display_SetLED1_Blink(void)
{
  SetBit(BlinkValue, 7); //4 DP1, 5 DP2, 6 led2, 7 LED1
  LED1 = TRUE;
}

void Display_ClrLED1_Blink(void)
{
  ClrBit(BlinkValue, 7); //4 DP1, 5 DP2, 6 led2, 7 LED1
  // LED1 = FALSE;
}

void Display_SetLED2_Blink(void)
{
  SetBit(BlinkValue, 6); //4 DP1, 5 DP2, 6 led2, 7 LED1
  LED2 = TRUE;
}
void Display_ClrLED2_Blink(void)
{
  ClrBit(BlinkValue, 6); //4 DP1, 5 DP2, 6 led2, 7 LED1
  // LED2 = FALSE;
}

void Display_Code_OFF(void)
{
  bDisplayOriginalF = FALSE;
}

void Display_OFF(void)
{
  Display_Clr_AllBlink();
  bDisplayOriginalF = TRUE;
  u8 i;
  for (i = 0; i < NUM_OF_LED; i++)
  {
    ucDisplayCode[i] = Display_Dark;
  }
}

void Display_code(u8 *pDisplayBuf)
{
  bDisplayOriginalF = TRUE;
  for (u8 i = 0; i < NUM_OF_LED; i++)
  {
    ucDisplayCode[i] = *pDisplayBuf;
    pDisplayBuf++;
  }
}
