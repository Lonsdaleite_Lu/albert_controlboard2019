#include "ntc.h"
#include "stm8s_adc1.h"
#include "display.h"
#include "stdlib.h"
#include "adc.h"

#define N 12 // filter
static u16 ucNTC_ADvalue, ucNTC_voltage;
static u16 NTC_value_buf[N];
static u8 count;

static u16 get_ad(void);
static u16 NTCADC_filter(void);

void NTC_Init(void)
{
  GPIO_Init(GPIOB, NTC_GPIO_JX7, GPIO_MODE_IN_FL_NO_IT);
  GPIO_Init(GPIOB, NTC_GPIO_JX8, GPIO_MODE_IN_FL_NO_IT);
  ADC_Init();
}

static u16 NTCADC_filter(void)
{
  u8 i, j;

  u16 temp, sum = 0;

  NTC_value_buf[count++] = get_ad();
  if (count == N)
  {
    count = 0;
  }

  for (j = 0; j < N - 1; j++)
  {
    for (i = 0; i < N - j; i++)
    {
      if (NTC_value_buf[i] > NTC_value_buf[i + 1])
      {
        temp = NTC_value_buf[i];
        NTC_value_buf[i] = NTC_value_buf[i + 1];
        NTC_value_buf[i + 1] = temp;
      }
    }
  }
  for (i = 1; i < N - 1; i++)
  {
    sum += NTC_value_buf[i];
  }
  return (u16)(sum / (N - 2));
}



static u16 get_ad(void)
{
  return Get_ADCCH_Avr_value(NTC_ADCH_JX7);
}

void NTC_Handler(void)
{
  if (TimeBase_Get10msSystemTimeDelta())
  {
    ucNTC_ADvalue = Get_ADCCH_Avr_value(NTC_ADCH_JX7);
    ucNTC_voltage = ucNTC_ADvalue *5 ;  // ucNTC_voltage = (((float)ucNTC_ADvalue * (float)REF_VTG))/((float)FULL_SCALE);
    // Display_SetDispalyInfo(ucNTC_voltage); // testing
  }
}