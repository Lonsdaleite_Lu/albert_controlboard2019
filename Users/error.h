#pragma once

enum errorcode
{
	NOERROR = 0x0,						// 
	CRYSTAL =  0x1,						// 
	PLL =  0x2,							// 
	SW_PLL =  0x3,						// 
	LSI =  0x4,							// 
	HardFault =  0x5,					// 
	MemManageFault =  0x6,				// 
	BusFault =  0x7,					// 
	UsageFault =  0x8,					// 
	WDTRESET =  0x9,					// watchdog

	OVERHEAT =  0x30,					// 
	NTC_NOT_PRESENT =  0x31,			// 
	NO_HEATER =  0x32,					// 
	BAD_HEATER_RELAY =  0x33,			// 
	HEATER_RELAY_STICKING =  0x34,		// 
	TRY_SET_HEAT_WITHOUT_WATER =  0x35, // 
	SET_HEAT_OVER90 =  0x36, 			// 
	KEY_SENSITIVY_CONFIG_FAIL = 0x37,
	//eeprom
	EEPROM_EMPTY =  0x70,				// 
	EEPROM_BADCRC =  0x71,				// 
	EEPROM_LOCK =  0x72,				// 
	EEPROM_WRITEERROR =  0x73,			// 
};

void set_error(enum errorcode code);
void set_warning(enum errorcode code);