#ifndef __SETTING_CONTROL_H__
#define __SETTING_CONTROL_H__

#include "stm8s.h"

#define NUM_OF_Autopowerdata 10
#define Menu_Display_Numb 1 // the time to display menu info. each x second
#define Autopower_Time_Max 5999 //99min 59s       38340  // 9h99min
#define SEC_STEP_TIMEADD 10   // 10 s
#define MIN_STEP_TIMEADD 60  //1 min

#define LONG_KEY_NUM 80   // long key + 80x10ms then circle add
#define LONG_KEY_MIN_NUM 20 // long key + 25x10ms then circle add fastest
#define LONG_KEY_SPEED_NUM 10

typedef enum _tMenu_State
{
  Menu_InitPara,
  Menu_Auto_Power,
  Menu_Key_sensitivity,
  Menu_Display_Brightness,
  Total_Menu_Num,
} tMenu_State;

typedef enum _tAutoPower_Setting_state
{
  Autopower_setting_InitPara,
  Autopower_setting_Step,
  AutoPower_Setting_Power1,
  AutoPower_Setting_Timing1,
  AutoPower_Setting_Power2,
  AutoPower_Setting_Timing2,
  Total_AutoPower_Setting_Num,
} tAutoPower_Setting_state;

typedef enum _tKeySensitive_Setting_state
{
  KeySensitive_Setting_InitPara,
  KeySensitive_Setting_Key_Num,
  KeySensitive_Setting_Level,
  Total_KeySensitive_Setting_Setting_Num,
} tKeySensitive_Setting_state;

void SettingControl_Handler(void);


void Autopower_Key_Power_setting(void);

void Autopower_Key_Set_setting(void);
void Autopower_LongKey_Set_setting(void);

void Autopower_Key_Lock_setting(void);
void Autopower_LongKey_Lock_setting(void);

void Autopower_Key_P1_setting(void);
void Autopower_LongKey_P1_setting(void);
void Autopower_Key_P2_setting(void);
void Autopower_LongKey_P2_setting(void);

void Autopower_LongKey_P1_Clear_setting(void);
void Autopower_LongKey_P2_Clear_setting(void);
void Autopower_LongKey_Time_PlusMinus_Clear(void);

void Autopower_Key_up_setting(void);
void Autopower_LongKey_up_setting(void);
void Autopower_Key_Down_setting(void);
void Autopower_LongKey_Down_setting(void);


#endif 