#include "powercontrol.h"
#include "settingcontrol.h"
#include "key.h"
#include "beep.h"
#include "display.h"
#include "stm8s.h"
#include "timebase.h"
#include "global_macro.h"
#include "eeprom.h"
#include "autolevel.h"

extern volatile bool bPowerOn;
extern volatile bool bsettingOn;
extern volatile bool bLockOff;
extern volatile bool bAutopowerRun;

extern eeprom_config_s eeprom_config;
extern u8 ucCurrentPowerLevel;

typedef enum _tautopowerline
{
  Line1 = 0,
  Line2 = 1,
} tAutopowerline;

static autolevel Autopowerline;
const tPowerTable PowerTable_s[TOTAL_LEVEL_NUMBER] = POWER_PERCENT_TABLE;
static tAutopowerline Autopowerline_number;
static bool bShowTiming;
static u8 ucShowTime_cout;
static bool bZeroPowerAuto_off;
static u8 ucZeroPower_Count;
static tPwControl_WorkingState ePwControl_WorkingState;
static u8 DisplaycodeData[NUM_OF_LED] = {Display_Dark, Display_Dark, Display_Dark, Display_Dark};
/************************************************************************************************************************************************************************************************/
void Autopower_P_End(void);
void Autopower_P_EachStep(void);
void Restartwithshowtime(void);
/************************************************************************************************************************************************************************************************
函数名称 ： PowerControl_Handler
功    能 ： 
参    数 ： 
            
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
void PowerControl_Handler(void)
{
  //auto power off
  if (bZeroPowerAuto_off)
  {
    bZeroPowerAuto_off = FALSE;
    Autopower_Key_Power_setting();
  }
  //l
  if ((!bLockOff))
  {
    Display_SetLED1();
  }
  else
  {
    Display_ClrLED1();
  }

  if (!bPowerOn)
  {
    ePwControl_WorkingState = PwControl_InitPara;
    //reset powerOn variates
    //*************************************************************************************************/
    ucCurrentPowerLevel = TOTAL_LEVEL_NUMBER - 1; // power set to 0;
    bsettingOn = FALSE;
    // bLockOff = TRUE;
    Autopower_Break();
    //*************************************************************************************************/
    Display_OFF(); //test should cancel//
    return;
  }

  //*************************************************************************************************/ powerOn handler
  if ((bPowerOn) && (!bsettingOn))
  {
    Display_7Seg_Not_Blink();
    Display_Not_Blink(b11111111);
    Display_Clr_DP();
    Display_Code_OFF();
    if (!Autopowerline.bstartF)
    {
      Display_SetDispalyInfo(PowerTable_s[ucCurrentPowerLevel].uiPowerRatio);
    }
    else
    {
      u16 RemainTimingBuf = SetpRemainTime(&Autopowerline);
      if ((!bShowTiming) || ((RemainTime(&Autopowerline)) < ALWAYSSHOWTIME_NUM) || Autopowerline.bAutoPause)
      {
        // Display_SetDispalyInfo(RemainTimingBuf);
        u16 j;
        j = RemainTimingBuf;
        if (j > 5999)
        {
          DisplaycodeData[3] = j / 60 % 10;
          DisplaycodeData[2] = j / 60 % 60 / 10;
          DisplaycodeData[1] = j / 60 / 60 % 10;
          if (j <= ((9 * 60 + 59) * 60))
          {
            DisplaycodeData[0] = Display_H;
          }
          // else
          {
            DisplaycodeData[0] = j / 60 / 60 / 10;
            if (DisplaycodeData[0] == 0)
            {
              DisplaycodeData[0] = Display_Dark;
            }
          }
        }
        else
        {
          DisplaycodeData[3] = j % 10;
          DisplaycodeData[2] = j % 60 / 10;
          DisplaycodeData[1] = j / 60 % 10;
          DisplaycodeData[0] = j / 60 / 10;
        }
        Display_DP_Blink();
        if (Autopowerline.bAutoPause)
        {
          Display_Blink(b00001111);
        }

        Display_code(DisplaycodeData);
      }
      else
      {
        Display_SetDispalyInfo(PowerTable_s[ucCurrentPowerLevel].uiPowerRatio);
        // Display_SetDispalyInfo(Autopowerline.ucAutoLevel_Step);
      }
    }
    switch (ePwControl_WorkingState)
    {
    case PwControl_InitPara:
    {
      ePwControl_WorkingState = PwControl_Idle;
    }
    break;
    case PwControl_Idle:
    {
      ePwControl_WorkingState = PwControl_SendModBusStart;
    }
    break;
    case PwControl_SendModBusStart:
    {

      ePwControl_WorkingState = PwControl_Working;
    }
    break;
    case PwControl_Working:
    {
      if (Autopowerline.bstartF)
      {
        if (TimeBase_Get1sSystemTimeDelta()) //testing 10ms .. after 1s
          ucCurrentPowerLevel = ShowPowerwithReduceTime(&Autopowerline);
      }
    }
    break;
    case PwControl_ErrorState:
    {
    }
    break;
    default:
      nop();
    }

    if (TimeBase_Get1sSystemTimeDelta())
    {

      // zero power power off timing
      if ((ucCurrentPowerLevel == TOTAL_LEVEL_NUMBER - 1) && !bAutopowerRun)
      {
        ucZeroPower_Count++;
      }
      else
      {
        ucZeroPower_Count = 0;
        bZeroPowerAuto_off = FALSE;
      }

      if (ucZeroPower_Count > ZEROPOWERAUTOOFF)
      {
        bZeroPowerAuto_off = TRUE;
        ucZeroPower_Count = 0;
      }

      // Autopwoer display time and power timing
      if (Autopowerline.bstartF)
      {
        if (ucShowTime_cout++ > (AUTOPOWERTIME_SHOW * 2))
        {
          ucShowTime_cout = 0;
        }
        if (ucShowTime_cout < AUTOPOWERTIME_SHOW)
        {
          bShowTiming = FALSE;
        }
        else if (ucShowTime_cout < (AUTOPOWERTIME_SHOW * 2))
        {
          bShowTiming = TRUE;
        }
      }
    }
  }
}

void Restartwithshowtime(void)
{
  bShowTiming = FALSE; // to show time when each step start
  ucShowTime_cout = 0;
}

void Autopower_P_End(void)
{
  Beep_ThreeLongBeep();
  bAutopowerRun = FALSE;
}
void Autopower_P_EachStep(void)
{
  Beep_OneShortBeep();
  Restartwithshowtime();
}



void Autopower_P1_Init(void)
{
  Init_Power_and_Time(&Autopowerline, eeprom_config.Power1_config, eeprom_config.Time1_config);
  Autopowerline_number = Line1;
  Autopowerline.Auto_end = Autopower_P_End;
  Autopowerline.Auto_EachStep = Autopower_P_EachStep;
  bAutopowerRun = TRUE;
  Restartwithshowtime();
}

void Autopower_P2_Init(void)
{
  Init_Power_and_Time(&Autopowerline, eeprom_config.Power2_config, eeprom_config.Time2_config);
  Autopowerline_number = Line2;
  Autopowerline.Auto_end = Autopower_P_End;
  Autopowerline.Auto_EachStep = Autopower_P_EachStep;
  bAutopowerRun = TRUE;
  Restartwithshowtime();
}

void Autopower_P1_Start(void) // start or pause
{
  if ((Autopowerline.bstartF) && (Autopowerline_number == Line1))
  {
    Autopowerline.bAutoPause = !(Autopowerline.bAutoPause);
  }
  else
  {
    Autopower_P1_Init();
    Autopowerline.bstartF = TRUE;
  }
}

void Autopower_P2_Start(void)
{
  if ((Autopowerline.bstartF) && (Autopowerline_number == Line2))
  {
    Autopowerline.bAutoPause = !(Autopowerline.bAutoPause); // pause or recover
  }
  else
  {
    Autopower_P2_Init();
    Autopowerline.bstartF = TRUE;
  }
}

void Autopower_P1_ReStart(void)
{
  Autopower_P1_Init();
  Autopowerline.bstartF = TRUE;
}

void Autopower_P2_ReStart(void)
{
  Autopower_P2_Init();
  Autopowerline.bstartF = TRUE;
}

void Autopower_Break(void)
{
  Autopowerline.bAutoPause = FALSE;
  Autopowerline.bstartF = FALSE;
  bAutopowerRun = FALSE;
  ucCurrentPowerLevel = TOTAL_LEVEL_NUMBER - 1; // power set to 0;
}
void Autopower_Pause(void)
{
  Autopowerline.bAutoPause = TRUE;
}

void Autopower_Pause_Recover(void)
{
  Autopowerline.bAutoPause = FALSE;
}
/************************************************************************************************************************************************************************************************
函数名称 ： PwControl_IncreasePowerLevel
功    能 ： 
参    数 ： ucPowerLevel
            
            
返 回 值 ： ucPowerLevel
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
static u8 PwControl_IncreasePowerLevel(u8 ucPowerLevel)
{
  u8 ucPowerLevel_Buf;
  ucPowerLevel_Buf = ucPowerLevel;
  if (ucPowerLevel_Buf != 0)
  {
    if (ucPowerLevel_Buf == (TOTAL_LEVEL_NUMBER - 1))
    {
      //ucPowerLevel_Buf = TOTAL_LEVEL_NUMBER - Defaultpoweronlevel;
    }
    ucPowerLevel_Buf--;
    Beep_OneShortBeep();
  }
  return ucPowerLevel_Buf;
}
/************************************************************************************************************************************************************************************************
函数名称 ： PwControl_DecreasePowerLevel
功    能 ： 
参    数 ： ucPowerLevel
            
            
返 回 值 ： ucPowerLevel
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
static u8 PwControl_DecreasePowerLevel(u8 ucPowerLevel)
{
  u8 ucPowerLevel_Buf;
  ucPowerLevel_Buf = ucPowerLevel;

  if (ucPowerLevel_Buf < TOTAL_LEVEL_NUMBER - 1)
  {
    ucPowerLevel_Buf++;
    Beep_OneShortBeep();
  }
  else if (ucPowerLevel_Buf == TOTAL_LEVEL_NUMBER - 1)
  {
    // if (key_bKeyPressLongF() == FALSE)
    {
      // ucPowerLevel_Buf = 0;
      Beep_TwoShortBeep();
      Key_ConfirmKey();
    }
  }
  return ucPowerLevel_Buf;
}
