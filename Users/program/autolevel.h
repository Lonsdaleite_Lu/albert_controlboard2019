#ifndef __AUTOLEVEL_H__
#define __AUTOLEVEL_H__

#include "stm8s.h"
#include "settingcontrol.h"



typedef struct autolevel
{
 u8 ucAutolevel_Power_Data[NUM_OF_Autopowerdata]; //= {Default_POWER_Level};
 u16 ucAutolevel_Time_Data[NUM_OF_Autopowerdata]; //= {Default_POWER_Level};
 bool bstartF;
 u8 ucAutoLevel_Step;  // the max non zero time level
 bool bAutoPause;
 void (*Auto_end) (void);   // end to run
 void (*Auto_EachStep) (void); 
} autolevel;

/*init the time and power*/
void Init_Power_and_Time(autolevel * const Auto_P, u8 *ucAuotlevel_Power_Data, u16 *ucAuotlevel_Time_Data);
/*  reduce 1 value of time_data once, then return power step accordingly */
u8 ShowPowerwithReduceTime(autolevel * const Auto_P);
/*show time if >60min*/
u16 RemainTime(autolevel const * const Auto_P);
u16 SetpRemainTime(autolevel const *const Auto_P);
u8 CurrentPowerSetp (autolevel const * const Auto_P);


#endif //#ifndef __AUTOLEVEL_H__