#include "settingcontrol.h"
#include "powercontrol.h"
#include "key.h"
#include "beep.h"
#include "display.h"
#include "stm8s.h"
#include "timebase.h"
#include "global_macro.h"
#include "eeprom.h"
#include "autolevel.h"

extern volatile bool bPowerOn;
extern volatile bool bsettingOn;
extern volatile bool bLockOff;
extern volatile bool bAutopowerRun;
extern eeprom_config_s eeprom_config;
u8 ucCurrentPowerLevel = TOTAL_LEVEL_NUMBER - 1;

typedef enum _set_min_sec
{
  second = 0,
  minute,
} set_min_sec;

const tPowerTable sPowerTable[TOTAL_LEVEL_NUMBER] = POWER_PERCENT_TABLE;
static set_min_sec tTiming_sec_min_select;
static bool bpower_line_setting;
static bool bAutopower_setting_Timing;
static bool bMenu_Display;
static u8 ucMenu_Display_cont;
static u8 ucAutopower_P1_Power_Data[NUM_OF_Autopowerdata] = {Default_POWER_Level};
static u16 ucAutopower_P1_Time_Data[NUM_OF_Autopowerdata]; //= {Default_POWER_Level};
static u8 ucAutopower_P2_Power_Data[NUM_OF_Autopowerdata] = {Default_POWER_Level};
static u16 ucAutopower_P2_Time_Data[NUM_OF_Autopowerdata]; //= {Default_POWER_Level};
static u8 *pAutopower_data;
static u16 *pAutopower_Timing_data;
static u8 ucpwr_time_step_value;
static u8 DisplaycodeData[NUM_OF_LED] = {Display_Dark, Display_Dark, Display_Dark, Display_Dark};
static tMenu_State eMenu_State_old, eMenu_State = Menu_Auto_Power;
static tAutoPower_Setting_state eAutoPower_Setting_state = AutoPower_Setting_Power1,
                                eAutoPower_Setting_state_save;
static tKeySensitive_Setting_state eKeySenitive_Menu;
static u8 key_sensitivity_value_save[KEY_TOTAL_NUMBER];
static u8 key_sensitivity_command_save[KEY_SENSITIVY_CONFIG_BUFFE_RSIZE];
static const u8 key_defaut_sensitivity_command[KEY_SENSITIVY_CONFIG_BUFFE_RSIZE] = KEY_DEFAUT_SENSITIVITY;
static u8 key_sensitive_select_Num;

/************************************************************************************************************************************************************************************************/
void Autopower_setting_display_handler(void);
void Setting_display_handler(void);
void Keysensitive_setting_display_handler(void);
void Long_key_handler(void);

void Copy_data_to_eeprom(void);
void Copy_eeprom_data(void);

static u16 Increase_globall(u16 ucAddup_value, u16 ucAddup_Max_value, u16 ucAddup_Min_value, bool bcircle, u16 ucstep);
static u16 Decrease_globall(u16 ucAddup_value, u16 ucAddup_Max_value, u16 ucAddup_Min_value, bool bcircle, u16 ucstep);
static u8 PwControl_IncreasePowerLevel(u8 ucPowerLevel, bool circle);
static u8 PwControl_DecreasePowerLevel(u8 ucPowerLevel, bool circle);
/*************************************************************************************************************************************************************************************************


/************************************************************************************************************************************************************************************************
函数名称 ： SettingControl_Handler
功    能 ： 
参    数 ： 
            
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
void SettingControl_Handler(void)
{
  Long_key_handler();
  //  bsettingOn = TRUE; // Testing
  //*************************************************************************************************/ setting handler
  if (bsettingOn)
  {
    Setting_display_handler();
    Display_code(DisplaycodeData);
  }
  else
  {
    //reset setting variates
    //*************************************************************************************************/
    eMenu_State = Menu_InitPara;
  }
}
/************************************************************************************************************************************************************************************************
函数名称 ： Setting_display_handler
功    能 ： 
参    数 ： 
            
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
static void Setting_display_handler(void)
{
  // if (eMenu_State != Menu_Auto_Power)
  //   eAutoPower_Setting_state = Autopower_setting_InitPara;
  //*************************************************************************************************/
  switch (eMenu_State)
  {
  case Menu_InitPara:
  {
    eMenu_State_old = Menu_InitPara;
    eAutoPower_Setting_state = Autopower_setting_InitPara;
    eMenu_State = Menu_Auto_Power;
    eKeySenitive_Menu = KeySensitive_Setting_InitPara;
  }
  break;
  //*************************************************************************************************/
  case Menu_Auto_Power:
  {
    //***********************************************************************************************/
    if (bMenu_Display)
    {
      DisplaycodeData[3] = Display_5; //eMenu_State + Menu_Auto_Power; //shows menu
      DisplaycodeData[2] = Display_R;
      DisplaycodeData[1] = Display_W;
      DisplaycodeData[0] = Display_P;
      Display_Not_Blink(b00001111);
      Display_Clr_DP();
    }
    else
    {
      Autopower_setting_display_handler();
    }
    //**********************************************************************************************/
  }
  break;

  case Menu_Display_Brightness:
  {
    if (bMenu_Display)
    {
      DisplaycodeData[3] = Display_P; //eMenu_State + Menu_Auto_Power; //shows menu
      DisplaycodeData[2] = Display_5;
      DisplaycodeData[1] = Display_I;
      DisplaycodeData[0] = Display_D;
      Display_Not_Blink(b00001111);
      Display_Clr_DP();
    }
    else //showing
    {
      DisplaycodeData[3] = Display_Brightness_level();
      DisplaycodeData[2] = Display_Dark;
      DisplaycodeData[1] = Display_V;
      DisplaycodeData[0] = Display_L;
      Display_Not_Blink(b00001111);
      Display_Set_DP();
      Display_ClrLED1(); // not useful here :(
      Display_ClrLED2(); // not useful here :(
    }
  }
  break;
  //**********************************************************************************************/
  case Menu_Key_sensitivity:
  {
    if (bMenu_Display)
    {
      DisplaycodeData[3] = Display_Dark; //eMenu_State + Menu_Auto_Power; //shows menu
      DisplaycodeData[2] = Display_Y;
      DisplaycodeData[1] = Display_E;
      DisplaycodeData[0] = Display_K;
      Display_Not_Blink(b00001111);
      Display_Clr_DP();
    }
    else
    {
      Keysensitive_setting_display_handler();
    }
  }
  break;
  //***********************************************************************************************/
  default:
    break;
  }
  //***********************************************************************************************/
  // Menu display:
  if (eMenu_State_old != eMenu_State)
  {
    bMenu_Display = TRUE;
    ucMenu_Display_cont = 0;
  }

  eMenu_State_old = eMenu_State;

  if (bMenu_Display)
  {
    /* code */
    if (TimeBase_Get1sSystemTimeDelta())
      ucMenu_Display_cont++;

    if (ucMenu_Display_cont > Menu_Display_Numb)
    {
      ucMenu_Display_cont = 0;
      bMenu_Display = FALSE;
    }
  }

  //**********************************************************************************************/
}
/************************************************************************************************************************************************************************************************
函数名称 ： Autopower_setting_display_handler
功    能 ： 
参    数 ： 
            
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
void Autopower_setting_display_handler(void)
{
  if (bPowerOn == FALSE)
    return;

  switch (eAutoPower_Setting_state)
  {
  case Autopower_setting_InitPara:
  {
    pAutopower_data = ucAutopower_P1_Power_Data;
    pAutopower_Timing_data = ucAutopower_P1_Time_Data;
    ucpwr_time_step_value = 0;
    eAutoPower_Setting_state = Autopower_setting_Step;
    bpower_line_setting = FALSE;
    eAutoPower_Setting_state_save = AutoPower_Setting_Power1;
    // Display_Not_Blink(b11111111);
    Display_Clr_AllBlink();
  }
  break;
  case Autopower_setting_Step:
  {
    DisplaycodeData[NUM_OF_LED - 4] = Display_L;
    DisplaycodeData[NUM_OF_LED - 3] = eAutoPower_Setting_state_save / 2;
    DisplaycodeData[NUM_OF_LED - 2] = Display_P;
    DisplaycodeData[NUM_OF_LED - 1] = ucpwr_time_step_value;
    if (eAutoPower_Setting_state_save % 2)
    {
      DisplaycodeData[NUM_OF_LED - 2] = Display_T;
    }

    if (bpower_line_setting)
    {
      // Display_Not_Blink(b00001111);
      Display_Clr_AllBlink();
      Display_Blink(b00000001);
    }
    else
    {
      // Display_Not_Blink(b00001111);
      Display_Clr_AllBlink();
      Display_Blink(b00000110);
    }
    // Display_DP_Not_Blink();
    //*************************************************************************************************/
  }
  break;
  case AutoPower_Setting_Power1:
  case AutoPower_Setting_Power2:
  {
    //*************************************************************************************************/

    u8 *p;
    p = pAutopower_data + ucpwr_time_step_value;
    DisplaycodeData[3] = sPowerTable[*p].ucPowerlevel % 10;
    DisplaycodeData[2] = Display_Dark; //(sPowerTable[*p].ucPowerlevel % 10) / 10;
    DisplaycodeData[1] = ucpwr_time_step_value;
    DisplaycodeData[0] = Display_P;

    if (eAutoPower_Setting_state_save % 2)
    {
      DisplaycodeData[0] = Display_T;
    }
    // Display_Set_DP();
    //Display_Not_Blink(b11111111);
    Display_Clr_AllBlink();
    // Display_DP_Blink();
    Display_Set_DP();
    Display_Blink(b00000011); // high 4 bit should be 0.eg.0x0f...low bit 1 means blink
  }
  break;
  case AutoPower_Setting_Timing1:
  case AutoPower_Setting_Timing2:
  {

    u16 *p;
    p = pAutopower_Timing_data + ucpwr_time_step_value;
    if (*p > 5999)
    {
      DisplaycodeData[3] = *p / 60 % 10;
      DisplaycodeData[2] = *p / 60 % 60 / 10;
      DisplaycodeData[1] = *p / 60 / 60 % 10;
      DisplaycodeData[0] = Display_H;
    }
    else
    {
      DisplaycodeData[3] = *p % 10;
      DisplaycodeData[2] = *p % 60 / 10;
      DisplaycodeData[1] = *p / 60 % 10;
      DisplaycodeData[0] = *p / 60 / 10;
    }

    // Display_Set_DP();
    // Display_Not_Blink(b11111111);
    Display_Clr_AllBlink();
    // Display_DP_Blink();
    Display_Set_DP();

    if (tTiming_sec_min_select == minute)
      Display_Blink(b00001100);
    else
    {
      Display_Blink(b00000011); // high 4 bit should be 0.eg.0x0f...low bit 1 means blink
    }
    if (Key_bUp() || Key_bDown())
    {
      Display_7Seg_Not_Blink();
    }
  }
  break;
  default:
    break;
  }
  // }
}

/************************************************************************************************************************************************************************************************
函数名称 ： Keysensitive_setting_display_handler
功    能 ： 
参    数 ： 
            
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/

void Keysensitive_setting_display_handler(void)
{
  if (bPowerOn == FALSE)
    return;

  switch (eKeySenitive_Menu)
  {
  case KeySensitive_Setting_InitPara:
    key_sensitive_select_Num = 0;
    for (u8 i = 0; i < KEY_TOTAL_NUMBER; i++)
    {
      key_sensitivity_value_save[i] = key_sensitivity_command_save[i];
    }
    eKeySenitive_Menu = KeySensitive_Setting_Key_Num;
    break;
  case KeySensitive_Setting_Key_Num:
    DisplaycodeData[3] = key_sensitivity_value_save[key_sensitive_select_Num] % 10;
    DisplaycodeData[2] = key_sensitivity_value_save[key_sensitive_select_Num] / 10;
    DisplaycodeData[1] = key_sensitive_select_Num + 1;
    DisplaycodeData[0] = Display_K;
    Display_Not_Blink(b00001111);
    Display_Blink(b00000100);
    Display_Set_DP();
    Display_ClrLED1(); // not useful here :(
    Display_ClrLED2(); // not useful here :(
    break;
  case KeySensitive_Setting_Level:
    DisplaycodeData[3] = key_sensitivity_value_save[key_sensitive_select_Num] % 10;
    DisplaycodeData[2] = key_sensitivity_value_save[key_sensitive_select_Num] / 10;
    DisplaycodeData[1] = key_sensitive_select_Num + 1;
    DisplaycodeData[0] = Display_K;
    Display_Not_Blink(b00001111);
    Display_Blink(b00000011);
    Display_Set_DP();
    Display_ClrLED1(); // not useful here :(
    Display_ClrLED2(); // not useful here :(
    break;
    break;
  default:
    break;
  }
}

void key_sensitive_savecommand(void)
{
  u8 ucchecksum = 0;
  for (u8 i = 0; i < KEY_TOTAL_NUMBER; i++)
  {
    ucchecksum += key_sensitivity_value_save[i];
    key_sensitivity_command_save[i] = key_sensitivity_value_save[i];
  }
  key_sensitivity_command_save[KEY_SENSITIVY_CONFIG_BUFFE_RSIZE - 1] = ucchecksum;
}
/************************************************************************************************************************************************************************************************
函数名称 ： Increase_globall
功    能 ： 
参    数 ： 
            
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
static u16 Increase_globall(u16 ucAddup_value, u16 ucAddup_Max_value, u16 ucAddup_Min_value, bool bcircle, u16 ucstep)
{

  if (ucAddup_value < (ucAddup_Max_value - ucstep))
  {
    ucAddup_value += ucstep;
    return ucAddup_value;
  }

  if (ucAddup_value > (ucAddup_Max_value - ucstep))
  {
    ucAddup_value = ucAddup_Max_value;
    if (bcircle)
      ucAddup_value = ucAddup_Min_value;

    return ucAddup_value;
  }

  if (ucAddup_value == (ucAddup_Max_value - ucstep))
  {
    ucAddup_value = ucAddup_Max_value;
    //if (bcircle)
    //ucAddup_value = ucAddup_Min_value;
  }
  return ucAddup_value;
}
/************************************************************************************************************************************************************************************************
函数名称 ： Decrease_globall
功    能 ： 
参    数 ： 
            
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
static u16 Decrease_globall(u16 ucAddup_value, u16 ucAddup_Max_value, u16 ucAddup_Min_value, bool bcircle, u16 ucstep)
{
  if (ucAddup_value <= ucAddup_Min_value)
  {
  overmaxF:
    ucAddup_value = ucAddup_Min_value;
    if (bcircle)
      ucAddup_value = ucAddup_Max_value;
    return ucAddup_value;
  }

  if (ucAddup_value > ucAddup_Min_value)
  {
    ucAddup_value -= ucstep;
    if (ucAddup_value > ucAddup_Max_value)
      goto overmaxF;
  }
  return ucAddup_value;
}
/************************************************************************************************************************************************************************************************
函数名称 ： PwControl_IncreasePowerLevel
功    能 ： 
参    数 ： ucPowerLevel
            
            
返 回 值 ： ucPowerLevel
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
static u8 PwControl_IncreasePowerLevel(u8 ucPowerLevel, bool circle)
{
  u8 ucPowerLevel_Buf;
  ucPowerLevel_Buf = ucPowerLevel;
  if (ucPowerLevel_Buf != 0)
  {
    if (ucPowerLevel_Buf == (TOTAL_LEVEL_NUMBER - 1))
    {
      // if(circle)
      //ucPowerLevel_Buf = TOTAL_LEVEL_NUMBER - Defaultpoweronlevel;
    }
    ucPowerLevel_Buf--;
    Beep_OneShortBeep();
  }
  else
  {
    if (circle)
      ucPowerLevel_Buf = TOTAL_LEVEL_NUMBER - 1; //change to level 0
    else
      Key_ConfirmKey();
  }

  return ucPowerLevel_Buf;
}
/************************************************************************************************************************************************************************************************
函数名称 ： PwControl_DecreasePowerLevel
功    能 ： 
参    数 ： ucPowerLevel
            
            
返 回 值 ： ucPowerLevel
作    者 ： Michael Shen
*************************************************************************************************************************************************************************************************/
static u8 PwControl_DecreasePowerLevel(u8 ucPowerLevel, bool circle)
{
  u8 ucPowerLevel_Buf;
  ucPowerLevel_Buf = ucPowerLevel;

  if (ucPowerLevel_Buf < TOTAL_LEVEL_NUMBER - 1)
  {
    ucPowerLevel_Buf++;
    Beep_OneShortBeep();
  }
  else if (ucPowerLevel_Buf == TOTAL_LEVEL_NUMBER - 1)
  {
    if (circle)
    {
      ucPowerLevel_Buf = 0;
      Beep_OneShortBeep();
    }
    else
    {
      Beep_TwoShortBeep();
      Key_ConfirmKey();
    }
  }
  return ucPowerLevel_Buf;
}

void Copy_data_to_eeprom(void)
{
  for (u8 i = 0; i < NUM_OF_Autopowerdata; i++)
  {
    eeprom_config.Power1_config[i] = ucAutopower_P1_Power_Data[i];
    eeprom_config.Power2_config[i] = ucAutopower_P2_Power_Data[i];
    eeprom_config.Time1_config[i] = ucAutopower_P1_Time_Data[i]; //transfer min to second
    eeprom_config.Time2_config[i] = ucAutopower_P2_Time_Data[i]; //transfer min to second
  }

  key_sensitive_savecommand();
  //if changed then need update
  if (check_sensitive_changed(eeprom_config.key_sensitivity_config_Buf, key_sensitivity_command_save, KEY_TOTAL_NUMBER))
  {
    eeprom_config.bkey_sensitivity_need_update = TRUE;

    for (u8 j = 0; j < KEY_SENSITIVY_CONFIG_BUFFE_RSIZE; j++)
    {
      eeprom_config.key_sensitivity_config_Buf[j] = key_sensitivity_command_save[j];
    }
    // eeprom_config.key_sensitivity_feedback = 0;
  }
}

void Copy_eeprom_data(void)
{
  for (u8 i = 0; i < NUM_OF_Autopowerdata; i++)
  {
    ucAutopower_P1_Power_Data[i] = eeprom_config.Power1_config[i];
    ucAutopower_P2_Power_Data[i] = eeprom_config.Power2_config[i];

    ucAutopower_P1_Time_Data[i] = eeprom_config.Time1_config[i]; //transfer  second to min
    ucAutopower_P2_Time_Data[i] = eeprom_config.Time2_config[i]; //transfer second to min
  }
  for (u8 j = 0; j < KEY_SENSITIVY_CONFIG_BUFFE_RSIZE; j++)
  {
    key_sensitivity_command_save[j] = eeprom_config.key_sensitivity_config_Buf[j];
    if (j < KEY_TOTAL_NUMBER)
      key_sensitivity_value_save[j] = key_sensitivity_command_save[j];
  }
}
//*************************************************************************************************/
void Autopower_Key_Power_setting(void)
{
  if ((!bLockOff))
    if (!bPowerOn)
      return;

  bPowerOn = !(bPowerOn);
  Beep_OneShortBeep();
}
//*************************************************************************************************/
void Autopower_Key_Set_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn)
    return;

  if (bPowerOn && bLockOff && (!bsettingOn))
  {

    return;
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        eAutoPower_Setting_state = eAutoPower_Setting_state_save;
        tTiming_sec_min_select = second;
      }
      break;
      case AutoPower_Setting_Power1:
      case AutoPower_Setting_Power2:
      case AutoPower_Setting_Timing1:
      case AutoPower_Setting_Timing2:
      {
        eAutoPower_Setting_state = Autopower_setting_Step;
      }
      break;
      default:
        break;
      }
    }
    break;

    case Menu_Display_Brightness:
    {
      return;
    }
    break;

    case Menu_Key_sensitivity:
    {
      return;
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

void Autopower_LongKey_Set_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn)
    return;

  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
    bsettingOn = TRUE;
    Autopower_Break();
    Copy_eeprom_data();
    Beep_OneShortBeep();
    return;
  }

  if (bsettingOn)
  {
    bsettingOn = FALSE;
    Copy_data_to_eeprom();
    writeconfig();

    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
    }
    break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
    }
    break;

    default:
      break;
    }
  }
  Beep_OneLongBeep();
}
//*************************************************************************************************/
void Autopower_Key_Lock_setting(void)
{
  if (!bPowerOn)
    return;

  if (!bsettingOn)
  {
    return;
  }

  if (bsettingOn)
  {

    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        bpower_line_setting = !(bpower_line_setting);
      }
      break;
      case AutoPower_Setting_Power1:
      case AutoPower_Setting_Power2:
      case AutoPower_Setting_Timing1:
      case AutoPower_Setting_Timing2:
      {
        bAutopower_setting_Timing = !(bAutopower_setting_Timing);

        // if (bAutopower_setting_Timing)
        if (eAutoPower_Setting_state % AutoPower_Setting_Power1)
        {
          eAutoPower_Setting_state--;
        }
        else
        {
          eAutoPower_Setting_state++;
        }
        eAutoPower_Setting_state_save = eAutoPower_Setting_state;
      }
      break;
      default:
        break;
      }
    }
    break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
      if ((eKeySenitive_Menu == KeySensitive_Setting_Key_Num) || (eKeySenitive_Menu == KeySensitive_Setting_Level))
        eKeySenitive_Menu = Increase_globall(eKeySenitive_Menu, KeySensitive_Setting_Level, KeySensitive_Setting_Key_Num, TRUE, 1);
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

void Autopower_LongKey_Lock_setting(void)
{
  if (!bsettingOn)
  {
    bLockOff = !(bLockOff);
    Beep_OneShortBeep();
  }

  if (!bPowerOn)
    return;

  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
  }

  if (bsettingOn)
  {
    eMenu_State = Increase_globall(eMenu_State, Menu_Display_Brightness, Menu_Auto_Power, TRUE, 1);

    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
    }
    break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}
//*************************************************************************************************/
void Autopower_Key_P1_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn)
    return;
  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
    Autopower_P1_Start();
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          // ucpwr_time_step_value = Increase_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          // eAutoPower_Setting_state_save = Increase_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }

        if (eAutoPower_Setting_state_save != AutoPower_Setting_Power1)
        {
          eAutoPower_Setting_state_save = AutoPower_Setting_Power1;
          pAutopower_data = ucAutopower_P1_Power_Data;
          pAutopower_Timing_data = ucAutopower_P1_Time_Data;
          bAutopower_setting_Timing = FALSE;
          ucpwr_time_step_value = 0;
          // Key_ConfirmKeyWithLongkeyRepetition(FALSE);
        }
      }
      break;

      case AutoPower_Setting_Power1:
      case AutoPower_Setting_Power2:

        break;
      case AutoPower_Setting_Timing1:
      case AutoPower_Setting_Timing2:
        tTiming_sec_min_select = minute;
        break;
      default:
        break;
      }
    }

    break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

void Autopower_LongKey_P1_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn)
    return;
  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
    Autopower_P1_ReStart();
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          // ucpwr_time_step_value = Increase_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          // eAutoPower_Setting_state_save = Increase_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }
      }
      break;

      case AutoPower_Setting_Power1:
      case AutoPower_Setting_Power2:

        break;
      case AutoPower_Setting_Timing1:
      case AutoPower_Setting_Timing2:

        break;
      default:
        break;
      }
    }

    break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

void Autopower_LongKey_P2_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn)
    return;
  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
    Autopower_P2_ReStart();
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          // ucpwr_time_step_value = Increase_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          // eAutoPower_Setting_state_save = Increase_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }
      }
      break;

      case AutoPower_Setting_Power1:
      case AutoPower_Setting_Power2:

        break;
      case AutoPower_Setting_Timing1:
      case AutoPower_Setting_Timing2:

        break;
      default:
        break;
      }
    }

    break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

void Autopower_Key_P2_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn)
    return;
  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
    Autopower_P2_Start();
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
    }

      {
        switch (eAutoPower_Setting_state)
        {
        case Autopower_setting_InitPara:
        {
        }
        break;
        case Autopower_setting_Step:
        {
          if (bpower_line_setting)
          {
            // ucpwr_time_step_value = Increase_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
          }
          else
          {
            // eAutoPower_Setting_state_save = Increase_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
          }

          if (eAutoPower_Setting_state_save != AutoPower_Setting_Power2)
          {
            eAutoPower_Setting_state_save = AutoPower_Setting_Power2;
            pAutopower_data = ucAutopower_P2_Power_Data;
            pAutopower_Timing_data = ucAutopower_P2_Time_Data;
            bAutopower_setting_Timing = FALSE;
            ucpwr_time_step_value = 0;
          }
        }
        break;

        case AutoPower_Setting_Power1:
        case AutoPower_Setting_Power2:

          break;
        case AutoPower_Setting_Timing2:
        case AutoPower_Setting_Timing1:

          tTiming_sec_min_select = second;
          break;
        default:
          break;
        }
      }

      break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

void Autopower_LongKey_P1_Clear_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn)
    return;
  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          // ucpwr_time_step_value = Increase_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          // eAutoPower_Setting_state_save = Increase_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }
      }
      break;

      case AutoPower_Setting_Timing2:
      case AutoPower_Setting_Power2:
        // u8 *p;
        // p = pAutopower_data + ucpwr_time_step_value;
        // *p = PwControl_IncreasePowerLevel(*p);
        break;
      case AutoPower_Setting_Timing1:
      case AutoPower_Setting_Power1:
      {

        for (u8 i = 0; i < NUM_OF_Autopowerdata; i++)
        {
          ucAutopower_P1_Time_Data[i] = 0;
          // ucAutopower_P2_Time_Data[i] = 0;
          ucAutopower_P1_Power_Data[i] = TOTAL_LEVEL_NUMBER - 1;
          // ucAutopower_P2_Power_Data[i] = TOTAL_LEVEL_NUMBER - 1;
        }
        // u8 *p;
        // p = pAutopower_Timing_data + ucpwr_time_step_value;
        // *p = Increase_globall(*p, Autopower_Time_Max, 0, TRUE, 1);
      }
        Beep_OneLongBeep();
        break;
      default:
        break;
      }
    }
    break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
      for (u8 i = 0; i < KEY_TOTAL_NUMBER; i++)
      {
        key_sensitivity_value_save[i] = key_defaut_sensitivity_command[i];
      }

      Beep_OneLongBeep();
    }
    break;

    default:
      break;
    }
  }
}

void Autopower_LongKey_P2_Clear_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn)
    return;
  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          // ucpwr_time_step_value = Increase_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          // eAutoPower_Setting_state_save = Increase_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }
      }
      break;
      case AutoPower_Setting_Power1:
      case AutoPower_Setting_Timing1:
        // u8 *p;
        // p = pAutopower_data + ucpwr_time_step_value;
        // *p = PwControl_IncreasePowerLevel(*p);
        break;
      case AutoPower_Setting_Power2:
      case AutoPower_Setting_Timing2:
      {

        for (u8 i = 0; i < NUM_OF_Autopowerdata; i++)
        {
          // ucAutopower_P1_Time_Data[i] = 0;
          ucAutopower_P2_Time_Data[i] = 0;
          // ucAutopower_P1_Power_Data[i] = TOTAL_LEVEL_NUMBER - 1;
          ucAutopower_P2_Power_Data[i] = TOTAL_LEVEL_NUMBER - 1;
        }
        // u8 *p;
        // p = pAutopower_Timing_data + ucpwr_time_step_value;
        // *p = Increase_globall(*p, Autopower_Time_Max, 0, TRUE, 1);
      }
        Beep_OneLongBeep();
        break;
      default:
        break;
      }
    }
    break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
    }
    break;

    default:
      break;
    }
  }
}

void Autopower_LongKey_Time_PlusMinus_Clear(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn)
    return;
  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          // ucpwr_time_step_value = Increase_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          // eAutoPower_Setting_state_save = Increase_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }
      }
      break;
      case AutoPower_Setting_Power1:
      case AutoPower_Setting_Power2:
        break;
      case AutoPower_Setting_Timing1:
      {
        pAutopower_Timing_data = ucAutopower_P1_Time_Data;
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        // *p = Increase_globall(*p, Autopower_Time_Max, 0, TRUE, (tTiming_sec_min_select <= second) ? 1 : 60);
        *p = 0; // clear current time
        Beep_TwoShortBeep();
      }
      break;
      case AutoPower_Setting_Timing2:
      {
        pAutopower_Timing_data = ucAutopower_P2_Time_Data;
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        // *p = Increase_globall(*p, Autopower_Time_Max, 0, TRUE, (tTiming_sec_min_select <= second) ? 1 : 60);
        *p = 0; // clear current time
        Beep_TwoShortBeep();
      }
      break;
      default:
        break;
      }
    }
    break;

    case Menu_Display_Brightness:
    {
    }
    break;

    case Menu_Key_sensitivity:
    {
    }
    break;

    default:
      break;
    }
  }
}
//*************************************************************************************************/

void Autopower_Key_up_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn || bAutopowerRun)
    return;

  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
    ucCurrentPowerLevel = PwControl_IncreasePowerLevel(ucCurrentPowerLevel, FALSE);
    return;
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          ucpwr_time_step_value = Increase_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          eAutoPower_Setting_state_save = Increase_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }
      }
      break;
      case AutoPower_Setting_Power1:
      {
        pAutopower_data = ucAutopower_P1_Power_Data;
        u8 *p;
        p = pAutopower_data + ucpwr_time_step_value;
        *p = PwControl_IncreasePowerLevel(*p, TRUE);
        break;
      }
      case AutoPower_Setting_Power2:
      {
        pAutopower_data = ucAutopower_P2_Power_Data;
        u8 *p;
        p = pAutopower_data + ucpwr_time_step_value;
        *p = PwControl_IncreasePowerLevel(*p, TRUE);
        break;
      }
      break;
      case AutoPower_Setting_Timing1:
      {
        pAutopower_Timing_data = ucAutopower_P1_Time_Data;
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        *p = Increase_globall(*p, Autopower_Time_Max, 0, TRUE, (tTiming_sec_min_select <= second) ? 1 : 60);
      }
      break;
      case AutoPower_Setting_Timing2:
      {
        pAutopower_Timing_data = ucAutopower_P2_Time_Data;
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        *p = Increase_globall(*p, Autopower_Time_Max, 0, TRUE, (tTiming_sec_min_select <= second) ? 1 : 60);
      }
      break;
      default:
        break;
      }
    }
    break;

    case Menu_Display_Brightness:
    {
      Display_Brightness_UPD(1);
    }
    break;

    case Menu_Key_sensitivity:
    {
      if (eKeySenitive_Menu == KeySensitive_Setting_Key_Num)
        key_sensitive_select_Num = Increase_globall(key_sensitive_select_Num, KEY_ACTUAL_NUMBER - 1, 0, TRUE, 1);

      if (eKeySenitive_Menu == KeySensitive_Setting_Level)
      {
        u8 *p;
        p = key_sensitivity_value_save + key_sensitive_select_Num;
        key_sensitivity_value_save[key_sensitive_select_Num] = Increase_globall(*p, KEY_SENSITIVITY_MAX, KEY_SENSITIVITY_MIN, TRUE, 1);
      }
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

void Autopower_LongKey_up_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn || bAutopowerRun)
    return;

  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
    ucCurrentPowerLevel = PwControl_IncreasePowerLevel(ucCurrentPowerLevel, FALSE);
    return;
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          ucpwr_time_step_value = Increase_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          eAutoPower_Setting_state_save = Increase_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }
      }
      break;
      case AutoPower_Setting_Power1:
      {
        pAutopower_data = ucAutopower_P1_Power_Data;
        u8 *p;
        p = pAutopower_data + ucpwr_time_step_value;
        *p = PwControl_IncreasePowerLevel(*p, FALSE);
        break;
      }
      case AutoPower_Setting_Power2:
      {
        pAutopower_data = ucAutopower_P2_Power_Data;
        u8 *p;
        p = pAutopower_data + ucpwr_time_step_value;
        *p = PwControl_IncreasePowerLevel(*p, FALSE);
        break;
      }
      break;
      case AutoPower_Setting_Timing1:
      {
        pAutopower_Timing_data = ucAutopower_P1_Time_Data;
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        *p = Increase_globall(*p, Autopower_Time_Max, 0, TRUE, (tTiming_sec_min_select <= second) ? SEC_STEP_TIMEADD : MIN_STEP_TIMEADD);
      }
      break;
      case AutoPower_Setting_Timing2:
      {
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        *p = Increase_globall(*p, Autopower_Time_Max, 0, FALSE, (tTiming_sec_min_select <= second) ? SEC_STEP_TIMEADD : MIN_STEP_TIMEADD);
      }
      break;
      default:
        break;
      }
    }
    break;

    case Menu_Display_Brightness:
    {
      Display_Brightness_UPD(1);
    }
    break;

    case Menu_Key_sensitivity:
    {
      if (eKeySenitive_Menu == KeySensitive_Setting_Key_Num)
        key_sensitive_select_Num = Increase_globall(key_sensitive_select_Num, KEY_ACTUAL_NUMBER - 1, 0, TRUE, 1);

      if (eKeySenitive_Menu == KeySensitive_Setting_Level)
      {
        u8 *p;
        p = key_sensitivity_value_save + key_sensitive_select_Num;
        key_sensitivity_value_save[key_sensitive_select_Num] = Increase_globall(*p, KEY_SENSITIVITY_MAX, KEY_SENSITIVITY_MIN, TRUE, 1);
      }
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

//*************************************************************************************************/
void Autopower_Key_Down_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn || bAutopowerRun)
    return;

  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
    ucCurrentPowerLevel = PwControl_DecreasePowerLevel(ucCurrentPowerLevel, TRUE);
    return;
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          ucpwr_time_step_value = Decrease_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          eAutoPower_Setting_state_save = Decrease_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }
      }
      break;
      case AutoPower_Setting_Power1:
      {
        pAutopower_data = ucAutopower_P1_Power_Data;
        u8 *p;
        p = pAutopower_data + ucpwr_time_step_value;
        *p = PwControl_DecreasePowerLevel(*p, TRUE);
        break;
      }
      case AutoPower_Setting_Power2:
      {
        pAutopower_data = ucAutopower_P2_Power_Data;
        u8 *p;
        p = pAutopower_data + ucpwr_time_step_value;
        *p = PwControl_DecreasePowerLevel(*p, TRUE);
        break;
      }
      case AutoPower_Setting_Timing1:
      {
        pAutopower_Timing_data = ucAutopower_P1_Time_Data;
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        *p = Decrease_globall(*p, Autopower_Time_Max, 0, TRUE, (tTiming_sec_min_select <= second) ? 1 : 60);
      }
      break;
      case AutoPower_Setting_Timing2:
      {
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        *p = Decrease_globall(*p, Autopower_Time_Max, 0, TRUE, (tTiming_sec_min_select <= second) ? 1 : 60);
      }
      break;
      default:
        break;
      }
    }
    break;

    case Menu_Display_Brightness:
    {
      Display_Brightness_UPD(0);
    }
    break;

    case Menu_Key_sensitivity:
    {
      if (eKeySenitive_Menu == KeySensitive_Setting_Key_Num)
        key_sensitive_select_Num = Decrease_globall(key_sensitive_select_Num, KEY_ACTUAL_NUMBER - 1, 0, TRUE, 1);

      if (eKeySenitive_Menu == KeySensitive_Setting_Level)
      {
        u8 *p;
        p = key_sensitivity_value_save + key_sensitive_select_Num;
        key_sensitivity_value_save[key_sensitive_select_Num] = Decrease_globall(*p, KEY_SENSITIVITY_MAX, KEY_SENSITIVITY_MIN, TRUE, 1);
      }
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

void Autopower_LongKey_Down_setting(void)
{
  if (!bLockOff)
    return;

  if (!bPowerOn || bAutopowerRun)
    return;

  if ((bPowerOn) && (!bsettingOn) && bLockOff)
  {
    ucCurrentPowerLevel = PwControl_DecreasePowerLevel(ucCurrentPowerLevel, FALSE);
    return;
  }

  if (bsettingOn)
  {
    switch (eMenu_State)
    {
    case Menu_InitPara:
    {
    }
    break;

    case Menu_Auto_Power:
    {
      switch (eAutoPower_Setting_state)
      {
      case Autopower_setting_InitPara:
      {
      }
      break;
      case Autopower_setting_Step:
      {
        if (bpower_line_setting)
        {
          ucpwr_time_step_value = Decrease_globall(ucpwr_time_step_value, (NUM_OF_Autopowerdata - 1), 0, TRUE, 1);
        }
        else
        {
          eAutoPower_Setting_state_save = Decrease_globall(eAutoPower_Setting_state_save, AutoPower_Setting_Timing2, AutoPower_Setting_Power1, TRUE, 1);
        }
      }
      break;
      case AutoPower_Setting_Power1:
      {
        pAutopower_data = ucAutopower_P1_Power_Data;
        u8 *p;
        p = pAutopower_data + ucpwr_time_step_value;
        *p = PwControl_DecreasePowerLevel(*p, FALSE);
        break;
      }
      case AutoPower_Setting_Power2:
      {
        pAutopower_data = ucAutopower_P2_Power_Data;
        u8 *p;
        p = pAutopower_data + ucpwr_time_step_value;
        *p = PwControl_DecreasePowerLevel(*p, FALSE);
        break;
      }
      case AutoPower_Setting_Timing1:
      {
        pAutopower_Timing_data = ucAutopower_P1_Time_Data;
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        *p = Decrease_globall(*p, Autopower_Time_Max, 0, TRUE, (tTiming_sec_min_select <= second) ? SEC_STEP_TIMEADD : MIN_STEP_TIMEADD);
      }
      break;
      case AutoPower_Setting_Timing2:
      {
        u16 *p;
        p = pAutopower_Timing_data + ucpwr_time_step_value;
        *p = Decrease_globall(*p, Autopower_Time_Max, 0, FALSE, (tTiming_sec_min_select <= second) ? SEC_STEP_TIMEADD : MIN_STEP_TIMEADD);
      }
      break;
      default:
        break;
      }
    }
    break;

    case Menu_Display_Brightness:
    {
      Display_Brightness_UPD(0);
    }
    break;

    case Menu_Key_sensitivity:
    {
      if (eKeySenitive_Menu == KeySensitive_Setting_Key_Num)
        key_sensitive_select_Num = Decrease_globall(key_sensitive_select_Num, KEY_ACTUAL_NUMBER - 1, 0, TRUE, 1);

      if (eKeySenitive_Menu == KeySensitive_Setting_Level)
      {
        u8 *p;
        p = key_sensitivity_value_save + key_sensitive_select_Num;
        key_sensitivity_value_save[key_sensitive_select_Num] = Decrease_globall(*p, KEY_SENSITIVITY_MAX, KEY_SENSITIVITY_MIN, TRUE, 1);
      }
    }
    break;

    default:
      break;
    }
  }
  Beep_OneShortBeep();
}

static u8 ucLong_key_count;
static u8 ucLong_key_cmp_count = LONG_KEY_NUM;
void Long_key_handler(void)
{
  if (TimeBase_Get10msSystemTimeDelta())
  {
    if (key_bKeyPressLongF())
    {
      if (ucLong_key_count++ > ucLong_key_cmp_count)
      {
        if (ucLong_key_cmp_count >= LONG_KEY_MIN_NUM)
        {
          ucLong_key_cmp_count -= LONG_KEY_SPEED_NUM;
        }
        ucLong_key_count = 0;
        if (Key_bUp())
        {
          Autopower_LongKey_up_setting();
        }
        if (Key_bDown())
        {
          Autopower_LongKey_Down_setting();
        }
      }
    }
    else
    {
      ucLong_key_count = 0;
      ucLong_key_cmp_count = LONG_KEY_NUM;
    }
  }
}
