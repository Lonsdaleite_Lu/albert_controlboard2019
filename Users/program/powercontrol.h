#ifndef __POWER_CONTROL_H__
#define __POWER_CONTROL_H__

#include "stm8s.h"

//#define bGetLogicalState(value) (value!=0 ? TRUE : FALSE)
#define TOTAL_LEVEL_NUMBER 10
#define MAX_POWER_RATIO 1000 // no used here.
#define NUM_OF_Autopower 2  // no used here.
#define Defaultpoweronlevel 4 // no use dhere.
#define ZEROPOWERAUTOOFF 120 //defaut:120s if zero then off automatic. 5 for test
#define AUTOPOWERTIME_SHOW 10 //defaut 10s, to show power and time when in automatic working. 5 for test
#define ALWAYSSHOWTIME_NUM 30 //defaut 30s, show time when the rest time lower than x. 10 for test



#define POWER_PERCENT_TABLE \
  {                         \
    {100, 1000, 9},         \
        {90, 900, 8},       \
        {80, 800, 7},       \
        {70, 700, 6},       \
        {55, 550, 5},       \
        {45, 450, 4},       \
        {35, 350, 3},       \
        {25, 250, 2},       \
        {10, 100, 1},       \
        {0, 0, 0},          \
  }

#define Default_POWER_Level   \
  TOTAL_LEVEL_NUMBER - 1,     \
      TOTAL_LEVEL_NUMBER - 1, \
      TOTAL_LEVEL_NUMBER - 1, \
      TOTAL_LEVEL_NUMBER - 1, \
      TOTAL_LEVEL_NUMBER - 1, \
      TOTAL_LEVEL_NUMBER - 1, \
      TOTAL_LEVEL_NUMBER - 1, \
      TOTAL_LEVEL_NUMBER - 1, \
      TOTAL_LEVEL_NUMBER - 1, \
      TOTAL_LEVEL_NUMBER - 1


typedef struct _tPowerTable
{
  u8 ucPowerPercentage; // absulate power percentage 1% ~ 100%
  u16 uiPowerRatio;     // power ratio send to generator
  u8 ucPowerlevel;
} tPowerTable;

typedef enum _tPwControl_WorkingState
{
  PwControl_InitPara,
  PwControl_Idle,
  PwControl_SendModBusStart,
  PwControl_Working,
  PwControl_ErrorState,
} tPwControl_WorkingState;


void PowerControl_Handler(void);
void Autopower_Break(void);
void Autopower_P1_ReStart(void); 
void Autopower_P2_ReStart(void); 
// void Autopower_Key_Power_setting(void);
// void Autopower_Key_Set_setting(void);
// void Autopower_LongKey_Set_setting(void);
// void Autopower_Key_Lock_setting(void);
// void Autopower_LongKey_Lock_setting(void);
// void Autopower_Key_P1_setting(void);
// void Autopower_Key_P2_setting(void);
// void Autopower_LongKey_P1_Clear_setting(void);
// void Autopower_LongKey_P2_Clear_setting(void);
// void Autopower_Key_up_setting(void);
// void Autopower_LongKey_up_setting(void);
// void Autopower_Key_Down_setting(void);
// void Autopower_LongKey_Down_setting(void);


#endif //#ifndef __POWER_CONTROL_H__