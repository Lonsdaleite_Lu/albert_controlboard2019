#include "autolevel.h"
#include "timebase.h"
#include "display.h"
#include "settingcontrol.h"
#include "powercontrol.h"
#include "stm8s.h"

static u8 Auotlevel_power_Buf;
// static u8 PowersetpSave_Buf;

/***************************************************************************/
/*                               Output Function                           */
/***************************************************************************/
void Init_Power_and_Time(autolevel *const Auto_P, u8 *ucAutolevel_Power_Data, u16 *ucAutolevel_Time_Data)
{
    Auto_P->bAutoPause = FALSE;
    // check which level time has 0;
    u8 PowerStep = NUM_OF_Autopowerdata;
    for (u8 i = 0; i < NUM_OF_Autopowerdata; i++) // to find non zero time data
    {

        if (0 == ucAutolevel_Time_Data[NUM_OF_Autopowerdata - i - 1])
        {
            PowerStep = NUM_OF_Autopowerdata - i - 1;
        }
    }
    Auto_P->ucAutoLevel_Step = PowerStep;

    // swap data , eg: [0] --> [7]
    for (u8 i = 0; i < Auto_P->ucAutoLevel_Step; i++)
    {
        Auto_P->ucAutolevel_Power_Data[i] = ucAutolevel_Power_Data[Auto_P->ucAutoLevel_Step - i - 1];
        Auto_P->ucAutolevel_Time_Data[i] = ucAutolevel_Time_Data[Auto_P->ucAutoLevel_Step - i - 1];
    }
}

u8 ShowPowerwithReduceTime(autolevel *const Auto_P)
{
    // u8 PowerStep = NUM_OF_Autopowerdata;
    // bool b_Time_correct = FALSE;

    // for (u8 i = 0; i < NUM_OF_Autopowerdata; i++) // to find non zero time data
    // {

    //     if (0 == Auto_P->ucAutolevel_Time_Data[NUM_OF_Autopowerdata - i - 1])
    //     {
    //         PowerStep = NUM_OF_Autopowerdata - i - 1;
    //     }
    // }

    if (Auto_P->ucAutoLevel_Step > 0) // one of time need auto handle
    {
        if (Auto_P->bstartF && (!Auto_P->bAutoPause))
        {
            if (Auto_P->ucAutolevel_Time_Data[Auto_P->ucAutoLevel_Step - 1] != 0)
            {
                Auto_P->ucAutolevel_Time_Data[Auto_P->ucAutoLevel_Step - 1]--;
            }
            else
            {
                Auto_P->ucAutoLevel_Step--;
                if (Auto_P->ucAutoLevel_Step != 0)
                {
                    if (Auto_P->Auto_EachStep)
                        Auto_P->Auto_EachStep();
                }
                else
                {
                    Auto_P->bstartF = FALSE;
                    if (Auto_P->Auto_end)
                        Auto_P->Auto_end();
                    Auto_P->ucAutolevel_Power_Data[Auto_P->ucAutoLevel_Step - 1] = TOTAL_LEVEL_NUMBER - 1; //end and set power to 0
                }
            }
        }
    }
    else
    {
        Auto_P->bstartF = FALSE;
        if (Auto_P->Auto_end)
            Auto_P->Auto_end();
        Auto_P->ucAutolevel_Power_Data[Auto_P->ucAutoLevel_Step - 1] = TOTAL_LEVEL_NUMBER - 1; //end and set power to 0
    }
    //Auto_P->ucAutoLevel_Step = PowerStep + 1;

    return Auto_P->ucAutolevel_Power_Data[Auto_P->ucAutoLevel_Step - 1];
}

u16 RemainTime(autolevel const *const Auto_P)
{
    // u8 PowerStep = NUM_OF_Autopowerdata;
    u16 RemainTimeBuf = 0;

    for (u8 i = 0; i < Auto_P->ucAutoLevel_Step; i++)
    {
        RemainTimeBuf += Auto_P->ucAutolevel_Time_Data[i];
    }

    return RemainTimeBuf;
}

u16 SetpRemainTime(autolevel const *const Auto_P)
{
    return Auto_P->ucAutolevel_Time_Data[Auto_P->ucAutoLevel_Step - 1];
}

u8 CurrentPowerSetp(autolevel const *const Auto_P)
{
    return Auto_P->ucAutoLevel_Step-1;
}