/**
  ******************************************************************************
  * @file    main.c
  * @author  MCD Application Team
  * @version V2.0.4
  * @date    26-April-2018
  * @brief   This file contains the main function for the ADC2 Continuous Conversion example.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "config.h"
#include "beep.h"
#include "display.h"
#include "timebase.h"
#include "key.h"
#include "iwdg.h"
#include "settingcontrol.h"
#include "eeprom.h"
#include "error.h"
#include "global_macro.h"
#include "I2C_Function.h"
#include "autolevel.h"
#include "adc.h"
#if defined(KNOB)
#include "knob.h"
#endif // KNOB
#if defined(NTC)
  #include "ntc.h"
#endif // NTC
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
uint16_t Conversion_Value = 0;
static u8 ucTestCounter;
// enum errorcode error = NOERROR;
/* Private function prototypes -----------------------------------------------*/
static void ADC_Config(void);
static void GPIO_Config(void);
static void CLK_Config(void);
static void DoMs1(void);
static void DoMs10(void);
static void DoMs250(void);
static void Dosecond(void);

/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
  * @brief  Configure ADC Continuous Conversion with End Of Conversion interrupt 
  *         enabled .
  * @param  None
  * @retval None
********************************************************************************/

static void ADC_Config()
{
  ADC_Init();
}

/********************************************************************************
  * @brief  Configure GPIO for LEDs and buttons available on the evaluation board
  * @param  None
  * @retval None
********************************************************************************/
static void GPIO_Config()
{
}

/********************************************************************************
  * @brief  Configure system clock to run at 16Mhz
  * @param  None
  * @retval None
********************************************************************************/
static void CLK_Config(void)
{
  /* Initialization of the clock */
  /* Clock divider to HSI/1 */
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
}

/********************************************************************************
  * @brief  Main function
  * @param  None
  * @retval None
********************************************************************************/
void main(void)
{
  /* Disable general interrupts -----------------------------------------*/
  disableInterrupts();

  /* Clock configuration -----------------------------------------*/
  CLK_Config();

  /* Watchdog Init -------------------------------------------*/
  IWDG_Initializes();

  /* GPIO configuration ------------------------------------------*/
  GPIO_Config();

  //Key_I2CSoftStop();
  /* EEPROM configuration ------------------------------------------*/
  EEPROM_Initializes();

  /* ADC configuration -------------------------------------------*/
  ADC_Config();

  /* Beep Init -------------------------------------------*/
  Beep_Init();

  /* TimeBase Init -----------------------------------------------*/
  TimeBase_Init();

  /* Init display state -----------------------------------------*/
  Display_Init();

  /* init key   -----------------------------------------*/
  Key_Init();

  /* init knob AD   -----------------------------------------*/
#if defined(KNOB)
  Knob_Init();
#endif // KNOB

  /* init NTC AD   -----------------------------------------*/
#if defined(NTC)
  NTC_Init();
#endif // NTC
  /* Enable general interrupts -----------------------------------------*/
  enableInterrupts();

  while (1)
  {
    IWDG_ReloadCounter(); //喂狗(理论小于4ms内喂狗都不会复位，由于HSI有偏差，我们设定在xxxms喂狗一次)

    TimeBase_HandleTimeBaseCouter(); // Timebase reset

    Display_Handler();

    Beep_Handler();

    Key_Handler();

    SettingControl_Handler();

    PowerControl_Handler();
    
#if defined(KNOB)
    Knob_Handler();
#endif // KNOB
#if defined(NTC)
    NTC_Handler();
#endif // NTC
    if (TimeBase_Get1msSystemTimeDelta())
    {
      //      DoMs1();
    }

    if (TimeBase_Get10msSystemTimeDelta())
    {
      //      DoMs10();
    }

    if (TimeBase_Get250msSystemTimeDelta())
    {
      DoMs250();
    }

    if (TimeBase_Get1sSystemTimeDelta())
    {
      Dosecond();
    }
  };
}

/**
  * @brief  This function will perform operation per 1 MS
  * @param  None
  * @retval None
  */
static void DoMs1()
{
}

/**
  * @brief  This function will perform operation per 10 MS
  * @param  None
  * @retval None
  */
static void DoMs10()
{
  /*
     ADChanel=0;
     GetArgAD();
     KeyADValueR=ArgADValueR;
     ADChanel=2;
     GetArgAD();
     TempR=ArgADValueR;
     ADChanel=3;
     GetArgAD();
     TempL=ArgADValueR;
     */
}

/**
  * @brief  This function will perform operation per 250 MS
  * @param  None
  * @retval None
  */
static void DoMs250()
{
  nop();
}

/**
  * @brief  This function will perform operation per 1S
  * @param  None
  * @retval None
  */
static void Dosecond(void)
{
  nop();

  //Key_SendsitivityConfig();
}

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
