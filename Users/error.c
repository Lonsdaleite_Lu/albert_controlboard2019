#include "error.h"
#include "stm8s.h"
#include "eeprom.h"

extern volatile bool bPowerOn;
extern enum errorcode error;


void set_error(enum errorcode code)
{	// therm_emergencydisable();
	// valve_emergencyclose();
	// engine_emergencystop();
	// //
	error = code;
	bPowerOn = FALSE;
	eeprom_config.error = code;
}

void set_warning(enum errorcode code)
{
	set_error(code);
}

void HardFault_Handler()
{
	set_error(HardFault);
}

void MemManage_Handler()
{
	set_error(MemManageFault);
}

void BusFault_Handler()
{
	set_error(BusFault);
}

void UsageFault_Handler()
{
	set_error(UsageFault);
}
