#ifndef __CONFIG_H__
#define __CONFIG_H__

/* To choose need Knob for contol or not */
// #define KNOB

/* To choose need NTC for contol or not */
// #define NTC

/* To define one display chip */
#define AIP650
// #define AIP1668
// #define TM1628

/* To define one Cap TC chip */
#define SCT80S16B  

#define ExtendKeys

/* To define Buzzer type */
#define Buzzer_Passive

#ifndef Buzzer_Passive
#define Buzzer_Active
#endif // Buzzer_Passive

#endif // !__CONFIG_H__