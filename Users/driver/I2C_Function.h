#ifndef __I2C_Function_H__
#define __I2C_Function_H__

#include "stm8s.h"
#define IIC_SLAVE_ADDRESS7 0x70
#define IIC_FREQUENCY 5000 // unit: Hz

    extern uint8_t I2CFunction;

void I2CInit(void);
void I2CFunctionWriteByte(void);
void I2CFunctionWriteData(void);
void I2CFunctionReadByte(void);
void I2CFunctionReadData(void);
void I2CFunctionGeneralReadData(void);
void I2C_Function_IRQHandler(void);

ErrorStatus I2CWriteByte(uint8_t SlaveAddress, uint8_t RegisterAddress, uint8_t *pWriteData);
ErrorStatus I2CWriteData(uint8_t SlaveAddress, uint8_t RegisterAddress, uint8_t *pWriteDataBuffer, uint8_t WriteDataCount);
ErrorStatus I2CReadByte(uint8_t SlaveAddress, uint8_t RegisterAddress, uint8_t *pReadData);
ErrorStatus I2CReadData(uint8_t SlaveAddress, uint8_t RegisterAddress, uint8_t *pReadDataBuffer, uint8_t ReadDataCount);
ErrorStatus I2CGeneralReadData(uint8_t SlaveAddress, uint8_t *pReadDataBuffer, uint8_t ReadDataCount);
ErrorStatus I2CWriteandReadData(uint8_t SlaveAddress, uint8_t *pWriteDataBuffer, uint8_t WriteDataCount, uint8_t *pReadDataBuffer, uint8_t ReadDataCount);
ErrorStatus I2C_Err_Happened(void);
ErrorStatus I2CCheckERREN(void);
#endif