#ifndef __ADC_H__
#define __ADC_H__

#include "stm8s.h"
#include "config.h"

#define REF_VTG 5
#define FULL_SCALE 1024 

void ADC_Init(void);
u16 Get_ADCCH_Avr_value(ADC1_Channel_TypeDef ADC1_Channel);
// void Knob_Handler(void);

#endif // !__ADC_H__