#include "stm8s.h"
#include "stm8s_i2c.h"
// #include "common.h"
//#include "gpio.h"
#include "I2C_Function.h"
#include "global_macro.h"

I2C_Event_TypeDef I2CLastEvent;
I2C_Direction_TypeDef I2CDirection;
uint8_t I2CSlaveAddress = 0;
uint8_t I2CRegisterAddress = 0;
uint8_t *pI2CWriteData;
uint8_t *pI2CReadData;
uint8_t *pI2CDataBuffer;
uint8_t *pI2CDataBuffer2;
uint8_t I2CDataCount = 0;
uint8_t I2CDataCount2 = 0;
uint8_t I2CFunction = 0; //0 无函数执行，1I2C 单次写入一个字节，2I2C 连续写入多个字节，3I2C 单次读取一个字节，4I2C 连续读取多个字节
uint8_t I2CGyroID = 0;
static bool bI2C_OVERRUNUNDERRUN = FALSE;
static bool bI2C_ACKNOWLEDGEFAILURE = FALSE;
static bool bI2C_ARBITRATIONLOSS = FALSE;
static bool bI2C_BUSERROR = FALSE;
static bool bI2C_Err_happened = FALSE;
static uint8_t I2CWriteDataStep = 0;
static uint8_t I2CReadDataStep = 0;
static uint8_t I2CReadGeneralDataStep = 0;
static uint8_t I2CWriteGeneralDataStep = 0;

/**
* @brief 执行I2C 读写函数之前检查I2C 是否存在错误，如果存在错误清除错误标
志，从新初始化I2C.
* @param
* @retval ErrorStatus:ERROR 存在错误，SUCCESS 无错误
*/
//   /* SR1 register flags */
//   I2C_FLAG_TXEMPTY             = (uint16_t)0x0180,  /*!< Transmit Data Register Empty flag */
//   I2C_FLAG_RXNOTEMPTY          = (uint16_t)0x0140,  /*!< Read Data Register Not Empty flag */
//   I2C_FLAG_STOPDETECTION       = (uint16_t)0x0110,  /*!< Stop detected flag */
//   I2C_FLAG_HEADERSENT          = (uint16_t)0x0108,  /*!< 10-bit Header sent flag */
//   I2C_FLAG_TRANSFERFINISHED    = (uint16_t)0x0104,  /*!< Data Byte Transfer Finished flag */
//   I2C_FLAG_ADDRESSSENTMATCHED  = (uint16_t)0x0102,  /*!< Address Sent/Matched (master/slave) flag */
//   I2C_FLAG_STARTDETECTION      = (uint16_t)0x0101,  /*!< Start bit sent flag */

//   /* SR2 register flags */
//   I2C_FLAG_WAKEUPFROMHALT      = (uint16_t)0x0220,  /*!< Wake Up From Halt Flag */
//   I2C_FLAG_OVERRUNUNDERRUN     = (uint16_t)0x0208,  /*!< Overrun/Underrun flag */
//   I2C_FLAG_ACKNOWLEDGEFAILURE  = (uint16_t)0x0204,  /*!< Acknowledge Failure Flag */
//   I2C_FLAG_ARBITRATIONLOSS     = (uint16_t)0x0202,  /*!< Arbitration Loss Flag */
//   I2C_FLAG_BUSERROR            = (uint16_t)0x0201,  /*!< Misplaced Start or Stop condition */

//   /* SR3 register flags */
//   I2C_FLAG_GENERALCALL         = (uint16_t)0x0310,  /*!< General Call header received Flag */
//   I2C_FLAG_TRANSMITTERRECEIVER = (uint16_t)0x0304,  /*!< Transmitter Receiver Flag */
//   I2C_FLAG_BUSBUSY             = (uint16_t)0x0302,  /*!< Bus Busy Flag */
//   I2C_FLAG_MASTERSLAVE         = (uint16_t)0x0301   /*!< Master Slave Flag */
ErrorStatus I2C_Err_Happened(void)
{
    return (bI2C_Err_happened);
}

ErrorStatus I2CCheckERREN(void)
{
    ErrorStatus Swif = SUCCESS;
    if (I2C_GetFlagStatus(I2C_FLAG_OVERRUNUNDERRUN) || I2C_GetFlagStatus(I2C_FLAG_ACKNOWLEDGEFAILURE) || I2C_GetFlagStatus(I2C_FLAG_ARBITRATIONLOSS) || I2C_GetFlagStatus(I2C_FLAG_BUSERROR))
    {
        Swif = ERROR;
        I2C_ClearFlag(I2C_FLAG_OVERRUNUNDERRUN);
        I2C_ClearFlag(I2C_FLAG_ACKNOWLEDGEFAILURE);
        I2C_ClearFlag(I2C_FLAG_ARBITRATIONLOSS);
        I2C_ClearFlag(I2C_FLAG_BUSERROR);
        bI2C_Err_happened = TRUE;
    }

    if (bI2C_OVERRUNUNDERRUN || bI2C_ACKNOWLEDGEFAILURE || bI2C_ARBITRATIONLOSS || bI2C_BUSERROR)
    {
        Swif = ERROR;
        bI2C_OVERRUNUNDERRUN = FALSE;
        bI2C_ACKNOWLEDGEFAILURE = FALSE;
        bI2C_ARBITRATIONLOSS = FALSE;
        bI2C_BUSERROR = FALSE;
        bI2C_Err_happened = TRUE;
    }

    return (Swif);
}
/**
* @brief 当I2CBUSY 死锁时，可以执行I2C 软停止指令
* @param
* @retval
*/
void I2CSoftStop(void)
{
    GPIO_Init(GPIOB, GPIO_PIN_4, GPIO_MODE_OUT_OD_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_OD_LOW_SLOW);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
    GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    delay_us(3);
    GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    delay_us(3);
}
/**
* @brief I2C 初始化函数，设置频率 地址位数 中断等
* @param
* @retval
*/
void I2CInit(void)
{
    I2C_DeInit();
    if (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
    {
        I2CSoftStop();
    }
    while (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY) || !I2CCheckERREN())
    {
        I2C_SoftwareResetCmd(ENABLE);
    }
    I2C_SoftwareResetCmd(DISABLE);
    I2C_Init(IIC_FREQUENCY, 0XA0, I2C_DUTYCYCLE_16_9, I2C_ACK_CURR, I2C_ADDMODE_7BIT, 16); //I2C 输出频    率100K，输入频率8MHz
    // I2C_GeneralCallCmd(ENABLE);                                                   //   General Call
    I2C_ITConfig(I2C_IT_ERR, ENABLE); //开启中断
    I2C_ITConfig(I2C_IT_EVT, ENABLE);
    I2C_ITConfig(I2C_IT_BUF, ENABLE);
    I2CFunction = 0;
    I2CWriteDataStep = 0;
    I2CReadDataStep = 0;
    I2CReadGeneralDataStep = 0;
    I2CWriteGeneralDataStep = 0;
}
/**
* @brief I2C 单次写入字节函数
* @param SlaveAddress:I2C 从地址
* @param RegisterAddress:I2C 从寄存器地址
* @param WriteData:写入I2C 寄存器地址的数据
* @retval ErrorStatus:ERROR 调用失败，SUCCESS 调用成功
*/
ErrorStatus I2CWriteByte(uint8_t SlaveAddress, uint8_t RegisterAddress, uint8_t *pWriteData)
{
    ErrorStatus Swif = ERROR;
    while (!I2CCheckERREN())
    {
        I2CInit();
    }
    if (!I2CFunction)
    {
        if (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2C_Cmd(DISABLE);
            I2CSoftStop();
            I2C_Cmd(ENABLE);
        }
        if (!I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2CFunction = 1;
            I2CDirection = I2C_DIRECTION_TX; //I2C_DIRECTION_RX
            I2CSlaveAddress = SlaveAddress;
            I2CRegisterAddress = RegisterAddress;
            pI2CWriteData = pWriteData;
            I2C_AcknowledgeConfig(I2C_ACK_CURR);
            I2C_GenerateSTART(ENABLE);
            Swif = SUCCESS;
        }
    }
    return (Swif);
}
/**
* @brief I2C 连续写入字节函数
* @param SlaveAddress:I2C 从地址
* @param RegisterAddress:I2C 从寄存器地址
* @param pWriteDataBuffer:写入I2C 寄存器地址的数据指针
* @param WriteDataCount:写入I2C 寄存器地址的数据个数
* @retval ErrorStatus:ERROR 调用失败，SUCCESS 调用成功
*/
ErrorStatus I2CWriteData(uint8_t SlaveAddress, uint8_t RegisterAddress, uint8_t *pWriteDataBuffer, uint8_t WriteDataCount)
{
    ErrorStatus Swif = ERROR;
    while (!I2CCheckERREN())
    {
        I2CInit();
    }
    if (!I2CFunction)
    {
        if (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2C_Cmd(DISABLE);
            I2CSoftStop();
            I2C_Cmd(ENABLE);
        }
        if (!I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2CFunction = 2;
            I2CDirection = I2C_DIRECTION_TX; //I2C_DIRECTION_RX
            I2CSlaveAddress = SlaveAddress;
            I2CRegisterAddress = RegisterAddress;
            pI2CDataBuffer = pWriteDataBuffer;
            I2CDataCount = WriteDataCount;
            I2C_AcknowledgeConfig(I2C_ACK_CURR);
            I2C_GenerateSTART(ENABLE);
            Swif = SUCCESS;
        }
    }
    return (Swif);
}
/**
* @brief I2C 单次读取字节函数
* @param SlaveAddress:I2C 从地址
* @param RegisterAddress:I2C 从寄存器地址
* @param pReadData:读取I2C 寄存器地址的数据
* @retval ErrorStatus:ERROR 调用失败，SUCCESS 调用成功
*/
ErrorStatus I2CReadByte(uint8_t SlaveAddress, uint8_t RegisterAddress, uint8_t *pReadData)
{
    ErrorStatus Swif = ERROR;
    while (!I2CCheckERREN())
    {
        I2CInit();
    }
    if (!I2CFunction)
    {
        if (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2C_Cmd(DISABLE);
            I2CSoftStop();
            I2C_Cmd(ENABLE);
        }
        if (!I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2CFunction = 3;
            I2CDirection = I2C_DIRECTION_TX; //I2C_DIRECTION_RX
            I2CSlaveAddress = SlaveAddress;
            I2CRegisterAddress = RegisterAddress;
            pI2CReadData = pReadData;
            I2C_AcknowledgeConfig(I2C_ACK_CURR);
            I2C_GenerateSTART(ENABLE);
            Swif = SUCCESS;
        }
    }
    return (Swif);
}
/**
* @brief I2C 连续读取字节函数
* @param SlaveAddress:I2C 从地址
* @param RegisterAddress:I2C 从寄存器地址
* @param pReadData:读取I2C 寄存器地址的数据
* @retval ErrorStatus:ERROR 调用失败，SUCCESS 调用成功
*/
ErrorStatus I2CReadData(uint8_t SlaveAddress, uint8_t RegisterAddress, uint8_t *pReadDataBuffer, uint8_t ReadDataCount)
{
    ErrorStatus Swif = ERROR;
    while (!I2CCheckERREN())
    {
        I2CInit();
    }
    if (!I2CFunction)
    {
        if (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2C_Cmd(DISABLE);
            I2CSoftStop();
            I2C_Cmd(ENABLE);
            // I2C_GeneralCallCmd(ENABLE);
        }
        if (!I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2CFunction = 4;
            I2CDirection = I2C_DIRECTION_TX; //I2C_DIRECTION_RX
            I2CSlaveAddress = SlaveAddress;
            I2CRegisterAddress = RegisterAddress;
            pI2CDataBuffer = pReadDataBuffer;
            I2CDataCount = ReadDataCount;
            I2C_AcknowledgeConfig(I2C_ACK_CURR);
            I2C_GenerateSTART(ENABLE);
            Swif = SUCCESS;
        }
    }
    return (Swif);
}

/**
* @brief I2C 普通广播呼叫连续读取字节函数
* @param SlaveAddress:I2C 从地址
* @param pReadData:读取I2C 寄存器地址的数据
* @retval ErrorStatus:ERROR 调用失败，SUCCESS 调用成功
*/
ErrorStatus I2CGeneralReadData(uint8_t SlaveAddress, uint8_t *pReadDataBuffer, uint8_t ReadDataCount)
{
    ErrorStatus Swif = ERROR;
    while (!I2CCheckERREN())
    {
        I2CInit();
    }
    if (!I2CFunction)
    {
        if (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2C_Cmd(DISABLE);
            I2CSoftStop();
            I2C_Cmd(ENABLE);
            // I2C_GeneralCallCmd(ENABLE);
        }
        if (!I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2CFunction = 5;
            I2CDirection = I2C_DIRECTION_RX; //I2C_DIRECTION_TX; //
            I2CSlaveAddress = SlaveAddress;  //
            // I2CRegisterAddress = RegisterAddress;
            pI2CDataBuffer = pReadDataBuffer;
            I2CDataCount = ReadDataCount;
            I2C_AcknowledgeConfig(I2C_ACK_CURR);
            // I2C_StretchClockCmd(ENABLE);
            // I2C_GeneralCallCmd(ENABLE);
            I2C_GenerateSTART(ENABLE);
            Swif = SUCCESS;
        }
    }
    return (Swif);
}

/**
* @brief I2C 普通连续写入字节函数
* @param SlaveAddress:I2C 从地址
* @param RegisterAddress:I2C 从寄存器地址--无
* @param pWriteDataBuffer:写入I2C 寄存器地址的数据指针
* @param pReadDataBuffer:读取I2C 寄存器地址的数据
* @param WriteDataCount:写入I2C 寄存器地址的数据个数
* @retval ErrorStatus:ERROR 调用失败，SUCCESS 调用成功
*/
ErrorStatus I2CWriteandReadData(uint8_t SlaveAddress, uint8_t *pWriteDataBuffer, uint8_t WriteDataCount, uint8_t *pReadDataBuffer, uint8_t ReadDataCount)
{
    ErrorStatus Swif = ERROR;
    while (!I2CCheckERREN())
    {
        I2CInit();
    }
    if (!I2CFunction)
    {
        if (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2C_Cmd(DISABLE);
            I2CSoftStop();
            I2C_Cmd(ENABLE);
        }
        if (!I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
        {
            I2CFunction = 6;
            I2CDirection = I2C_DIRECTION_TX; //I2C_DIRECTION_RX
            I2CSlaveAddress = SlaveAddress;
            // I2CRegisterAddress = RegisterAddress;
            pI2CDataBuffer = pWriteDataBuffer;
            pI2CDataBuffer2 = pReadDataBuffer;
            I2CDataCount = WriteDataCount;
            I2CDataCount2 = ReadDataCount;
            I2C_AcknowledgeConfig(I2C_ACK_CURR);
            I2C_GenerateSTART(ENABLE);
            Swif = SUCCESS;
        }
    }
    return (Swif);
}
/**
* @brief I2C 中断调用单次写入字节函数
* @param
* @retval
*/
void I2CFunctionWriteByte(void)
{
    static uint8_t I2CWriteByteStep = 1; //remove master for TC
    switch (I2CWriteByteStep)
    {
    case 0:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT)) //Event 5
        {
            I2C_Send7bitAddress(I2CSlaveAddress, I2CDirection);
            I2C->SR1;
            I2C->SR3;
            I2CWriteByteStep = 1;
        }
        break;
    case 1:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) //Event 6
        {
            I2C_SendData(I2CRegisterAddress);
            I2C->SR1;
            I2C->SR3;
            I2CWriteByteStep = 2;
        }
        break;
    case 2:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTING)) //Event 8
        {
            I2C_SendData(*pI2CWriteData);
            I2C->SR1;
            I2C->SR3;
            I2CWriteByteStep = 3;
        }
        break;
    case 3:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED)) //Event 8_2
        {
            I2C_GenerateSTOP(ENABLE);
            I2C->SR1;
            I2C->SR3;
            I2CWriteByteStep = 0;
            I2CFunction = 0;
        }
        break;
    default:
        I2CWriteByteStep = 0;
        I2CFunction = 0;
    }
}
/**
* @brief I2C 中断调用连续写入字节函数
* @param
* @retval
*/
void I2CFunctionWriteData(void)
{

    //static uint8_t WriteDataCount=0;
    switch (I2CWriteDataStep)
    {
    case 0:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT)) //Event 5
        {
            I2C_Send7bitAddress(I2CSlaveAddress, I2CDirection);
            I2C->SR1;
            I2C->SR3;
            I2CWriteDataStep = 1;
        }
        break;
    case 1:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) //Event 6
        {
            I2C_SendData(I2CRegisterAddress);
            I2C->SR1;
            I2C->SR3;
            I2CWriteDataStep = 2;
        }
        break;
    case 2:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTING)) //Event 8
        {
            I2C_SendData(*pI2CDataBuffer);
            I2C->SR1;
            I2C->SR3;
            if (I2CDataCount > 1)
            {
                I2CDataCount--;
                pI2CDataBuffer++;
            }
            else
            {
                I2CWriteDataStep = 3;
            }
        }
        break;
    case 3:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED)) //Event 8_2
        {
            I2C_GenerateSTOP(ENABLE);
            I2C->SR1;
            I2C->SR3;
            I2CDataCount = 0;
            I2CWriteDataStep = 0;
            I2CFunction = 0;
        }
        break;
    default:
        I2CDataCount = 0;
        I2CWriteDataStep = 0;
        I2CFunction = 0;
    }
}
/**
* @brief I2C 中断调用单次读取字节函数
* @param
* @retval
*/
void I2CFunctionReadByte(void)
{
    static uint8_t I2CReadByteStep = 0;
    switch (I2CReadByteStep)
    {
    case 0:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT)) //Event 5
        {
            I2C_Send7bitAddress(I2CSlaveAddress, I2CDirection);
            I2C->SR1;
            I2C->SR3;
            I2CReadByteStep = 1;
        }
        break;
    case 1:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) //Event 6
        {
            I2C_SendData(I2CRegisterAddress);
            I2C->SR1;
            I2C->SR3;
            I2CReadByteStep = 2;
        }
        break;
    case 2:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED)) //Event 8_2
        {
            I2C_GenerateSTART(ENABLE);
            //I2CDirection=I2C_DIRECTION_RX;
            I2C->SR1;
            I2C->SR3;
            I2CReadByteStep = 3;
        }
        break;
    case 3:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT)) //Event 5
        {
            I2C_Send7bitAddress(I2CSlaveAddress, I2C_DIRECTION_RX);
            I2C->SR1;
            I2C->SR3;
            I2CReadByteStep = 4;
        }
        break;
    case 4:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) //Event 6
        {
            I2C_AcknowledgeConfig(I2C_ACK_NONE);
            I2C_GenerateSTOP(ENABLE);
            I2C->SR1;
            I2C->SR3;
            I2CReadByteStep = 5;
        }
        break;
    case 5:
        if (I2C_GetFlagStatus(I2C_FLAG_RXNOTEMPTY)) //Event 7
        {
            *pI2CReadData = I2C_ReceiveData();
            I2CReadByteStep = 0;
            I2CFunction = 0;
        }
        break;
    default:
        I2CReadByteStep = 0;
        I2CFunction = 0;
    }
}
/**
* @brief I2C 中断调用连续读取字节函数
* @param
* @retval
*/
void I2CFunctionReadData(void)
{ // uint8_t test;
    switch (I2CReadDataStep)
    {
    case 0:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT)) //Event 5
        {

            I2C_Send7bitAddress(I2CSlaveAddress, I2CDirection);
            I2C->SR1;
            I2C->SR3;
            I2CReadDataStep = 1;
        }
        break;
    case 1:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) //Event 6
        {
            //I2C_SendData(I2CRegisterAddress);
            I2C->SR1;
            I2C->SR3;
            I2CReadDataStep = 2;
        }
        break;
    case 2:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED)) //Event 8_2
        {
            I2C_GenerateSTART(ENABLE);
            //I2CDirection=I2C_DIRECTION_RX;
            I2C->SR1;
            I2C->SR3;
            I2CReadDataStep = 3;
        }
        break;
    case 3:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT)) //Event 5
        {
            //I2C_AcknowledgeConfig(I2C_ACK_NONE);
            I2C_Send7bitAddress(I2CSlaveAddress, I2C_DIRECTION_RX);
            I2C->SR1;
            I2C->SR3;
            I2CReadDataStep = 4;
        }
        break;
    case 4:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) //Event 6
        {
            //I2C_GenerateSTOP(ENABLE);
            I2C->SR1;
            I2C->SR3;
            I2CReadDataStep = 5;
        }
        break;
    case 5:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_RECEIVED)) //Event 7
        {
            if (I2CDataCount == 2)
            {
                I2C_AcknowledgeConfig(I2C_ACK_NONE);
                I2C_GenerateSTOP(ENABLE);
                I2CReadDataStep = 6;
                //I2C->SR1;
            }
            *pI2CDataBuffer = I2C_ReceiveData();
            if (I2CDataCount > 1)
            {
                I2CDataCount--;
                pI2CDataBuffer++;
            }
        }
        break;
    case 6:
        if (I2C_GetFlagStatus(I2C_FLAG_RXNOTEMPTY)) //Event 7
        {
            I2C->SR1;
            *pI2CDataBuffer = I2C_ReceiveData();
            I2CDataCount = 0;
            I2CReadDataStep = 0;
            I2CFunction = 0;
        }
        break;
    default:
        I2CDataCount = 0;
        I2CReadDataStep = 0;
        I2CFunction = 0;
    }
}

/**
* @brief I2C 中断调用连续读取字节函数无slave地址
* @param
* @retval
*/
void I2CFunctionGeneralReadData(void)
{

    switch (I2CReadGeneralDataStep)
    {
    case 0:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT)) //Event 5
        {
            // I2C_Send7bitAddress(0x30, I2CDirection); //Test if no ACK, communication handle
            I2C_Send7bitAddress(I2CSlaveAddress, I2CDirection);
            I2C->SR1;
            I2C->SR3;
            I2CReadGeneralDataStep = 1;
        }
        break;
    case 1:
        if (I2C->SR1 & 0x02) // no need slaveAddress
        //(I2C_CheckEvent(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) //Event 6
        {
            // I2C_SendData(I2CRegisterAddress);
            //I2C_AcknowledgeConfig(I2C_ACK_CURR);
            I2C->SR1;
            I2C->SR3;
            I2CReadGeneralDataStep = 3; // not need slaveAddress, skip  case 2 here
        }
        break;
    case 2:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED)) //Event 8_2
        {

            I2C_GenerateSTART(ENABLE);
            //I2CDirection=I2C_DIRECTION_RX;
            I2C->SR1;
            I2C->SR3;
            I2CReadGeneralDataStep = 3;
        }
        break;

    case 3:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_RECEIVED)) //Event 7
        {
            //GPIO_WriteLow(GPIOD, GPIO_PIN_4);
            //GPIO_WriteHigh(GPIOD, GPIO_PIN_4);
            if (I2C_GetFlagStatus(I2C_FLAG_RXNOTEMPTY)) //Event 7
            {

                if (I2CDataCount <= 2)
                {
                    I2C_AcknowledgeConfig(I2C_ACK_CURR);
                    I2C_GenerateSTOP(ENABLE);
                    I2CReadGeneralDataStep = 4;
                    //I2C->SR1;
                }
                *pI2CDataBuffer = I2C_ReceiveData();
                if (I2CDataCount > 1) //Event 7_1
                {
                    I2CDataCount--;
                    pI2CDataBuffer++;
                }
            }
        }
        break;
    case 4:
        if (I2C_GetFlagStatus(I2C_FLAG_RXNOTEMPTY)) //Event 7
        {
            I2C->SR1;
            *pI2CDataBuffer = I2C_ReceiveData();
            I2CDataCount = 0;
            I2CReadGeneralDataStep = 0;
            I2CFunction = 0;
        }
        break;
    default:
        I2CDataCount = 0;
        I2CReadGeneralDataStep = 0;
        I2CFunction = 0;
    }
}

/**
* @brief I2C 中断调用连续读取字节函数无slave地址
* @param
* @retval
*/
void I2CFunctionWriteandReadData(void)
{

    switch (I2CWriteGeneralDataStep)
    {
    case 0:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT)) //Event 5
        {
            // I2C_Send7bitAddress(0x30, I2CDirection); //Test if no ACK, communication handle
            I2C_Send7bitAddress(I2CSlaveAddress, I2CDirection);
            I2C->SR1;
            I2C->SR3;
            I2CWriteGeneralDataStep = 1;
        }
        break;
    case 1:
        if (I2C->SR1 & 0x02) // no need slaveAddress
        //(I2C_CheckEvent(I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) //Event 6
        {
            // I2C_SendData(I2CRegisterAddress);
            //I2C_AcknowledgeConfig(I2C_ACK_CURR);
            I2C->SR1;
            I2C->SR3;
            I2CWriteGeneralDataStep = 2; // not need slaveAddress, skip  case 2 here
        }
        break;
    case 2:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTING)) //Event 8
        {
            I2C_SendData(*pI2CDataBuffer);
            I2C->SR1;
            I2C->SR3;
            if (I2CDataCount > 1)
            {
                I2CDataCount--;
                pI2CDataBuffer++;
            }
            else
            {
                I2CWriteGeneralDataStep = 3;
            }
        }
        break;

    case 3:
        if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_TRANSMITTED)) //Event 8_2
        {
            I2C_GenerateSTOP(ENABLE);
            I2C->SR1;
            I2C->SR3;
            I2CDataCount = 0;
            I2CWriteGeneralDataStep = 4;
        }
        break;
    case 4:
        I2CWriteGeneralDataStep = 0;
        I2CFunction = 0;
        I2CGeneralReadData(IIC_SLAVE_ADDRESS7, pI2CDataBuffer2, I2CDataCount2);
            break;
    default:
        I2CDataCount = 0;
        I2CWriteGeneralDataStep = 0;
        I2CFunction = 0;
    }
}

/*
* @brief I2C Interrupt routine.
* @param None
* @retval None
*/
void I2C_Function_IRQHandler(void)
{
    /* In order to detect unexpected events during development,it is recommended to set a breakpoint on the following instruction.*/
    //disableInterrupts();
    if (I2C_GetITStatus(I2C_ITPENDINGBIT_OVERRUNUNDERRUN) ||
        I2C_GetITStatus(I2C_ITPENDINGBIT_ACKNOWLEDGEFAILURE) ||
        I2C_GetITStatus(I2C_ITPENDINGBIT_ARBITRATIONLOSS) ||
        I2C_GetITStatus(I2C_ITPENDINGBIT_BUSERROR))
    {
        I2C_Cmd(DISABLE);
        I2C_Cmd(ENABLE);

        I2CInit();
        I2CSoftStop();

        //while (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY) || !I2CCheckERREN())
        //{
        //    I2C_SoftwareResetCmd(ENABLE);
        //}
        //I2C_SoftwareResetCmd(DISABLE);

        I2C_GenerateSTART(DISABLE);
        I2C_ITConfig(I2C_IT_ERR, DISABLE); //disable interrupt
        I2C_ITConfig(I2C_IT_EVT, DISABLE);
        I2C_ITConfig(I2C_IT_BUF, DISABLE);

        I2C_ClearITPendingBit(I2C_ITPENDINGBIT_OVERRUNUNDERRUN);
        I2C_ClearITPendingBit(I2C_ITPENDINGBIT_ACKNOWLEDGEFAILURE);
        I2C_ClearITPendingBit(I2C_ITPENDINGBIT_ARBITRATIONLOSS);
        I2C_ClearITPendingBit(I2C_ITPENDINGBIT_BUSERROR);

        bI2C_OVERRUNUNDERRUN = TRUE;
        bI2C_ACKNOWLEDGEFAILURE = TRUE;
        bI2C_ARBITRATIONLOSS = TRUE;
        bI2C_BUSERROR = TRUE;
    }
    else
    {
        switch (I2CFunction)
        {
        case 1: //执行I2C 单次写入字节函数
            I2CFunctionWriteByte();
            break;
        case 2: //执行I2C 连续写入字节函数
            I2CFunctionWriteData();
            break;
        case 3: //执行I2C 单次读取字节函数
            I2CFunctionReadByte();
            break;
        case 4: //执行I2C 连续读取字节函数
            I2CFunctionReadData();
            break;
        case 5: //执行I2C 普通呼叫连续读取字节函数
            I2CFunctionGeneralReadData();
            break;
        case 6: //执行I2C 普通呼叫连续写字节函数
            I2CFunctionWriteandReadData();
            break;
        default: //清除寄存器状态
            I2C->SR1;
            I2C->SR3;
            I2C->DR;
            break;
        }
    }
    //enableInterrupts();
}