/**
  **********************************  STM8S  ***********************************
  * @文件名     ：eeprom.c
  * @作者       ： Michael Shen
  * @库版本     ： V2.2.0
  * @文件版本   ： V1.0.0
  * @日期       ： 
  * @摘要       ： 内部EEPROM源文件
  ******************************************************************************/
/*----------------------------------------------------------------------------
  更新日志:
  2017-05-18 V1.0.0:初始版本
  ----------------------------------------------------------------------------*/
/* 包含的头文件 --------------------------------------------------------------*/
#include "eeprom.h"
#include "stm8s_flash.h"
#include "stm8s.h"
#include <string.h>
#include "display.h"
#include "key.h"
#include "beep.h"

#define EEpnbytesize 10
#define EEpnbytesize3 EEpnbytesize * 3
#define NUM_OF_Autopowerdata 10
u8 ucorignalBuffer[EEpnbytesize3]; // define for reading EEp 3time buffer

static u8 EEPROM_P1_Power_Data[NUM_OF_Autopowerdata]; // = {Default_POWER_Level};
static u8 EEPROM_P1_Time_Data[NUM_OF_Autopowerdata];  //= {Default_POWER_Level};
static u8 EEPROM_P2_Power_Data[NUM_OF_Autopowerdata]; // = {Default_POWER_Level};
static u8 EEPROM_P2_Time_Data[NUM_OF_Autopowerdata];  //= {Default_POWER_Level};
static const u8 key_defaut_sensitivity_command[KEY_SENSITIVY_CONFIG_BUFFE_RSIZE] = KEY_DEFAUT_SENSITIVITY;
eeprom_config_s eeprom_config;

/************************************************
函数名称 ： EEPROM_Initializes
功    能 ： 内部EEPROM初始化
参    数 ： 无
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void EEPROM_Initializes(void)
{
  FLASH_SetProgrammingTime(FLASH_PROGRAMTIME_STANDARD);
  // ucAutopower_P1_Power_Data
  // ucAutopower_P1_Time_Data
  // EEPROM_ReadNByteBuf(EEPROM_P1_Power_Data, EEPROM_ADDR_Power1, EEPROM_SIZE_Power1);
  // EEPROM_ReadNByteBuf(EEPROM_P2_Power_Data, EEPROM_ADDR_Power2, EEPROM_SIZE_Power2);
  // EEPROM_ReadNByteBuf(EEPROM_P1_Time_Data, EEPROM_ADDR_Time1, EEPROM_SIZE_Time1);
  // EEPROM_ReadNByteBuf(EEPROM_P2_Time_Data, EEPROM_ADDR_Time2, EEPROM_SIZE_Time2);

  readconfig();
}

/************************************************
函数名称 ： EEPROM_WriteNByte
功    能 ： 写EEPROM
参    数 ： pBuffer ----- 写入数据区首地址
            WriteAddr --- 要写入Flash的地址
            nByte ------- 要写入的字节数
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void EEPROM_WriteNByte(uint8_t *pBuffer, uint32_t WriteAddr, uint8_t nByte)
{
  FLASH_Unlock(FLASH_MEMTYPE_DATA);

  while (FLASH_GetFlagStatus(FLASH_FLAG_DUL) == RESET)
    ;

  while (nByte--)
  {
    FLASH_ProgramByte(WriteAddr, *pBuffer);
    WriteAddr++;
    pBuffer++;
    FLASH_WaitForLastOperation(FLASH_MEMTYPE_DATA);
  }
  FLASH_Lock(FLASH_MEMTYPE_DATA);
}
/************************************************
函数名称 ： EEPROM_WriteNByteBuf
功    能 ： 写EEPROM 3times
参    数 ： pBuffer ----- 写入数据区首地址
            WriteAddr --- 要写入Flash的地址
            nByte ------- 要写入的字节数
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void EEPROM_WriteNByteBuf(uint8_t *pBuffer, uint32_t WriteAddr, uint8_t nByte)
{
  u16 uc3nByte;
  uc3nByte = nByte * 3;
  u8 *porignalBuffer;
  porignalBuffer = pBuffer;

  EEPROM_WriteNByte(porignalBuffer, WriteAddr, nByte);
  while (uc3nByte--)
  {
    WriteAddr++;
    if (uc3nByte == (nByte * 2))
      EEPROM_WriteNByte(porignalBuffer, WriteAddr, nByte);

    if (uc3nByte == nByte)
    {
      EEPROM_WriteNByte(porignalBuffer, WriteAddr, nByte);
      break;
    }
  }
}
/************************************************
函数名称 ： EEPROM_ReadNByte
功    能 ： 读EEPROM
参    数 ： pBuffer ---- 数据buf
            ReadAddr --- 要读取Flash的首地址
            nByte ------ 要读取的字节数
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
/************************************************/
void EEPROM_ReadNByte(uint8_t *pBuffer, uint32_t ReadAddr, uint8_t nByte)
{
  while (nByte--)
  {
    *pBuffer = FLASH_ReadByte(ReadAddr);
    ReadAddr++;
    pBuffer++;
  }
}
/*************************************************/
/************************************************
函数名称 ： EEPROM_ReadNByte
功    能 ： 读EEPROM
参    数 ： pBuffer ---- 数据buf
            ReadAddr --- 要读取Flash的首地址
            nByte ------ 要读取的字节数
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void EEPROM_ReadNByteBuf(uint8_t *pBuffer, uint32_t ReadAddr, uint8_t nByte)
{
  u8 uc3nByte2;
  uc3nByte2 = nByte * 3;
  u8 i;
  i = 0;
  u8 *pBufferoriginal;
  pBufferoriginal = pBuffer;
  uint32_t ucorignalReadAddr;
  ucorignalReadAddr = ReadAddr;
  u8 ucErrDataPossition;

  while (uc3nByte2--)
  {
    ucorignalBuffer[i] = FLASH_ReadByte(ucorignalReadAddr);
    ucorignalReadAddr++;
    i++;
  }

  for (i = 0; i < nByte; i++)
  {
    ucErrDataPossition = 0;
    if (ucorignalBuffer[i] != ucorignalBuffer[(i + nByte)])
      SetBit(ucErrDataPossition, 0);

    if (ucorignalBuffer[i + nByte] != ucorignalBuffer[(i + nByte * 2)])
      SetBit(ucErrDataPossition, 1);

    if (ucorignalBuffer[i] != ucorignalBuffer[(i + nByte * 2)])
      SetBit(ucErrDataPossition, 2);

    u8 j;
    pBuffer = pBufferoriginal;

    switch (ucErrDataPossition)
    {
    case 7: // all fail
    {

      for (j = 0; j < nByte; j++)
      {
        *pBuffer = ucorignalBuffer[j]; //porignalReadAddr in actual
        pBuffer++;
      }

      EEPROM_WriteNByteBuf(pBuffer, ReadAddr, nByte);
      return;
    }
    break;
    case 0: // no error
    {
      for (j = 0; j < nByte; j++)
      {
        *pBuffer = ucorignalBuffer[j];
        pBuffer++;
      }
    }
    break;
    case 5: // possition 1 error  101
    {
      EEPROM_WriteNByteBuf((ucorignalBuffer + nByte), ReadAddr, nByte);

      for (j = 0; j < nByte; j++)
      {
        *pBuffer = ucorignalBuffer[nByte + j];
        pBuffer++;
      }

      return;
    }
    break;
    case 3: // possition 2 error 011
    {
      goto copypossition1;
    }
    break;
    case 6: // possition 3 error 110
    {
    copypossition1:
      EEPROM_WriteNByteBuf(ucorignalBuffer, ReadAddr, nByte); // copy 1 to 2 and 3
      for (j = 0; j < nByte; j++)
      {
        *pBuffer = ucorignalBuffer[j];
        pBuffer++;
      }
      return;
    }
    break;
    default:
      nop();
    }
  }
}

/************************************************
函数名称 ： EEPROM_EraseNByte
功    能 ： 连续擦除N字节
参    数 ： EraseAddr --- 要擦除的首地址
            nByte ------- 要擦除的字节数
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void EEPROM_EraseNByte(uint32_t EraseAddr, uint8_t nByte)
{
  uint16_t i;

  FLASH_Unlock(FLASH_MEMTYPE_DATA);

  while (FLASH_GetFlagStatus(FLASH_FLAG_DUL) == RESET)

    for (i = 0; i < nByte; i++)
    {
      *(PointerAttr uint8_t *)(MemoryAddressCast)EraseAddr = 0xFF;
      EraseAddr++;
      FLASH_WaitForLastOperation(FLASH_MEMTYPE_DATA);
    }
  FLASH_Lock(FLASH_MEMTYPE_DATA);
}

/************************************************
函数名称 ： EEPROM_Handler
功    能 ： 
参    数 ： 无
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void EEPROM_Handler(void)
{
}

/************************************************
函数名称 ： EEPROM_setdefaultconfig
功    能 ： 
参    数 ： 无
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void setdefaultconfig()
{
  //brightness
  eeprom_config.SEG_birghtness_config = SYSON_4_SEG8;
  //KEY sensitivity defaut
  
  for (u8 i = 0; i < KEY_SENSITIVY_CONFIG_BUFFE_RSIZE; i++)
  {
    eeprom_config.key_sensitivity_config_Buf[i] = key_defaut_sensitivity_command[i];
  }
  eeprom_config.key_sensitivity_feedback = KEY_COMMAND_SENSITIVITY_FEEDBACK;
  eeprom_config.bkey_sensitivity_need_update = TRUE; 
  // Beep_OneLongBeep(); //testing
}

/************************************************
函数名称 ： EEPROM_readconfig
功    能 ： 
参    数 ： 无
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void readconfig()
{
  EEPROM_ReadNByte((uint8_t *)&eeprom_config, EEPROM_ADDR_config, sizeof(eeprom_config));
  if (eeprom_config.writemarker != 0xAB)
  {
    // set_warning(EEPROM_EMPTY);
    setdefaultconfig();
    writeconfig();
    return;
  }

  uint8_t crc = 0;
  uint8_t *config = (uint8_t *)&eeprom_config;
  for (uint32_t i = 0; i < sizeof(eeprom_config); i++)
  {
    uint8_t data = config[i];
    for (uint8_t p = 0; p < 8; p++)
    {
      if ((crc ^ data) & 0x01)
        crc = ((crc >> 1) ^ 0x8C);
      else
        crc >>= 1;

      data >>= 1;
    }
  }
  if (crc)
  {
    // set_warning(EEPROM_BADCRC);
    setdefaultconfig();    
    writeconfig();
    return;
  }
//KEY_COMMAND_SENSITIVITY_FEEDBACK
  if ((!check_sensitive_inrange(eeprom_config.key_sensitivity_config_Buf,KEY_TOTAL_NUMBER)) || !Display_Brightness_Level_Check(eeprom_config.SEG_birghtness_config))
  {
    // set_warning(EEPROM_BADCRC);
    setdefaultconfig();
    writeconfig();
  }
}
/************************************************
函数名称 ： EEPROM_writeconfig
功    能 ： 
参    数 ： 无
            
返 回 值 ： 无
作    者 ： Michael Shen
*************************************************/
void writeconfig()
{
  eeprom_config.writemarker = 0xAB;

  uint8_t crc = 0;
  uint8_t *config = (uint8_t *)&eeprom_config;
  for (uint32_t i = 0; i < sizeof(eeprom_config) - 1; i++)
  {
    uint8_t data = config[i];
    for (uint8_t p = 0; p < 8; p++)
    {
      if ((crc ^ data) & 0x01)
        crc = ((crc >> 1) ^ 0x8C);
      else
        crc >>= 1;

      data >>= 1;
    }
  }
  eeprom_config.crc = crc;

  EEPROM_WriteNByte((uint8_t *)&eeprom_config, EEPROM_ADDR_config, sizeof(eeprom_config)); //EEPROM_ADDR_config

  // if(!I2C_WriteBuffer((uint8_t*)&eeprom_config, sizeof(eeprom_config)))
  // {
  // 	set_error(EEPROM_WRITEERROR);
  // 	return;
  // }

  // uint8_t readdata[sizeof(eeprom_config)];

  // EEPROM_ReadNByte((uint8_t*)readdata,EEPROM_ADDR_config,sizeof(eeprom_config));
  // I2C_TryReadBuffer((uint8_t*)readdata, sizeof(eeprom_config));

  // uint8_t cmpresult = memcmp((uint8_t*) &eeprom_config, readdata, sizeof(eeprom_config));
  //if(cmpresult != 0)
  // set_error(EEPROM_LOCK);
}

/**** Copyright (C)2017 Michael Shen. All Rights Reserved **** END OF FILE ****/
