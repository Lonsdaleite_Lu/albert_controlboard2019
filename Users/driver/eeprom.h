/**
  **********************************  STM8S  ***********************************
  * @文件名     ： eeprom.h
  * @作者       ： Michael Shen
  * @库版本     ： V2.2.0
  * @文件版本   ： V1.0.0
  * @日期       ： 
  * @摘要       ： 内部EEPROM头文件
  ******************************************************************************/

/* 定义防止递归包含 ----------------------------------------------------------*/
// #ifndef ___EEPROM_H__
// #define ___EEPROM_H__
#pragma once
/* 包含的头文件 --------------------------------------------------------------*/
#include "stm8s.h"
#include "display.h"
#include "key.h"
#include "error.h"

#define EEPROM_ADDR_config 0x4000 //EEPROM编程地址
#define EEPROM_ADDR_Power1 0x4000 //EEPROM编程地址
#define EEPROM_SIZE_Power1 10     //BUF长度
#define EEPROM_ADDR_Power2 0x4020 //EEPROM编程地址
#define EEPROM_SIZE_Power2 10     //BUF长度
#define EEPROM_ADDR_Time1 0x4040  //EEPROM编程地址
#define EEPROM_SIZE_Time1 10      //BUF长度
#define EEPROM_ADDR_Time2 0x4060  //EEPROM编程地址
#define EEPROM_SIZE_Time2 10      //BUF长度
#define EEPROM_SEGLightEEpsize 2
#define EEPROM_SEGLightEEpaddress 0x4080
//static u8 WriteBuf[BUF_SIZE] = {0x68, 0x6A, 0x6C, 0x6E};
//static u8 ReadBuf[BUF_SIZE];

/* 宏定义 --------------------------------------------------------------------*/
#pragma pack(push, 1)
typedef struct _eeprom_config_s
{
  uint8_t writemarker;
  //
  uint8_t Power1_config[EEPROM_SIZE_Power1];
  //
  uint8_t Power2_config[EEPROM_SIZE_Power2];
  //
  uint16_t Time1_config[EEPROM_SIZE_Time1];
  //
  uint16_t Time2_config[EEPROM_SIZE_Time2];
  //
  uint16_t SEG_birghtness_config;
  //
  bool bkey_sensitivity_need_update;
  //
  uint8_t key_sensitivity_config_Buf[KEY_SENSITIVY_CONFIG_BUFFE_RSIZE];
  //
  uint8_t key_sensitivity_feedback;
  //
  enum errorcode error;

  uint8_t crc;
  
} eeprom_config_s;
// static eeprom_config_s eeprom_config;

#pragma pack(pop)

extern eeprom_config_s eeprom_config;

/* 函数申明 ------------------------------------------------------------------*/
void EEPROM_Initializes(void);
void EEPROM_WriteNByte(uint8_t *pBuffer, uint32_t WriteAddr, uint8_t nByte);
void EEPROM_ReadNByte(uint8_t *pBuffer, uint32_t ReadAddr, uint8_t nByte);
void EEPROM_ReadNByteBuf(uint8_t *pBuffer, uint32_t ReadAddr, uint8_t nByte);
void EEPROM_WriteNByteBuf(uint8_t *pBuffer, uint32_t WriteAddr, uint8_t nByte);
void EEPROM_Handler(void);

void readconfig();
void writeconfig();
// #endif /* _BSP_EEPROM_H */
/*
  WriteBuf[0] = 1;
  WriteBuf[2] = 1;
  WriteBuf[3] = 1;

  WriteBuf[1] = 2;

  EEPROM_WriteNByte(WriteBuf, EEPROM_ADDR, BUF_SIZE);
    EEPROM_ReadNByte(ReadBuf, EEPROM_ADDR, BUF_SIZE);
*/

/**** Copyright (C)2017 Michael Shen. All Rights Reserved **** END OF FILE ****/
