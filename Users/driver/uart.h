#ifndef __UART_H__
#define __UART_H__

#include "stm8s.h"
#include "stm8s_uart1.h"



void Uart_Init(void);
void Uart_ParameterConfig(uint32_t BaudRate, UART1_WordLength_TypeDef WordLength, 
                UART1_StopBits_TypeDef StopBits, UART1_Parity_TypeDef Parity, 
                UART1_SyncMode_TypeDef SyncMode, UART1_Mode_TypeDef Mode);
void Uart_Enable(FunctionalState NewState);
void UART_InterruptConfig(UART1_IT_TypeDef UART1_IT, FunctionalState NewState);
void Uart_SendData(uint8_t Data);
uint8_t Uart_ReadData(void);

#endif //#ifndef __UART_H__