#include "adc.h"
#include "stm8s_adc1.h"

void ADC_Init(void)
{
    /* De-Init ADC peripheral*/
    ADC1_DeInit();

    /* Init ADC2 peripheral */
    //   ADC1_Init(ADC1_CONVERSIONMODE_SINGLE,
    //             ADC1_CHANNEL_0,
    //             ADC1_PRESSEL_FCPU_D2,
    //             ADC1_EXTTRIG_TIM,
    //             DISABLE,
    //             ADC1_ALIGN_RIGHT,
    //             ADC1_SCHMITTTRIG_CHANNEL0,
    //             DISABLE);
    /* Enable EOC interrupt */
    // ADC1_ITConfig(ADC1_IT_EOCIE, ENABLE);
    ADC1_PrescalerConfig(ADC1_PRESSEL_FCPU_D2);
    ADC1_ExternalTriggerConfig(ADC1_EXTTRIG_TIM, DISABLE);
    //   ADC1_SchmittTriggerConfig(ADC1_SCHMITTTRIG_CHANNEL0,DISABLE);
    ADC1_Cmd(ENABLE); //开启ADC
}

u16 Get_ADCCH_Avr_value(ADC1_Channel_TypeDef ADC1_Channel)
{
    ADC1_ConversionConfig(ADC1_CONVERSIONMODE_SINGLE, ADC1_Channel, ADC1_ALIGN_RIGHT);
    uint8_t i;
    uint16_t adc_value = 0;
    for (i = 0; i < 10; i++)
    {
        ADC1_StartConversion(); //启动AD转换
        while (RESET == ADC1_GetFlagStatus(ADC1_FLAG_EOC))
        {
            /* code */
            nop();
        }
        ADC1_ClearFlag(ADC1_FLAG_EOC); //等待转换完成，并清除标志

        adc_value += ADC1_GetConversionValue(); //读取转换结果
    }
    adc_value = adc_value / 10; //求平均
    return adc_value;
}
