#ifndef __MODBUS_H__
#define __MODBUS_H__

#include "stm8s.h"

typedef struct _tModBus_SendStart
{
  u8 ucDeviceAddr;
  u8 ucCMD;
  u8 ucDataAddrH;
  u8 ucDataAddrL;
  u8 ucDataNumberH;
  u8 ucDataNumberL;
  u8 ucCRCValueL;
  u8 ucCRCValueH;
}tModBus_SendStart;


typedef struct _tModBus_ReadStart
{
  u8 ucDeviceAddr;
  u8 ucCMD;
  u8 ucDataNumber;
  u8 ucDataH;
  u8 ucDataL;
  u8 ucCRCValueL;
  u8 ucCRCValueH;
}tModBus_ReadStart;


typedef struct _tModBus_Send
{
  u8 ucDeviceAddr;
  u8 ucCMD;
  u8 ucDataAddrH;
  u8 ucDataAddrL;
  u8 ucARequestPowerH;
  u8 ucARequestPowerL;
  u8 ucBRequestPowerH;
  u8 ucBRequestPowerL;
  u8 ucACMDConfig;
  u8 ucBCMDConfig;
  u8 ucAPotTemperature;    // read only
  u8 ucBPotTemperature;    // read only
  u8 ucAIgbtTemperature;   // read only
  u8 ucBIgbtTemperature;   // read only
  u8 ucCRCValueL;
  u8 ucCRCValueH;
}tModBus_Send;

typedef struct _tModBus_Read
{
  u8 ucDeviceAddr;
  u8 ucCMD;
  u8 ucDataAddrH;
  u8 ucDataAddrL;
  u8 ucARequestPowerH;
  u8 ucARequestPowerL;
  u8 ucBRequestPowerH;
  u8 ucBRequestPowerL;
  u8 ucMainsVoltage;
  u8 ucErrorCode;
  u8 ucAPotTemperature;    // read only
  u8 ucBPotTemperature;    // read only
  u8 ucAIgbtTemperature;   // read only
  u8 ucBIgbtTemperature;   // read only
  u8 ucCRCValueL;
  u8 ucCRCValueH;
}tModBus_Read;
  
  









#endif //#ifndef __MODBUS_H__