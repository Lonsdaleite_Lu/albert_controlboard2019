#include "modbus.h"


#define I2C_SPEED              9600
#define I2C_SLAVE_ADDRESS7     0x01










/*******************************************************************************
**函数名称:void MODBUS_Init() 
**功能描述:初始化IIC1接口
**入口参数:
**输出:无
*******************************************************************************/
void MODBUS_Init(){					
  I2C_Cmd(ENABLE);
  /* sEE_I2C configuration after enabling it */
  I2C_Init(I2C_SPEED, I2C_SLAVE_ADDRESS7, 
           I2C_DUTYCYCLE_2, I2C_ACK_NONE,
           I2C_ADDMODE_7BIT, 16);
}









